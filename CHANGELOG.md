
# Changelog
All notable changes to the Samia software will be documented in this file.

## [0.1.0] - 2020-03-26

### Initial version
 - Magnetic field calculation from current density (2D and 3D)
 - Magnetic field calcualtion from magnetized material (2D and 3D)
 - Calculation of magnetization of linear and non-linear magnetic materials (2D and 3D)
 - Basic mesh functionalities (2D and 3D)
 - Current density of superconductors in a magnetic field (only 2D)
 - Postprocess functionalities, such as enery and force calculations