# -*- coding: utf-8 -*-
"""
Created on Wed May 01 22:41:01 2013

@author: Kevin
"""

from Samia.MagneticField.field_from_source import field_from_current, field_from_magnetization
from Samia.Support.plot import plot_mesh_data, plot_mesh_data_2
from Samia.MagneticField.method_of_moments import find_M_local, find_mur
      
from numpy import pi, sqrt, array

def Calculate_Energy(mesh, mesh_pm, Mx_pm, My_pm, mesh_mag, Mx_mag, My_mag, opt, sol):
    
    from numpy import pi, sqrt
    
    mu0 = 4*pi*10**-7
    E = 0.0
    
    X = [0] * mesh.n_ele
    Y = [0] * mesh.n_ele
    for i in range(mesh.n_ele):
        X[i] = [mesh.cent[i][0]]
        Y[i] = [mesh.cent[i][1]]
    

    Bc_x, Bc_y = field_from_magnetization(X, Y,  mesh_pm, Mx_pm, My_pm)  # De l'aimant permanent
    BX, BY = field_from_magnetization(X, Y,  mesh_mag, Mx_mag, My_mag) # De la magnétisation calculée
    Btot_norm = [0] * len(Bc_x)
    Btot_x = [0] * len(Bc_x)
    Btot_y = [0] * len(Bc_x)
    
    for i in range(mesh.n_ele):
        Btot_x[i] = Bc_x[i][0]+BX[i][0]
        Btot_y[i] = Bc_y[i][0]+BY[i][0]
        Btot_norm[i] = sqrt(Btot_x[i]**2 + Btot_y[i]**2)
        # Adding energy of one point (without the weight of all energies)

    if opt=='air': 
        for i in range(mesh.n_ele):
            E += mesh.area[i] * Btot_norm[i]**2 / mu0
    
    else:
        if opt=='mag':
            Mx = Mx_mag
            My = My_mag
        else:
            Mx = Mx_pm
            My = My_pm
        
        Htot_x = [0] * len(Bc_x)
        Htot_y = [0] * len(Bc_x)
        for i in range(mesh.n_ele):
            Htot_x[i] = Btot_x[i]/mu0 - Mx[i]
            Htot_y[i] = Btot_y[i]/mu0 - My[i]
            # Adding energy of one point (without the weight of all energies)
            #E += mesh.area[i]*abs(Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i])
            mur = find_mur(sol, mesh_mag, [X[i][0],Y[i][0]])
    
            # Adding energy of one point (without the weight of all energies)
            #print Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i]
            #E += mesh.area[i]*(Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i])
            E += mesh.area[i] * Btot_norm[i]**2 / ((mur)*mu0)
            
    # Add the weight of all energies
    E *= 0.5
    
    return E


def Calculate_Energy_J(mesh, mesh_con, J, mesh_mag, Mx_mag, My_mag, opt, sol):
    
    #mesh.plot()
    #show()

    mu0 = 4*pi*10**-7
    E = 0.0
    Ed = [0] * mesh.n_ele
    X = [0] * mesh.n_ele
    Y = [0] * mesh.n_ele
    for i in range(mesh.n_ele):
        X[i] = [mesh.cent[i][0]]
        Y[i] = [mesh.cent[i][1]]

    Bc_x, Bc_y = field_from_current(X, Y,  mesh_con, J)  # Du courant
    BX, BY = field_from_magnetization(X, Y,  mesh_mag, Mx_mag, My_mag) # De la magnétisation calculée
    Btot_norm = [0] * len(Bc_x)
    Btot_x = [0] * len(Bc_x)
    Btot_y = [0] * len(Bc_x)
    
    for i in range(mesh.n_ele):
        Btot_x[i] = Bc_x[i][0]+BX[i][0]
        Btot_y[i] = Bc_y[i][0]+BY[i][0]
        Btot_norm[i] = sqrt(Btot_x[i]**2 + Btot_y[i]**2)
        # Adding energy of one point (without the weight of all energies)

    if opt=='air': 
        
        for i in range(mesh.n_ele):
            E += mesh.area[i] * Btot_norm[i]**2 / mu0
            #Ed[i] = 0.5*Btot_norm[i]**2 / mu0


    else:
        #Mx = Mx_mag
        #My = My_mag

        Htot_x = [0] * len(Bc_x)
        Htot_y = [0] * len(Bc_x)
        for i in range(mesh.n_ele):
            pos = array(mesh.cent[i])
            Mx, My = find_M_local(Mx_mag, My_mag, mesh_mag, pos)
            #mur = find_mur(sol, mesh_mag, pos)
            #print mur
            Htot_x[i] = Btot_x[i]/mu0 - Mx
            Htot_y[i] = Btot_y[i]/mu0 - My
            #mur = find_mur(sol, mesh_mag, [X[i][0],Y[i][0]])
    
            # Adding energy of one point (without the weight of all energies)
            #print Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i]
            E += mesh.area[i]*(Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i])
            #Ed[i] = 0.5*(Btot_x[i]*Htot_x[i] + Btot_y[i]*Htot_y[i])
            #E += mesh.area[i] * Btot_norm[i]**2 / ((mur)*mu0)
            #if opt=='mag':
                #print mur
    # Add the weight of all energies
    
    E *= 0.5


    #plot_mesh_data_2(mesh, Ed,'Energy density')
    #plot_mesh_data(mesh, Btot_x, Btot_y,'B norm')
    #show()
    return E
    
    


    

    
    
    
    