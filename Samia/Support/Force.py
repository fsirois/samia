 
from Samia.MagneticField.field_from_source import field_from_current, field_from_magnetization

from numpy import linspace, meshgrid, pi
    
def force_from_energy(x,E):
    ''''''
        
    
    F = [0] * len(E)
    for i in range(len(E)):
        if i==0:
            F[0] = -(E[1]-E[0])/(x[1]-x[0])
        elif i==(len(E)-1):
            F[-1] = -(E[-1]-E[-2])/(x[-1]-x[-2])
        else:
            F[i] = -(E[i+1]-E[i-1])/(x[i+1]-x[i-1])
        
        
    return F
    
def maxwell_force(geometry, list_of_types, *args):

    n_types = len(list_of_types)
    
    mu0 = 4*pi*10**-7
    
    Fy1 = 0.0
    Fx1 = 0.0
    Fy2 = 0.0
    Fx2 = 0.0
    Fy3 = 0.0
    Fx3 = 0.0
    Fy4 = 0.0
    Fx4 = 0.0
    
    xMin = geometry[0][0]
    xMax = geometry[0][1]
    yMin = geometry[1][0]
    yMax = geometry[1][1]
    
    gap = 0.0002
    npt = 100
    
    xMin -= gap
    xMax += gap
    yMin -= gap
    yMax += gap
    
    y_sweep = linspace(yMin, yMax, npt)
    x_sweep = linspace(xMin, xMax, npt)


    #-----------------
    # Bottom
    #----------------
    [x_hor_bottom, y_hor_bottom] = meshgrid(x_sweep, [yMin])

    Btot_x = [0] * npt
    Btot_y = [0] * npt
    
    for source in range(n_types):
        if list_of_types[source] == 1:
            Btmp_x, Btmp_y = field_from_current(args[3*source], args[3*source+1], args[3*source+2], x_hor_bottom, y_hor_bottom)  # Du courant
        else:
            Btmp_x, Btmp_y = field_from_magnetization(args[3*source], args[3*source+1], args[3*source+2], x_hor_bottom, y_hor_bottom)
    
        for i in range(npt):
            Btot_x[i] += Btmp_x[0][i]
            Btot_y[i] += Btmp_y[0][i]

    for i in range(npt):
        dx = x_hor_bottom[0][1]-x_hor_bottom[0][0]

        Fy1 -= 0.5 * dx * (Btot_y[i]**2-Btot_x[i]**2)/mu0
        Fx1 -= dx * (Btot_x[i]*Btot_y[i])/mu0



    #-----------------
    # Top
    #-----------------
    [x_hor_top, y_hor_top] = meshgrid(x_sweep, [yMax])
    
    Btot_x = [0] * npt
    Btot_y = [0] * npt
    
    for source in range(n_types):
        if list_of_types[source] == 1:
            Btmp_x, Btmp_y = field_from_current(args[3*source], args[3*source+1], args[3*source+2], x_hor_top, y_hor_top)  # Du courant
        else:
            Btmp_x, Btmp_y = field_from_magnetization(args[3*source], args[3*source+1], args[3*source+2], x_hor_top, y_hor_top)
    
        for i in range(npt):
            Btot_x[i] += Btmp_x[0][i]
            Btot_y[i] += Btmp_y[0][i]

    for i in range(npt):
        dx = x_hor_top[0][2]-x_hor_top[0][1]
        Fy2 += 0.5 * dx * (Btot_y[i]**2-Btot_x[i]**2)/mu0
        Fx2 += dx * (Btot_y[i]*Btot_x[i])/mu0
    

    #-----------------
    # Left
    #-----------------
    [x_lat_left, y_lat_left]  = meshgrid([xMin],y_sweep)
    
    Btot_x = [0] * npt
    Btot_y = [0] * npt
    
    for source in range(n_types):
        if list_of_types[source] == 1:
            Btmp_x, Btmp_y = field_from_current(args[3*source], args[3*source+1], args[3*source+2], x_lat_left, y_lat_left)  # Du courant
        else:
            Btmp_x, Btmp_y = field_from_magnetization(args[3*source], args[3*source+1], args[3*source+2], x_lat_left, y_lat_left)
    
        for i in range(npt):
            Btot_x[i] += Btmp_x[i][0]
            Btot_y[i] += Btmp_y[i][0]

    for i in range(npt):
        dy = y_lat_left[1][0]-y_lat_left[0][0]
        Fy3 -=  dy * (Btot_y[i]*Btot_x[i])/mu0
        Fx3 -= 0.5 * dy * (Btot_x[i]**2 - Btot_y[i]**2)/mu0
    

    
    #-----------------
    # Right
    #-----------------
    [x_lat_right,y_lat_right] = meshgrid([xMax],y_sweep)

    Btot_x = [0] * npt
    Btot_y = [0] * npt
    
    for source in range(n_types):
        if list_of_types[source] == 1:
            Btmp_x, Btmp_y = field_from_current(args[3*source], args[3*source+1], args[3*source+2], x_lat_right, y_lat_right)  # Du courant
        else:
            Btmp_x, Btmp_y = field_from_magnetization(args[3*source], args[3*source+1], args[3*source+2], x_lat_right, y_lat_right)
    
        for i in range(npt):
            Btot_x[i] += Btmp_x[i][0]
            Btot_y[i] += Btmp_y[i][0]

    for i in range(npt):
        dy = y_lat_right[2][0]-y_lat_right[1][0]
        Fy4 += dy * (Btot_y[i]*Btot_x[i])/mu0
        Fx4 += 0.5 * dy * (Btot_x[i]**2 - Btot_y[i]**2)/mu0

    Fx = Fx1 + Fx2 + Fx3 + Fx4
    Fy = Fy1 + Fy2 + Fy3 + Fy4
    
    #print (Fx1)
    #print (Fx2)
    #print (Fx3)
    #print (Fx4)
    
    #print (Fy1)
    #print (Fy2)
    #print (Fy3)
    #print (Fy4)
    
    return Fx, Fy


def lorentz_force(cent, area, J, list_of_types, *args):
    ''''''
    npt = len(cent)
    n_types = len(list_of_types)
    Fx = 0.0
    Fy = 0.0
    Btot_x = [0] * npt
    Btot_y = [0] * npt
    X = [0] * len(cent)
    Y = [0] * len(cent)
    for i in range(len(X)):
        X[i] = cent[i][0]
        Y[i] = cent[i][1]
    

    for source in range(n_types):
        #print "Type",source
        if list_of_types[source] == 1:
            Btmp_x, Btmp_y = field_from_current(args[3*source], args[3*source+1], args[3*source+2], X, Y)  # Du courant
        else:
            Btmp_x, Btmp_y = field_from_magnetization(args[3*source], args[3*source+1], args[3*source+2], X, Y)
        for i in range(npt):
            Btot_x[i] += Btmp_x[i]
            Btot_y[i] += Btmp_y[i]

    
    for i in range(npt):
        Fx -= J[i] * area[i] * Btot_y[i]
        Fy += J[i] * area[i] * Btot_x[i]
        
        
    return Fx, Fy


    

    

    
    
    
    
    
    