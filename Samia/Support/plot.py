# -*- coding: utf-8 -*-
"""
Created on Thu May 09 23:18:26 2013

@author: Kevin
"""

def plot_square_2val(X, Y, zx, zy, name, *args):
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''


    from numpy import zeros, array
    from math import sqrt
    
    dx = X[0][1]-X[0][0]
    dy = Y[1][0]-Y[0][0]

    from matplotlib.pyplot import quiver, figure, colorbar
    from matplotlib.patches import Polygon
    from matplotlib.collections import PatchCollection
    import pylab
            
    Z = []
    fig = figure()
    ax = fig.add_subplot(111)
    patches = []
    for x in range(len(X)):
        for x2 in range(len(X[0])):
            matrix = zeros((4,2))
            matrix[0,:] = array([X[x][x2]-dx/2.0,Y[x][x2]-dy/2.0])
            matrix[1,:] = array([X[x][x2]+dx/2.0,Y[x][x2]-dy/2.0])
            matrix[2,:] = array([X[x][x2]+dx/2.0,Y[x][x2]+dy/2.0])
            matrix[3,:] = array([X[x][x2]-dx/2.0,Y[x][x2]+dy/2.0])
            polygon = Polygon(matrix, True)
            patches.append(polygon)
            Z.append(sqrt(zx[x][x2]**2+zy[x][x2]**2))
    Z = array(Z)
    p = PatchCollection(patches)
    p.set_array(Z)
    ax.add_collection(p)
    colorbar(p)
    
    if args:
        p.set_clim(0.0,args[0])
        
    # quiver part
    x = zeros(len(X)*len(X[0]))
    y = zeros(len(X)*len(X[0]))
    for i in range(len(X)):
        for j in range(len(X[0])):
            x[i*len(X[0])+j] = X[i][j]
            y[i*len(X[0])+j] = Y[i][j]
            
            if args:
                ratio = args[0]/Z[i*len(X[0])+j]
                if ratio<1.0:
                    zx[i][j] *= ratio
                    zy[i][j] *= ratio
            
    
    quiver(x,y,zx,zy , linewidth=0.1)
            
    pylab.axis('equal')
    pylab.ylabel('Y component (m)')
    pylab.xlabel('X component (m)')
    pylab.title(name)
    
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    
    
def plot_square(X, Y, zin, name):
    
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
    '''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''

    from numpy import zeros, array
    
    dx = X[0][1]-X[0][0]
    dy = Y[1][0]-Y[0][0]

    from matplotlib.pyplot import figure, colorbar
    from matplotlib.patches import Polygon
    from matplotlib.collections import PatchCollection
    import pylab
            
    Z = []
    fig = figure()
    ax = fig.add_subplot(111)
    patches = []
    for x in range(len(X)):
        for x2 in range(len(X[0])):
            matrix = zeros((4,2))
            matrix[0,:] = array([X[x][x2]-dx/2.0,Y[x][x2]-dy/2.0])
            matrix[1,:] = array([X[x][x2]+dx/2.0,Y[x][x2]-dy/2.0])
            matrix[2,:] = array([X[x][x2]+dx/2.0,Y[x][x2]+dy/2.0])
            matrix[3,:] = array([X[x][x2]-dx/2.0,Y[x][x2]+dy/2.0])
            polygon = Polygon(matrix, True)
            patches.append(polygon)
            Z.append(zin[x][x2])
    Z = array(Z)
    p = PatchCollection(patches)
    p.set_array(Z)
    ax.add_collection(p)
    colorbar(p)
    
    pylab.axis('equal')
    pylab.ylabel('Y component (m)')
    pylab.xlabel('X component (m)')
    pylab.title(name)
    
def plot_mesh_data_2val(mesh, datax, datay, name):
        ''' Plots the solution for either : - H-Field
                                            - Magnetization
                                            - Hc-Field due to current
                                            - B : Magnetic Flux Density
        '''
        from numpy import transpose, zeros, sqrt
        from matplotlib.pyplot import quiver, figure, colorbar
        from matplotlib.patches import Polygon
        from matplotlib.collections import PatchCollection
        import pylab
        
        data = [0]*len(datax)
        for i in range(len(datax)):
            data[i] = sqrt(datax[i]**2+datay[i]**2)
        
        Z = transpose(data)

        fig = figure()

        ax = fig.add_subplot(111)
        patches = []
        for x in range(int(mesh.n_ele)):
            matrix = zeros((3,2))
            for i in range(3):
                matrix[i,:] = mesh.node[mesh.ele[x][i]]
            polygon = Polygon(matrix, True)
            patches.append(polygon)
        p = PatchCollection(patches)
        p.set_array(Z)
        ax.add_collection(p)
        colorbar(p)
        
        # quiver part
        x = zeros(mesh.n_ele)
        y = zeros(mesh.n_ele)
        for i in range(int(mesh.n_ele)):
            x[i] = mesh.cent[i][0]
            y[i] = mesh.cent[i][1]
        
        quiver(x,y,datax,datay , linewidth=0.1)

        pylab.axis('equal')
        pylab.ylabel('Y component (m)')
        pylab.xlabel('X component (m)')
        pylab.title(name)
        
def plot_mesh_data(mesh, data, name):
        ''' Plots the solution for either : - H-Field
                                            - Magnetization
                                            - Hc-Field due to current
                                            - B : Magnetic Flux Density
        '''
        from numpy import transpose, zeros
        from matplotlib.pyplot import figure, colorbar
        from matplotlib.patches import Polygon
        from matplotlib.collections import PatchCollection
        import pylab
        
        Z = transpose(data)

        fig = figure()

        ax = fig.add_subplot(111)
        patches = []
        for x in range(int(mesh.n_ele)):
            matrix = zeros((3,2))
            for i in range(3):
                matrix[i,:] = mesh.node[mesh.ele[x][i]]
            polygon = Polygon(matrix, True)
            patches.append(polygon)
        p = PatchCollection(patches)
        p.set_array(Z)
        ax.add_collection(p)
        colorbar(p)
        
        pylab.axis('equal')
        pylab.ylabel('Y component (m)')
        pylab.xlabel('X component (m)')
        pylab.title(name)