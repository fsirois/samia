# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 23:08:23 2013

@author: ratelle
"""

def filter_data(E_old):

    E = [0]*len(E_old)
    
    for i in range(len(E_old)):
        if i==0:
            E[0] = E_old[0]#E[0] = (2*E_old[0]+E_old[1])/3.0
        elif i==(len(E_old)-1):
            E[-1] = E_old[-1]#E[-1] = (2*E_old[-1]+E_old[-2])/3.0
        else:
            E[i] = (E_old[i-1]+E_old[i+1]+2*E_old[i])/4.0

    return E