# -*- coding: utf-8 -*-
"""
Created on Thu Jun 06 13:43:01 2013

@author: Kevin
"""

from pylab import show

from Samia.MagneticField.field_from_source import field_from_current, field_from_magnetization
from Samia.MagneticField.method_of_moments import find_magnetization
from Samia.Superconductor import mesh_superconductor, update_candidates

def Superconductor_with_MagneticMaterial(sc, mag, mag_info, *args):
    ''' '''

    pm = args[0]
    M = args[1]
    material = mag_info[0]
    lin_val = mag_info[1]
    tol = mag_info[2]

    mesh_sc = mesh_superconductor(sc.X_sc, sc.Y_sc)
    
    J = [0] * len(sc.J)
    for i in range(len(J)):
        J[i] = sc.J[i] * sc.Jc
    mesh_sc.set_J(J)
    
    verify_J = [0] * len(sc.X_sc)
    for i in range(sc.ny_sc):
        verify_J[i] = [0.0] * sc.nx_sc
            
    convergence = False
    iteration = 0
    
    while convergence == False:
        iteration += 1
        #print "Iteration on materials :",iteration
        for i in range(sc.ny_sc):
            for j in range(sc.nx_sc):
                verify_J[i][j] = sc.J[i][j]
        

        # Calcul du champ B généré par la magnétisation

        X = [0] * mag.n_ele
        Y = [0] * mag.n_ele
        for i in range(mag.n_ele):
            X[i] = mag.cent[i][0]
            Y[i] = mag.cent[i][1]

        B_source_X, B_source_Y = field_from_magnetization(pm.node, pm.ele, M, X, Y)
        B_sc_X, B_sc_Y = field_from_current(mesh_sc.node, mesh_sc.ele, mesh_sc.J, X, Y)

        Bx = [0] * mag.n_ele
        By = [0] * mag.n_ele
        for i in range(mag.n_ele):
            Bx[i] = (B_sc_X[i] + B_source_X[i])
            By[i] = (B_sc_Y[i] + B_source_Y[i])

        sol = find_magnetization(mag, [Bx, By], tol, material, lin_val)
        
        
        # Descemte du sc ------------------------------------------------------
        M_mag = [0] * mag.n_ele
        for i in range(mag.n_ele):
            M_mag[i] = [sol.Mx[i], sol.My[i]]
            
        Bm_x, Bm_y = field_from_magnetization(mag.node, mag.ele, M_mag, sc.X_air, sc.Y_air) # B de l'aimant
        Bc_x, Bc_y = field_from_magnetization(pm.node,pm.ele, M, sc.X_air, sc.Y_air)
        
        Bt_x = [0] * len(Bm_x)
        Bt_y = [0] * len(Bm_y)
        for i in range(len(Bm_x)):
            Bt_x[i] = [0.0] * len(Bm_x[0])
            Bt_y[i] = [0.0] * len(Bm_x[0])
            for j in range(len(Bm_x[0])):
                Bt_x[i][j] = Bm_x[i][j] + Bc_x[i][j]
                Bt_y[i][j] = Bm_y[i][j] + Bc_y[i][j]
        
        
        sc.set_J_to_old()
        sc.reset_current_field()
        sc.reset_temp_current_field()
        
        update_candidates(sc)

        #sc.generate_energy_weight(mag, sol)
        sc.set_B_source(Bt_x, Bt_y) # Champ source appliqué au modèle
    
        sc.minimize()        # Minimisation de l'énergie
    
        mesh_sc = mesh_superconductor(sc.X_sc, sc.Y_sc)
        J = [0] * len(sc.J)
        for i in range(len(J)):
            J[i] = sc.J[i] * sc.Jc
        mesh_sc.set_J(J)
        

        convergence = True
        for i in range(sc.ny_sc):
            for j in range(sc.nx_sc):
                if verify_J[i][j] != sc.J[i][j]:
                    convergence = False
        
        if iteration >= 10:
            convergence = True
            
    sc.set_old_J()
    sc.set_B_old()
    
    '''
    J_total = [0] * (len(J) + len(mesh_sc.J))
    for i in range(con.n_ele):
        J_total[i] = J[i]
        
    for i in range(mesh_sc.n_ele):
        J_total[i+con.n_ele] = mesh_sc.J[i]
              
    #con.add_mesh(mesh_sc)
    #mesh_sc.plot_current()
    #con.plot()
    '''
    show()
        
    return sc, mesh_sc, sol
	
	
	