# -*- coding: utf-8 -*-
"""
Created on Thu May 16 17:11:06 2013

@author: Kevin
"""

from Samia.Superconductor.superconductor_support.superconductor_support import double_array_sum

def minimization_algorithm(superConductor):
     
    if not hasattr(superConductor, 'front_plus'):
        superConductor.front_plus = initiate_front(superConductor)
        superConductor.front_minus = initiate_front(superConductor)
        
    #--------------------------------------------------------------------------
    # Definitions
    #--------------------------------------------------------------------------
    ny = len(superConductor.J)    # Number of squares in the Y axis
    nx = len(superConductor.J[0]) # Number of squares in the X axis

    current = [1, -1]
    possible_to_minimize = True
    iteration = 0.0
    
    #--------------------------------------------------------------------------
    # Loop on all currents
    #--------------------------------------------------------------------------
    while possible_to_minimize == True:
        
        iteration+=1
            
        initial_energy = energy(superConductor) # Actual energy
        # Energy minimum for one current ring *********************************

        
        for test_current in current:
            
            minimum_energy = initial_energy
            #print minimum_energy
            ene_set = False
            
            for i in range(ny):

                for j in range(nx):


                    check = False
                    if test_current == 1:
                        if superConductor.front_plus[i][j] == 1:
                            check = True
                            superConductor.reset_temp_current_field()
                            superConductor.update_temp_current_field([i], [j], [test_current])
                            test_energy = energy_temp(superConductor)
                            
                    else:
                        if superConductor.front_minus[i][j] == 1:
                            check = True
                            superConductor.reset_temp_current_field()
                            superConductor.update_temp_current_field([i], [j], [test_current])
                            test_energy = energy_temp(superConductor)
                            

                    if ene_set==False and check==True:

                            ene_set=True
                            minimum_energy = test_energy # Set of minimum energy
                            if test_current > 0:
                                xPosition1 = j
                                yPosition1 = i
                            else:
                                xPosition2 = j
                                yPosition2 = i
                            
                    # Version 1    
                    if (ene_set == True) and (test_energy < minimum_energy):
                        
                        minimum_energy = test_energy
                        
                        if test_current > 0:
                            xPosition1 = j
                            yPosition1 = i
                        else:
                            xPosition2 = j
                            yPosition2 = i

        
        # Update of the field and energy calculation
        superConductor.reset_temp_current_field()
        superConductor.update_temp_current_field([yPosition1,yPosition2], [xPosition1,xPosition2], current)
        test_energy = energy_temp(superConductor)
            


        # Set of chosen current ***********************************************
        if (test_energy < initial_energy):

            # Set the current
            superConductor.J[yPosition1][xPosition1] += current[0]
            superConductor.J[yPosition2][xPosition2] += current[1]

            # Updates
            superConductor.update_current_field([yPosition1,yPosition2], [xPosition1,xPosition2], current)
            update_candidates(superConductor)

        else:
            possible_to_minimize = False
            
    return superConductor

def initiate_front(superConductor):
    
    front = [0] * superConductor.ny_sc
    for i in range(superConductor.ny_sc):
        front[i] = [0] * superConductor.nx_sc
        for j in range(superConductor.nx_sc):
            
            if i==0 or i==(superConductor.ny_sc-1):
                front[i][j] = 1
            
            if j==0 or j==(superConductor.nx_sc-1):
                front[i][j] = 1
                
    return front
    
    
def energy(superConductor):

    BX_tot_temp = superConductor.BX_diff + superConductor.BX_current
    BY_tot_temp = superConductor.BY_diff + superConductor.BY_current
    
    BX_tot_temp *= BX_tot_temp
    BY_tot_temp *= BY_tot_temp
    
    e = double_array_sum(BX_tot_temp+BY_tot_temp)
    return e

def energy_temp(superConductor):

    BX_tot_temp = superConductor.BX_diff + superConductor.BX_current_temp
    BY_tot_temp = superConductor.BY_diff + superConductor.BY_current_temp
    
    BX_tot_temp *= BX_tot_temp
    BY_tot_temp *= BY_tot_temp
    
    e = double_array_sum(BX_tot_temp+BY_tot_temp)

    return e
        
def update_candidates(superConductor):            

    if not hasattr(superConductor, 'front_plus'):
        superConductor.front_plus = initiate_front(superConductor)
        superConductor.front_minus = initiate_front(superConductor)
        
    for i in range(superConductor.ny_sc):
        for j in range(superConductor.nx_sc):

            superConductor.front_plus[i][j] = 0
            superConductor.front_minus[i][j] = 0
            
            # Border check
            if superConductor.J[i][j] != 1:
                if i==0 or j==0 or i == (superConductor.ny_sc-1) or j == (superConductor.nx_sc-1):
                    superConductor.front_plus[i][j] = 1

                else:
                    if (i - 1)>=0:
                        if superConductor.J[i-1][j]==1:
                            superConductor.front_plus[i][j] = 1
                    if (i + 1)<superConductor.ny_sc:
                        if superConductor.J[i+1][j]==1:
                            superConductor.front_plus[i][j] = 1
                    if (j - 1)>=0:
                        if superConductor.J[i][j-1]==1:
                            superConductor.front_plus[i][j] = 1
                    if (j + 1)<superConductor.nx_sc:
                        if superConductor.J[i][j+1]==1:
                            superConductor.front_plus[i][j] = 1   
                            
                            # Border check
            if superConductor.J[i][j] != -1:
                if i==0 or j==0 or i == (superConductor.ny_sc-1) or j == (superConductor.nx_sc-1):
                    superConductor.front_minus[i][j] = 1

                else:
                    if (i - 1)>=0:
                        if superConductor.J[i-1][j]==-1:
                            superConductor.front_minus[i][j] = 1
                    if (i + 1)<superConductor.ny_sc:
                        if superConductor.J[i+1][j]==-1:
                            superConductor.front_minus[i][j] = 1
                    if (j - 1)>=0:
                        if superConductor.J[i][j-1]==-1:
                            superConductor.front_minus[i][j] = 1
                    if (j + 1)<superConductor.nx_sc:
                        if superConductor.J[i][j+1]==-1:
                            superConductor.front_minus[i][j] = 1
                            
                            
            if superConductor.J[i][j] == -1:                    
                if (i - 1)>=0:
                    if superConductor.J[i-1][j]==0:
                        superConductor.front_plus[i][j] = 1
                if (i + 1)<superConductor.ny_sc:
                    if superConductor.J[i+1][j]==0:
                        superConductor.front_plus[i][j] = 1
                if (j - 1)>=0:
                    if superConductor.J[i][j-1]==0:
                        superConductor.front_plus[i][j] = 1
                if (j + 1)<superConductor.nx_sc:
                    if superConductor.J[i][j+1]==0:
                        superConductor.front_plus[i][j] = 1   

            if superConductor.J[i][j] == 1:                    
                if (i - 1)>=0:
                    if superConductor.J[i-1][j]==0:
                        superConductor.front_minus[i][j] = 1
                if (i + 1)<superConductor.ny_sc:
                    if superConductor.J[i+1][j]==0:
                        superConductor.front_minus[i][j] = 1
                if (j - 1)>=0:
                    if superConductor.J[i][j-1]==0:
                        superConductor.front_minus[i][j] = 1
                if (j + 1)<superConductor.nx_sc:
                    if superConductor.J[i][j+1]==0:
                        superConductor.front_minus[i][j] = 1 
#--------------------------------- EOF ----------------------------------------

