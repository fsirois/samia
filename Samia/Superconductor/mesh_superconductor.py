# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:49:29 2013

@author: Kevin
"""

from Samia.Mesh.Mesh2D.mesh_2D import mesh2D

class mesh_superconductor(mesh2D):
    
    def __init__(self, X, Y):
        
        dx = X[0][1]-X[0][0]
        dy = Y[1][0]-Y[0][0]
        self.ele = [0] * 2*len(X[0])*len(X)
        self.node = [0]  * (len(X)+1)*(len(X[0])+1)
        self.n_ele = len(self.ele)
        self.n_node = len(self.node)
        
        self.J = [0] * self.n_ele
        
        for i in range(len(X)+1):
            for j in range(len(X[0])+1):
                pos_x = X[0][0] +dx*j - dx/2.0
                pos_y = Y[0][0] +dy*i - dy/2.0
                self.node[i*(len(X[0])+1)+j] = [pos_x, pos_y]
        
        for i in range(len(X)):
            for j in range(len(X[0])):
                self.ele[i*len(X[0])+j] = [i*(len(X[0])+1)+j,  (i+1)*(len(X[0])+1)+j+1, (i+1)*(len(X[0])+1)+j]
                self.ele[i*len(X[0])+j + len(X[0])*len(X)] = [i*(len(X[0])+1)+j, i*(len(X[0])+1)+j+1, (i+1)*(len(X[0])+1)+j+1]
                
        self.create_edges()
        self.add_cent()
        self.add_area()
        
    def set_J(self, J):
        for i in range(len(J)):
            for j in range(len(J[0])):
                self.J[i*len(J[0])+j] = J[i][j]
                self.J[i*len(J[0])+j+len(J[0])*len(J)] = J[i][j]
    
    def plot_current(self):
        from plot import plot_mesh_data
        plot_mesh_data(self, self.J, 'Current Density [A]')
        

      
class sub_mesh(mesh_superconductor):
    
    def __init__(self, x, y, dx, dy, J):
        
        n = len(x)
        
        self.ele = [0] * 2 * n
        self.node = [0]  * 4 * n
        self.n_ele = 2 * n
        self.n_node = 4 * n
        
        self.node[0] = [x[0] - dx/2.0, y[0] - dy/2.0]
        self.node[1] = [x[0] - dx/2.0, y[0] + dy/2.0]
        self.node[2] = [x[0] + dx/2.0, y[0] + dy/2.0]
        self.node[3] = [x[0] + dx/2.0, y[0] - dy/2.0]
        
        self.ele[0] = [0, 3, 1]
        self.ele[1] = [1, 3, 2]
        
        if n==2:
            self.node[4] = [x[1] - dx/2.0, y[1] - dy/2.0]
            self.node[5] = [x[1] - dx/2.0, y[1] + dy/2.0]
            self.node[6] = [x[1] + dx/2.0, y[1] + dy/2.0]
            self.node[7] = [x[1] + dx/2.0, y[1] - dy/2.0]
            
            self.ele[2] = [4, 7, 5]
            self.ele[3] = [5, 7, 6]

  