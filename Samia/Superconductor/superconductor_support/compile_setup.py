from distutils.core import setup, Extension

import numpy as np
 
extensions = [Extension('superconductor_support', sources = ['superconductor_support.c'],
				include_dirs=['.',np.get_include()])]
 
setup (ext_modules = extensions)