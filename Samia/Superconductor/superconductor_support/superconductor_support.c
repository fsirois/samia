#include "Python.h"

#include <numpy/arrayobject.h>

//--------------------------------------------------
PyObject* double_array_sum(PyObject* self, PyObject* args)
{
	PyArrayObject *matrix;
	PyArg_ParseTuple(args, "O", &matrix);

	double* matrix_pointer = (double *) matrix->data;
	const int m = matrix->dimensions[0];
	const int n = matrix->dimensions[1];

	// Algorithm ------------
	double sum = 0.0;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			sum += *(matrix_pointer + i * n + j);
		}
	}
	//-----------------------

	matrix_pointer = NULL;

	return Py_BuildValue("d", sum);
}

//--------------------------------------------------
static PyObject* weighted_double_array_sum(PyObject* self, PyObject* args)
{
	PyArrayObject *matrix, *weight;
	PyArg_ParseTuple(args, "OO", &matrix, &weight);

	double* matrix_pointer = (double *)matrix->data;
	double* weight_pointer = (double *)weight->data;
	const int m = matrix->dimensions[0];
	const int n = matrix->dimensions[1];

	// Algorithm ------------
	double sum = 0;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			const int index = i * n + j;
			sum += matrix_pointer[index] * weight_pointer[index];
		}
	}
	//-----------------------

	matrix_pointer = NULL;
	weight_pointer = NULL;

	return Py_BuildValue("d", sum);

}

//--------------------------------------------------
static PyObject* reset_array(PyObject* self, PyObject* args)
{
	PyArrayObject *J, *J_temp;
	PyArg_ParseTuple(args, "OO", &J, &J_temp);

	double* J_pointer = (double*) J->data;
	double* J_pointer_temp = (double*) J_temp->data;
	const int m = J->dimensions[0];
	const int n = J->dimensions[1];

	// Algorithm ------------
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			const int index = i * n + j;
			J_pointer_temp[index] = J_pointer[index];
		}
	}
	//-----------------------

	J_pointer = NULL;
	J_pointer_temp = NULL;

	Py_RETURN_NONE;
}

//--------------------------------------------------
static PyObject* get_elements(PyObject* self, PyObject* args)
{
	int m_B, n_B, n_main;
	int i_sc, j_sc, n_sc;
	PyArrayObject *B_mat, *main_mat;
	int J_new;
	double *B_mat_pointer, *main_mat_pointer;

	PyArg_ParseTuple(args, "OOiiii", &B_mat, &main_mat, &i_sc, &j_sc, &n_sc, &J_new);

	B_mat_pointer = (double *)B_mat->data;
	main_mat_pointer = (double *)main_mat->data;

	m_B = B_mat->dimensions[0];
	n_B = B_mat->dimensions[1];
	n_main = main_mat->dimensions[1];

	// Algorithm ------------
	const int pos_sc = i_sc * n_sc + j_sc;
	for (int i = 0; i < m_B; i++)
	{
		for (int j = 0; j < n_B; j++)
		{
			const int pos_air = i * n_B + j;
			B_mat_pointer[pos_air] += J_new * main_mat_pointer[pos_air * n_main + pos_sc];
		}
	}
	//-----------------------

	B_mat_pointer = NULL;
	main_mat_pointer = NULL;

	Py_RETURN_NONE;
}

//
// Module definition
//
PyDoc_STRVAR(get_elements_doc, "TODO");
PyDoc_STRVAR(reset_array_doc, "TODO");
PyDoc_STRVAR(double_array_sum_doc, "TODO");
PyDoc_STRVAR(weighted_double_array_doc, "TODO");

static PyMethodDef superconductor_support_functions[] = {
	{"get_elements", (PyCFunction) get_elements, METH_VARARGS, get_elements_doc},
	{"reset_array", (PyCFunction) reset_array, METH_VARARGS, reset_array_doc},
	{"double_array_sum", (PyCFunction) double_array_sum, METH_VARARGS, double_array_sum_doc},
	{"weighted_double_array_sum", (PyCFunction) weighted_double_array_sum, METH_VARARGS, weighted_double_array_doc},
	{ NULL, NULL, 0, NULL }
};

int exec_superconductor_support(PyObject *module) {
	PyModule_AddFunctions(module, superconductor_support_functions);

	PyModule_AddStringConstant(module, "__author__", "kevin");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2019);

	return 0; /* success */
}

// Documentation for superconductor_support.
PyDoc_STRVAR(superconductor_support_doc, "The superconductor_support module");


static PyModuleDef_Slot superconductor_support_slots[] = {
	{ Py_mod_exec, exec_superconductor_support },
	{ 0, NULL }
};

static PyModuleDef superconductor_support_def = {
	PyModuleDef_HEAD_INIT,
	"superconductor_support",
	superconductor_support_doc,
	0,              /* m_size */
	NULL,           /* m_methods */
	superconductor_support_slots,
};

PyMODINIT_FUNC PyInit_superconductor_support()
{
	return PyModuleDef_Init(&superconductor_support_def);
}
