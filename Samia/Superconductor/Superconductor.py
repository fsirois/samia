# -*- coding: utf-8 -*-
"""
Created on Thu May 16 11:41:02 2013

@author: Kevin
"""

from matplotlib.pylab import show
from numpy import linspace, meshgrid, zeros, ones
from numpy import array

from Samia.Superconductor.superconductor_support.superconductor_support import reset_array, get_elements

class superconductor:
    

    #--------------------------------------------------------------------------
    # Constructor of the class superconductor
    #--------------------------------------------------------------------------
    def __init__(self, geom, nx, ny):
        multiplicator_x = 2
        multiplicator_y = 6
        
        # Creating the grid ---------------------------------------------------
        xmin = geom[0][0]
        xmax = geom[0][1]
        ymin = geom[1][0]
        ymax = geom[1][1]
        
        #ratio = (xmax-xmin)/float(ymax-ymin)
        
        #print ratio
        
        #if ratio >= 4:
            #multiplicator_y *= 3 
        #if ratio <= 0.25:
            #multiplicator_x *= 2
        
        print (multiplicator_x)
        print (multiplicator_y)
        x_step = (xmax - xmin) / float(nx)
        y_step = (ymax - ymin) / float(ny)
        
        x_sc = linspace(xmin + 0.5*x_step, xmax - 0.5*x_step, nx)
        y_sc = linspace(ymin + 0.5*y_step, ymax - 0.5*y_step, ny)
        

        xmin_air = xmin - 0.25 * multiplicator_x * (xmax-xmin) + 0.5*x_step
        xmax_air = xmax + 0.25 * multiplicator_x * (xmax-xmin) - 0.5*x_step
        ymin_air = ymin - 0.25 * multiplicator_y * (ymax-ymin) + 0.5*y_step
        ymax_air = ymax + 0.25 * multiplicator_y * (ymax-ymin) - 0.5*y_step
        x_air = linspace(xmin_air, xmax_air, multiplicator_x*nx)
        y_air = linspace(ymin_air, ymax_air, multiplicator_y*ny)
        
        [self.X_air, self.Y_air] = meshgrid(x_air, y_air)
        [self.X_sc,self.Y_sc] = meshgrid(x_sc, y_sc)
        
        self.limits = [xmin, xmax, ymin, ymax]
        
        self.nx_air = len(self.X_air[0])
        self.ny_air = len(self.X_air)
        self.nx_sc = len(self.X_sc[0])
        self.ny_sc = len(self.X_sc)
        

        self.J = zeros((self.ny_sc,self.nx_sc))
        self.old_J = zeros((self.ny_sc, self.nx_sc))
        
        self.BX_source = zeros((self.ny_air,self.nx_air))
        self.BY_source = zeros((self.ny_air,self.nx_air))
        self.BX_source_old = zeros((self.ny_air,self.nx_air))
        self.BY_source_old = zeros((self.ny_air,self.nx_air))
        self.BX_diff = zeros((self.ny_air,self.nx_air))
        self.BY_diff = zeros((self.ny_air,self.nx_air)) 
        
        self.BX_current_temp = zeros((self.ny_air,self.nx_air))
        self.BY_current_temp = zeros((self.ny_air,self.nx_air))
        self.BX_current = zeros((self.ny_air,self.nx_air))
        self.BY_current = zeros((self.ny_air,self.nx_air))
        
        self.energy_weight = ones((self.ny_air,self.nx_air)) 
        
        #self.create_matrix()
        
    def translate(self, vector):
                
        dx = vector[0]
        dy = vector[1]
        
        self.limits[0] += dx
        self.limits[1] += dx
        self.limits[2] += dy
        self.limits[3] += dy

        for i in range(self.ny_air):
            for j in range(self.nx_air):
                self.X_air[i][j] += dx
                self.Y_air[i][j] += dy

        for i in range(self.ny_sc):
            for j in range(self.nx_sc):
                self.X_sc[i][j] += dx
                self.Y_sc[i][j] += dy

    def generate_energy_weight(self, mesh, sol):
        ''''''
        node = array(mesh.node)
        ele = array(mesh.ele)
        for i in range(len(self.X_air)):
            for j in range(len(self.X_air[0])):
                self.weight[i][j] = (find_mur(sol, mesh, node, ele, [self.X_air[i][j],self.Y_air[i][j]]))**-1
        
        
    def create_matrix(self):
        
        from numpy import zeros
        
        dx = self.X_sc[0][1]-self.X_sc[0][0] # X length of element
        dy = self.Y_sc[1][0]-self.Y_sc[0][0] # Y length of element


        n_air = len(self.X_air)*len(self.X_air[0])
        n_sc = len(self.X_sc) * len(self.X_sc[0])
        self.matrix_x = zeros((n_air, n_sc))
        self.matrix_y = zeros((n_air, n_sc))
        
        from Samia.MagneticField.field_from_source import field_from_current
        from Samia.Superconductor.mesh_superconductor import sub_mesh

        for y_sc in range(len(self.X_sc)):
            print ("Line",y_sc,"of",len(self.X_sc))
            for x_sc in range(len(self.X_sc[0])):
                pos_sc = y_sc * len(self.X_sc[0]) + x_sc
                
                sub_mesh_obj = sub_mesh([self.X_sc[y_sc][x_sc]], [self.Y_sc[y_sc][x_sc]], dx, dy, self.Jc)

                sub_J = [self.Jc, self.Jc]

                BX, BY = field_from_current(sub_mesh_obj.node, sub_mesh_obj.ele, sub_J, self.X_air, self.Y_air)

                for y_air in range(len(self.X_air)):
                    for x_air in range(len(self.X_air[0])):
                        
                        pos_air = y_air * len(self.X_air[0]) + x_air
                        
                        self.matrix_x[pos_air][pos_sc] = BX[y_air][x_air]
                        self.matrix_y[pos_air][pos_sc] = BY[y_air][x_air]

    #--------------------------------------------------------------------------
    # Minimizing energy algorithm
    #--------------------------------------------------------------------------
    def minimize(self):
        from Samia.Superconductor.functional_minimization import minimization_algorithm
        self = minimization_algorithm(self)
    
    
    #--------------------------------------------------------------------------
    # Setting the source field
    #--------------------------------------------------------------------------
    def set_B_source(self, BX, BY):
        # Version 1

        for i in range(len(self.BX_source)):
            for j in range(len(self.BX_source[0])):
                self.BX_source[i][j] = BX[i][j]
                self.BY_source[i][j] = BY[i][j]

        for i in range(len(self.BX_source)):
            for j in range(len(self.BX_source[0])):
                if self.X_air[i][j]<self.limits[0] or self.X_air[i][j]>self.limits[1] or self.Y_air[i][j]<self.limits[2] or self.Y_air[i][j]>self.limits[3]:
                    self.BX_diff[i][j] = BX[i][j] - self.BX_source_old[i][j]
                    self.BY_diff[i][j] = BY[i][j] - self.BY_source_old[i][j]
                else:
                    self.BX_diff[i][j] = BX[i][j] - self.BX_source_old[i][j]
                    self.BY_diff[i][j] = BY[i][j] - self.BY_source_old[i][j]
                


    #--------------------------------------------------------------------------
    # Setting critical current density
    #--------------------------------------------------------------------------
    def set_Jc(self,Jc):
        self.Jc = Jc
                
    def set_B_old(self):
        
        for i in range(len(self.BX_source)):
            for j in range(len(self.BX_source[0])):
                self.BX_source_old[i][j] = self.BX_source[i][j]
                self.BY_source_old[i][j] = self.BY_source[i][j]
        
    def set_J_to_old(self):
        for i in range(len(self.X_sc)):
            for j in range(len(self.X_sc[0])):
                self.J[i][j] = self.old_J[i][j]
    
    def set_old_J(self):
        for i in range(len(self.X_sc)):
            for j in range(len(self.X_sc[0])):
                self.old_J[i][j] = self.J[i][j]
                
                
    #--------------------------------------------------------------------------
    # Resetting current front
    #--------------------------------------------------------------------------
        
    def reset_current(self):
        self.J = [0] * len(self.X_sc)
        for i in range(len(self.X_sc)):
            self.J[i] = [0.0] * len(self.X_sc[0])
            
    def reset_current_field(self):
        for i in range(len(self.X_air)):
            for j in range(len(self.X_air[0])):
                self.BX_current[i][j] = 0.0
                self.BY_current[i][j] = 0.0
        self.reset_temp_current_field()
        
    def reset_temp_current_field(self):
        reset_array(self.BX_current, self.BX_current_temp)
        reset_array(self.BY_current, self.BY_current_temp)


    def update_temp_current_field(self, i_sc, j_sc, J_new):
        for i in range(len(i_sc)):
            get_elements(self.BX_current_temp, self.matrix_x, i_sc[i], j_sc[i], len(self.X_sc[0]), J_new[i])
            get_elements(self.BY_current_temp, self.matrix_y, i_sc[i], j_sc[i], len(self.X_sc[0]), J_new[i])
        
        
    def update_current_field(self, i_sc, j_sc, J_new):
        get_elements(self.BX_current, self.matrix_x, i_sc[0], j_sc[0], len(self.X_sc[0]), J_new[0])
        get_elements(self.BY_current, self.matrix_y, i_sc[0], j_sc[0], len(self.X_sc[0]), J_new[0])
        get_elements(self.BX_current, self.matrix_x, i_sc[1], j_sc[1], len(self.X_sc[0]), J_new[1])
        get_elements(self.BY_current, self.matrix_y, i_sc[1], j_sc[1], len(self.X_sc[0]), J_new[1])
        
        
        
    '''************************************************************************
    *** PRINT TOOLS ***********************************************************
    ************************************************************************'''
    def plot_current(self):
        from Samia.Support.plot import plot_square
        plot_square(self.X_sc, self.Y_sc, self.J, 'Current Density')
        
    def plot_field(self):
        from Samia.Support.plot import plot_square_2val
        plot_square_2val(self.X_air, self.Y_air, self.BX_current+self.BX_diff, self.BY_current+self.BY_diff, 'Field')

#-------------------------------- EOF -----------------------------------------



