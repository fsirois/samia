# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
SYMMETRIES
This set of functions provides tools to take advantage of symmetries in an
electrical machine.
-------------------------------------------------------------------------------
FUNCTIONS
- build_m_sym : Builds the main matrix to solve the problem, but includes the
                effect of all 'submeshes' since only a part of the back-iron
                is meshed.
                
- gen_complete_solution : Once the solution is known on one pole, it is
                          possible to report the solution on a new mesh that
                          includes all the poles in the machine. This is what
                          we do. Once this 'new' solution is built, B can be
                          found anywhere in space.
-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation date : August 9, 2011
Last modification : August 25, 2011
Written at : Advanced Magnet Lab
----------------------------------------------------------------------------'''
'''-----------------------------------------------------------------------------
--------------------------------------------------------------------------------
SYMMETRIES
This set of functions provides tools to take advantage of symmetries in an
electrical machine.
--------------------------------------------------------------------------------
FUNCTIONS
- build_m_sym : Builds the main matrix to solve the problem, but includes the
                effect of all 'submeshes' since only a part of the back-iron
                is meshed.
- gen_complete_solution : Once the solution is known on one pole, it is possible
                          to report the solution on a new mesh that includes
                          all the poles in the machine. This is what we do.
                          Once this 'new' solution is built, B can be found
                          anywhere in space.
--------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation date : July 29, 2011
Last Modification : August 21, 2011
Location : Advanded Magnet Lab, Melbourne, FLorida
-----------------------------------------------------------------------------'''

###############################################################################
# Build special matrix for symmetries
###############################################################################
def build_m_sym(sol, mesh, n_sym_rot=0, sym_z=False):
    ''' Builds the main matrix N which includes all information on geometry and
        takes into account the symmetries.
        
        INPUTS :
         - Sol : Solution Object
         - Mesh : Mesh of one Pole
         - N : number of pair of poles
         - option_symmetry : tells how to use the symmetries
                option_symmetry == 2 --> all symmetries
                option_symmetry == 1 --> only Z symmetry
         - mat_gen : tells which to be run in C and which to be run in python
                mat_gen == 2 --> Main Matrix is built in C
                mat_gen == 1 --> Main Matrix in Python, but submatrices in C
                mat_gen == 0 --> Main Matrix is built in Python
            
        OUTPUTS :
         - Sol.N : Main Matrix containing information on geometry
                   (including symmetries cases)
    '''
    
    # Import from Python
    import numpy as np
    import copy
    
    dim = sol.dim
    
    #--------------------------------------------------------------------------
    # NO SYMMETRY
    #--------------------------------------------------------------------------
    # Create a copy of the solution object
    sol_temp = copy.deepcopy(sol)
    
    # build the main_matrix for own mesh
    sol_temp.build_m(mesh, mesh)
    sol.N = sol_temp.N

    #--------------------------------------------------------------------------
    # Z SYMMETRY
    #--------------------------------------------------------------------------    
    if sym_z == True and dim==3:
        mesh2 = copy.deepcopy(mesh)
    
        # create de matrix
        for i in range(mesh2.n_node):
            mesh2.node[i][2] = -mesh2.node[i][2]
        sol_temp.build_m(mesh, mesh2)
        
        # arrange the matrix for the good signal source
        for i in range(mesh2.n_ele):
            sol_temp.N[:,3*i+2] = -sol_temp.N[:,3*i+2]
        sol.N += sol_temp.N


    #--------------------------------------------------------------------------
    # Y SYMMETRY
    #--------------------------------------------------------------------------
    if n_sym_rot >= 1:
        
        # Copy the mesh
        mesh2 = copy.deepcopy(mesh)

        # Make a mirror of the nodes
        for i in range(mesh2.n_node):
            mesh2.node[i][1] = -mesh2.node[i][1]
        
        # Reorganize nodes anticlockwise
        if dim==2:
            for i in range(mesh2.n_ele):
                tmp = mesh2.ele[i][2]
                mesh2.ele[i][2] = mesh2.ele[i][1]
                mesh2.ele[i][1] = tmp
        
        # Build the matrix
        sol_temp.build_m(mesh, mesh2)
        
        # arrange the matrix for the good signal source
        for i in range(mesh2.n_ele):
            sol_temp.N[:,dim*i+1] = -sol_temp.N[:,dim*i+1]
            
        sol.N += sol_temp.N
        
        # Z symmetry of Y symmetry ********************************************
        if sym_z == True and dim==3:
            
            # Z symmetry of the Y symmetry
            mesh2 = copy.deepcopy(mesh)
    
            for i in range(mesh2.n_node):
                mesh2.node[i][1] = -mesh2.node[i][1]
                mesh2.node[i][2] = -mesh2.node[i][2]
            sol_temp.build_m(mesh, mesh2)
            
            # arrange the matrix for the good signal source
            for i in range(mesh2.n_ele):
                sol_temp.N[:,3*i+1] = -sol_temp.N[:,3*i+1]
                sol_temp.N[:,3*i+2] = -sol_temp.N[:,3*i+2]
                
            sol.N += sol_temp.N
    
    #----------------------------------------------------------------------
    # OTHER SYMMETRIES
    #----------------------------------------------------------------------
    if n_sym_rot >= 2:
        for sym in range(n_sym_rot-1):     

            angle = (sym+1)*2*np.pi / n_sym_rot

            c = np.cos(angle)
            s = np.sin(angle)
                
            #--------------------------------------------------------------
            # add copies of the main mesh (rotations)
            #--------------------------------------------------------------
            mesh2 = copy.deepcopy(mesh)

            # translate nodes
            # no need for reorganize nodes anticlockwise since its a translation
            for i in range(mesh2.n_node):
                mesh2.node[i][0] = c * mesh.node[i][0] - s * mesh.node[i][1]
                mesh2.node[i][1] = s * mesh.node[i][0] + c * mesh.node[i][1]
                
            # create main matrix
            sol_temp.build_m(mesh, mesh2)
            
            # arrange the matrix for the good signal source
            for i in range(mesh2.n_ele):
                for j in range(mesh2.n_ele):
                    a = sol_temp.N[dim*j][dim*i]
                    b = sol_temp.N[dim*j][dim*i+1]
                    d = sol_temp.N[dim*j+1][dim*i]
                    e = sol_temp.N[dim*j+1][dim*i+1]
                    
                    sol_temp.N[dim*j][dim*i]     = a*c + b*s
                    sol_temp.N[dim*j][dim*i+1]   = b*c - a*s
                    sol_temp.N[dim*j+1][dim*i]   = d*c + e*s
                    sol_temp.N[dim*j+1][dim*i+1] = e*c - d*s
                    
                    if dim==3:
                        g = sol_temp.N[3*j+2][3*i]
                        h = sol_temp.N[3*j+2][3*i+1]
                        
                        sol_temp.N[3*j+2][3*i]   = g*c + h*s
                        sol_temp.N[3*j+2][3*i+1] = h*c - g*s
                    
            # add effect to the PRINCIPAL main matrix
            sol.N += sol_temp.N
            
            
            # Z symmetry ******************************************************
            if sym_z == True and dim==3:

                mesh2 = copy.deepcopy(mesh)
    
                for i in range(mesh2.n_node):
                    mesh2.node[i][0] = s * mesh.node[i][0] - s * mesh.node[i][1]
                    mesh2.node[i][1] = s * mesh.node[i][0] + c * mesh.node[i][1]
                    mesh2.node[i][2] = -mesh2.node[i][2]
                sol_temp.build_m(mesh, mesh2)
                
                # arrange the matrix for the good signal source
                for i in range(mesh2.n_ele):
                    for j in range(mesh2.n_ele):
                        a = sol_temp.N[3*j][3*i]
                        b = sol_temp.N[3*j][3*i+1]
                        d = sol_temp.N[3*j+1][3*i]
                        e = sol_temp.N[3*j+1][3*i+1]
                        g = sol_temp.N[3*j+2][3*i]
                        h = sol_temp.N[3*j+2][3*i+1]
                        
                        sol_temp.N[3*j][3*i]     = a*c + b*s
                        sol_temp.N[3*j][3*i+1]   = b*c - a*s
                        sol_temp.N[3*j+1][3*i]   = d*c + e*s
                        sol_temp.N[3*j+1][3*i+1] = e*c - d*s
                        sol_temp.N[3*j+2][3*i]   = g*c + h*s
                        sol_temp.N[3*j+2][3*i+1] = h*c - g*s
                    sol_temp.N[:,3*i+2] = -sol_temp.N[:,3*i+2]
                        
                sol.N += sol_temp.N

            
            #--------------------------------------------------------------
            # add copies of the main mesh's mirror (rotations)
            #--------------------------------------------------------------           
            mesh2 = copy.deepcopy(mesh)

            # translate nodes for mesh's Y mirror
            for i in range(mesh2.n_node):
                mesh2.node[i][0] = c * mesh.node[i][0] + s * mesh.node[i][1]
                mesh2.node[i][1] = s * mesh.node[i][0] - c * mesh.node[i][1]
            
            # reorganize nodes anticlockwise
            if dim==2:
                for i in range(mesh2.n_ele):
                    tmp = mesh2.ele[i][2]
                    mesh2.ele[i][2] = mesh2.ele[i][1]
                    mesh2.ele[i][1] = tmp
                
            # create main matrix
            sol_temp.build_m(mesh, mesh2)
            
            # arrange the matrix for the good signal source
            for i in range(mesh2.n_ele):
                for j in range(mesh2.n_ele):
                    a = sol_temp.N[dim*j][dim*i]
                    b = sol_temp.N[dim*j][dim*i+1]
                    d = sol_temp.N[dim*j+1][dim*i]
                    e = sol_temp.N[dim*j+1][dim*i+1]
                    
                    sol_temp.N[dim*j][dim*i]     = a*c + b*s
                    sol_temp.N[dim*j][dim*i+1]   = -b*c + a*s
                    sol_temp.N[dim*j+1][dim*i]   = d*c + e*s
                    sol_temp.N[dim*j+1][dim*i+1] = -e*c + d*s
                    
                    if dim==3:
                        g = sol_temp.N[3*j+2][3*i]
                        h = sol_temp.N[3*j+2][3*i+1]
                        
                        sol_temp.N[3*j+2][3*i]   = g*c + h*s
                        sol_temp.N[3*j+2][3*i+1] = -h*c + g*s
                    
            # add the effect to the PRINCIPAL main matrix
            sol.N += sol_temp.N

            # Z symmetry ******************************************************
            if sym_z == True and dim==3:
                mesh2 = copy.deepcopy(mesh)
                for i in range(mesh2.n_node):
                    mesh2.node[i][0] = c * mesh.node[i][0] + s * mesh.node[i][1]
                    mesh2.node[i][1] = s * mesh.node[i][0] - c * mesh.node[i][1]
                    mesh2.node[i][2] = -mesh2.node[i][2]
                sol_temp.build_m(mesh, mesh2)
                
                # arrange the matrix for the good signal source
                for i in range(mesh2.n_ele):
                    for j in range(mesh2.n_ele):
                        a = sol_temp.N[3*j][3*i]
                        b = sol_temp.N[3*j][3*i+1]
                        d = sol_temp.N[3*j+1][3*i]
                        e = sol_temp.N[3*j+1][3*i+1]
                        g = sol_temp.N[3*j+2][3*i]
                        h = sol_temp.N[3*j+2][3*i+1]
                        
                        sol_temp.N[3*j][3*i]     = a*c + b*s
                        sol_temp.N[3*j][3*i+1]   = -b*c + a*s
                        sol_temp.N[3*j+1][3*i]   = d*c + e*s
                        sol_temp.N[3*j+1][3*i+1] = -e*c + d*s
                        sol_temp.N[3*j+2][3*i]   = g*c + h*s
                        sol_temp.N[3*j+2][3*i+1] = -h*c + g*s
                        
                    sol_temp.N[:,3*i+2] = -sol_temp.N[:,3*i+2]
                    
                sol.N += sol_temp.N

    return sol

###############################################################################
# Generate a complete solution on a complete mesh
###############################################################################
def gen_complete_solution(mesh, sol, n_sym_rot=0, sym_z=False):
    ''' Generate the solution for the COMPLETE back-iron when the solution is
        only known for one pole.
        
        INPUTS :
            - mesh : mesh of one pole
            - sol : solution object, containing the solution for one pole
            - n : number of symmetries
        
        OUTPUTS :
            - mesh : new mesh including all poles
            - sol : new solution including all poles
    '''
    
    import copy
    from numpy import array, cos, sin, pi
    dim = sol.dim
    
    #--------------------------------------------------------------------------
    # Change solution from array to list
    #--------------------------------------------------------------------------
    sol.Hx = sol.Hx.tolist()
    sol.Hy = sol.Hy.tolist()
    sol.Hnorm = sol.Hnorm.tolist()
    
    sol.Hc_x = sol.Hc_x.tolist()
    sol.Hc_y = sol.Hc_y.tolist()
    sol.Hc_norm = sol.Hc_norm.tolist()
    
    sol.Bx = sol.Bx.tolist()
    sol.By = sol.By.tolist()
    sol.Bnorm = sol.Bnorm.tolist()
    
    sol.Mx = sol.Mx.tolist()
    sol.My = sol.My.tolist()
    sol.Mnorm = sol.Mnorm.tolist()
    
    sol.xi = sol.xi.tolist()
    
    if dim==3:
        sol.Hz = sol.Hz.tolist()
        sol.Hc_z = sol.Hc_z.tolist()
        sol.Bz = sol.Bz.tolist()
        sol.Mz = sol.Mz.tolist()
    
    if type(mesh.node) is not list:
        mesh.node = mesh.node.tolist()
    if type(mesh.ele) is not list:
        mesh.ele = mesh.ele.tolist()

    # Memorize mesh
    mesh_mem = copy.deepcopy(mesh)
    
    #--------------------------------------------------------------------------
    # Z SYMMETRY
    #--------------------------------------------------------------------------
    
    if sym_z == True:

        mesh2 = copy.deepcopy(mesh_mem)
    
        # create de matrix
        for i in range(mesh2.n_node):
            mesh.node.append([mesh2.node[i][0], mesh2.node[i][1], -mesh2.node[i][2]])
        
        for i in range(mesh2.n_ele):
            mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                             mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                             
            sol.Bx.append(sol.Bx[i])
            sol.By.append(sol.By[i])
            sol.Bz.append(-sol.Bz[i])
            
            sol.Hx.append(sol.Hx[i])
            sol.Hy.append(sol.Hy[i])
            sol.Hz.append(-sol.Hz[i])
            
            sol.Hc_x.append(sol.Hc_x[i])
            sol.Hc_y.append(sol.Hc_y[i])
            sol.Hc_z.append(-sol.Hc_z[i])
            
            sol.Mx.append(sol.Mx[i])
            sol.My.append(sol.My[i])
            sol.Mz.append(-sol.Mz[i])
            
            sol.xi.append(sol.xi[i])
                                   
        mesh.n_node += mesh2.n_node
        mesh.n_ele += mesh2.n_ele

    #----------------------------------------------------------------------
    # Y SYMMETRY
    #----------------------------------------------------------------------
    if n_sym_rot >= 1:
        # Copy of first mesh
        mesh2 = copy.deepcopy(mesh_mem)

        # Make a symmetry of the nodes
        for i in range(mesh2.n_node):
            if dim==3:
                mesh.node.append([mesh2.node[i][0], -mesh2.node[i][1], mesh2.node[i][2]])
            else:
                mesh.node.append([mesh2.node[i][0], -mesh2.node[i][1]])
        
        # Organize nodes anticlockwise
        if dim==2:
            for i in range(mesh2.n_ele):
                tmp = mesh2.ele[i][2]
                mesh2.ele[i][2] = mesh2.ele[i][1]
                mesh2.ele[i][1] = tmp
            
        # Change the elements, centroids and solution
        for i in range(mesh2.n_ele):
            # elements
            if dim==3:
                mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                               mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
            else:
                 mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                               mesh2.ele[i][2]+mesh.n_node])
                               
            sol.Bx.append(sol.Bx[i])
            sol.By.append(-sol.By[i])

            sol.Hx.append(sol.Hx[i])
            sol.Hy.append(-sol.Hy[i])
                           
            sol.Hc_x.append(sol.Hc_x[i])
            sol.Hc_y.append(-sol.Hc_y[i])

            sol.Mx.append(sol.Mx[i])
            sol.My.append(-sol.My[i])

            sol.xi.append(sol.xi[i])
        
            if dim==3:
                sol.Bz.append(sol.Bz[i])
                sol.Hz.append(sol.Hz[i]) 
                sol.Hc_z.append(sol.Hc_z[i])
                sol.Mz.append(sol.Mz[i])

            
        mesh.n_node += mesh2.n_node
        mesh.n_ele += mesh2.n_ele

        
        if sym_z == True:
            # Z symmetry of the Y symmetry
            mesh2 = copy.deepcopy(mesh_mem)
    
            for i in range(mesh2.n_node):
                mesh.node.append([mesh2.node[i][0], -mesh2.node[i][1], -mesh2.node[i][2]])
                
            for i in range(mesh2.n_ele):
                mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                   mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                sol.Bx.append(sol.Bx[i])
                sol.By.append(-sol.By[i])
                sol.Bz.append(-sol.Bz[i])
                
                sol.Hx.append(sol.Hx[i])
                sol.Hy.append(-sol.Hy[i])
                sol.Hz.append(-sol.Hz[i])
                
                sol.Hc_x.append(sol.Hc_x[i])
                sol.Hc_y.append(-sol.Hc_y[i])
                sol.Hc_z.append(-sol.Hc_z[i])
                
                sol.Mx.append(sol.Mx[i])
                sol.My.append(-sol.My[i])
                sol.Mz.append(-sol.Mz[i])
                
                sol.xi.append(sol.xi[i])
                                   
            mesh.n_node += mesh2.n_node
            mesh.n_ele += mesh2.n_ele

    #----------------------------------------------------------------------
    # OTHER SYMMETRIES
    #----------------------------------------------------------------------
    if n_sym_rot >= 2:
        for sym in range(n_sym_rot-1):     

            angle = (sym+1)*2*pi / n_sym_rot
                
            #--------------------------------------------------------------
            # add copies of the main mesh (rotations)
            #--------------------------------------------------------------
            mesh2 = copy.deepcopy(mesh_mem)

            for i in range(mesh2.n_node):
                temp_a = cos(angle) * mesh2.node[i][0] - sin(angle) * mesh2.node[i][1]
                temp_b = sin(angle) * mesh2.node[i][0] + cos(angle) * mesh2.node[i][1]
                if dim==3:
                    mesh.node.append([temp_a, temp_b, mesh2.node[i][2]])
                else:
                    mesh.node.append([temp_a, temp_b])
                
                
                
            for i in range(mesh2.n_ele):
                
                if dim==3:
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                     mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                else:
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                     mesh2.ele[i][2]+mesh.n_node])
                                     
                sol.Bx.append(cos(angle)*sol.Bx[i] - sin(angle)*sol.By[i])
                sol.By.append(sin(angle)*sol.Bx[i] + cos(angle)*sol.By[i])
                
                sol.Hx.append(cos(angle)*sol.Hx[i] - sin(angle)*sol.Hy[i])
                sol.Hy.append(sin(angle)*sol.Hx[i] + cos(angle)*sol.Hy[i])
                
                sol.Hc_x.append(cos(angle)*sol.Hc_x[i] - sin(angle)*sol.Hc_y[i])
                sol.Hc_y.append(sin(angle)*sol.Hc_x[i] + cos(angle)*sol.Hc_y[i])
                
                sol.Mx.append(cos(angle)*sol.Mx[i] - sin(angle)*sol.My[i])
                sol.My.append(sin(angle)*sol.Mx[i] + cos(angle)*sol.My[i])
                
                sol.xi.append(sol.xi[i])
                
                if dim==3:
                    sol.Bz.append(sol.Bz[i])
                    sol.Hz.append(sol.Hz[i])
                    sol.Hc_z.append(sol.Hc_z[i])
                    sol.Mz.append(sol.Mz[i])
                               
            mesh.n_node += mesh2.n_node
            mesh.n_ele += mesh2.n_ele

            #----- for the Z symmetry -----------------------------------------
            if sym_z == True:
                
                mesh2 = copy.deepcopy(mesh_mem)
    
                for i in range(mesh2.n_node):
                    temp_a = cos(angle) * mesh2.node[i][0] - sin(angle) * mesh2.node[i][1]
                    temp_b = sin(angle) * mesh2.node[i][0] + cos(angle) * mesh2.node[i][1]
                    mesh.node.append([temp_a, temp_b, -mesh2.node[i][2]])
                    
                for i in range(mesh2.n_ele):
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                   mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                    sol.Bx.append(cos(angle)*sol.Bx[i] - sin(angle)*sol.By[i])
                    sol.By.append(sin(angle)*sol.Bx[i] + cos(angle)*sol.By[i])
                    sol.Bz.append(-sol.Bz[i])
                    
                    sol.Hx.append(cos(angle)*sol.Hx[i] - sin(angle)*sol.Hy[i])
                    sol.Hy.append(sin(angle)*sol.Hx[i] + cos(angle)*sol.Hy[i])
                    sol.Hz.append(-sol.Hz[i])
                    
                    sol.Hc_x.append(cos(angle)*sol.Hc_x[i] - sin(angle)*sol.Hc_y[i])
                    sol.Hc_y.append(sin(angle)*sol.Hc_x[i] + cos(angle)*sol.Hc_y[i])
                    sol.Hc_z.append(-sol.Hc_z[i])
                    
                    sol.Mx.append(cos(angle)*sol.Mx[i] - sin(angle)*sol.My[i])
                    sol.My.append(sin(angle)*sol.Mx[i] + cos(angle)*sol.My[i])
                    sol.Mz.append(-sol.Mz[i])
                    
                    sol.xi.append(sol.xi[i])
                                   
                mesh.n_node += mesh2.n_node
                mesh.n_ele += mesh2.n_ele
            
            #--------------------------------------------------------------
            # add copies of the main mesh's mirror (rotations)
            #--------------------------------------------------------------           
            mesh2 = copy.deepcopy(mesh_mem)

            for i in range(mesh2.n_node):
                temp_a = cos(angle) * mesh.node[i][0] + sin(angle) * mesh.node[i][1]
                temp_b = sin(angle) * mesh.node[i][0] - cos(angle) * mesh.node[i][1]
                
                if dim==3:
                    mesh.node.append([temp_a, temp_b, mesh2.node[i][2]])
                else:
                    mesh.node.append([temp_a, temp_b])
            
            if dim==2:
                for i in range(mesh2.n_ele):
                    tmp = mesh2.ele[i][2]
                    mesh2.ele[i][2] = mesh2.ele[i][1]
                    mesh2.ele[i][1] = tmp
                
            for i in range(mesh2.n_ele):
                if dim==3:
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                     mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                else:
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                     mesh2.ele[i][2]+mesh.n_node])
                               
                sol.Bx.append(cos(angle)*sol.Bx[i] + sin(angle)*sol.By[i])
                sol.By.append(sin(angle)*sol.Bx[i] - cos(angle)*sol.By[i])
                
                sol.Hx.append(cos(angle)*sol.Hx[i] + sin(angle)*sol.Hy[i])
                sol.Hy.append(sin(angle)*sol.Hx[i] - cos(angle)*sol.Hy[i])
                
                sol.Hc_x.append(cos(angle)*sol.Hc_x[i] + sin(angle)*sol.Hc_y[i])
                sol.Hc_y.append(sin(angle)*sol.Hc_x[i] - cos(angle)*sol.Hc_y[i])
                
                sol.Mx.append(cos(angle)*sol.Mx[i] + sin(angle)*sol.My[i])
                sol.My.append(sin(angle)*sol.Mx[i] - cos(angle)*sol.My[i])
                
                sol.xi.append(sol.xi[i])
    
                if dim==3:
                    sol.Bz.append(sol.Bz[i])
                    sol.Hz.append(sol.Hz[i])
                    sol.Hc_z.append(sol.Hc_z[i])
                    sol.Mz.append(sol.Mz[i])
            mesh.n_node += mesh2.n_node
            mesh.n_ele += mesh2.n_ele

            #----- for the Z symmetry -----------------------------------------
            if sym_z == True:
                
                mesh2 = copy.deepcopy(mesh_mem)
                for i in range(mesh2.n_node):
                    temp_a = cos(angle) * mesh.node[i][0] + sin(angle) * mesh.node[i][1]
                    temp_b = sin(angle) * mesh.node[i][0] - cos(angle) * mesh.node[i][1]
                    mesh.node.append([temp_a, temp_b, -mesh2.node[i][2]])
                    
                for i in range(mesh2.n_ele):
                    mesh.ele.append([mesh2.ele[i][0]+mesh.n_node, mesh2.ele[i][1]+mesh.n_node,
                                   mesh2.ele[i][2]+mesh.n_node, mesh2.ele[i][3]+mesh.n_node])
                    sol.Bx.append(cos(angle)*sol.Bx[i] + sin(angle)*sol.By[i])
                    sol.By.append(sin(angle)*sol.Bx[i] - cos(angle)*sol.By[i])
                    sol.Bz.append(-sol.Bz[i])
                    
                    sol.Hx.append(cos(angle)*sol.Hx[i] + sin(angle)*sol.Hy[i])
                    sol.Hy.append(sin(angle)*sol.Hx[i] - cos(angle)*sol.Hy[i])
                    sol.Hz.append(-sol.Hz[i])
                    
                    sol.Hc_x.append(cos(angle)*sol.Hc_x[i] + sin(angle)*sol.Hc_y[i])
                    sol.Hc_y.append(sin(angle)*sol.Hc_x[i] - cos(angle)*sol.Hc_y[i])
                    sol.Hc_z.append(-sol.Hc_z[i])
    
                    sol.Mx.append(cos(angle)*sol.Mx[i] + sin(angle)*sol.My[i])
                    sol.My.append(sin(angle)*sol.Mx[i] - cos(angle)*sol.My[i])
                    sol.Mz.append(-sol.Mz[i])
                    
                    sol.xi.append(sol.xi[i])
                                   
                mesh.n_node += mesh2.n_node
                mesh.n_ele += mesh2.n_ele
    
    
    #--------------------------------------------------------------------------
    # Return mesh lists to arrays
    #--------------------------------------------------------------------------
    mesh.node = array(mesh.node)
    mesh.ele = array(mesh.ele)
    mesh.create_edges()
    mesh.add_cent()
    
    #--------------------------------------------------------------------------
    # Return solution lists to arrays
    #--------------------------------------------------------------------------
    sol.Mx = array(sol.Mx)
    sol.My = array(sol.My)
    
    sol.Bx = array(sol.Bx)
    sol.By = array(sol.By)
    
    sol.Hx = array(sol.Hx)
    sol.Hy = array(sol.Hy)
    
    sol.Hc_x = array(sol.Hc_x)
    sol.Hc_y = array(sol.Hc_y)
    
    if dim==2:
        sol.Mnorm = (sol.Mx**2 + sol.My**2)**0.5
        sol.Bnorm = (sol.Bx**2 + sol.By**2)**0.5
        sol.Hnorm = (sol.Hx**2 + sol.Hy**2)**0.5
        sol.Hc_norm = (sol.Hc_x**2 + sol.Hc_y**2)**0.5
    else:
        sol.Mz = array(sol.Mz)
        sol.Bz = array(sol.Bz)
        sol.Hz = array(sol.Hz)
        sol.Hc_z = array(sol.Hc_z)
        sol.Mnorm = (sol.Mx**2 + sol.My**2 + sol.Mz**2)**0.5
        sol.Bnorm = (sol.Bx**2 + sol.By**2 + sol.Bz**2)**0.5
        sol.Hnorm = (sol.Hx**2 + sol.Hy**2 + sol.Hz**2)**0.5
        sol.Hc_norm = (sol.Hc_x**2 + sol.Hc_y**2 + sol.Hc_z**2)**0.5

    return mesh, sol

'''------------------------------- EOF -------------------------------------'''




