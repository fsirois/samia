# -*- coding: utf-8 -*-
"""
Created on Sun Jun 23 21:21:13 2013

@author: ratelle
"""

def materials(material, *args):
    
    if material=='Iron':
        H_data = [0., 663.146, 1067.5, 1705.23, 2463.11, 3841.67, 5425.75, 
                  7957.75, 12298.3, 20462.8, 32169.6, 61213.4, 111408.,
                  175070., 261469., 318310.]
                      
        B_data = [0., 1., 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.,
                  2.1, 2.2, 2.3, 2.4]
        
    elif material=='Hard-Iron':
        H_data = [0., 372.5, 464.5, 592.5, 778.5, 1084.5, 1626.6, 2639.5,
                  4412.5, 7221., 11583., 14679., 16000.]
                      
        B_data = [0., 1., 1.1, 1.2, 1.3, 1.4, 1.5, 1.6,
                  1.7, 1.8, 1.9, 1.95, 1.97]
                          
    elif material=='Linear':
        from math import pi
        mu0 = 4* pi * 10**-7
        mu = args[0]*mu0
        H_data = [0., float(10**6)]
        B_data = [0., float(10**6)*mu]
    
    return H_data, B_data