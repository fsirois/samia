# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
SOLUTION CLASS 3D
Class that stores and generates solution for the Magnetization of a Body
inside a Magnetic Field.
-------------------------------------------------------------------------------
FUNCTIONS
    
    1. __init__ : initialization of solution

    2. build_m : main matrix N construction

    3. build_k : K matrix construction (modifies N to include susceptibility)

    4. solve : solves the linear problem for a given susceptibility, also gives
                the convergence value for the gfun method
    
    5. find_chi : finds new susceptibility, given a magnetic field in material
      
    6. plot : plots a given solution on a given plane along the Z, Y or Z axis

    7. Find_Anywhere : Finds field anywhere in space from a given magnetization

-------------------------------------------------------------------------------
Created By : Kevin Ratelle
Created at : École Polytechnique de Montréal
Date Created : July 11, 2011
Last mod : August 25, 2011
----------------------------------------------------------------------------'''
'''-----------------------------------------------------------------------------
--------------------------------------------------------------------------------
SOLUTION CLASS for magnetization of iron
Class that stores and generates solution for the Magnetization of a Body
inside a Magnetic Field.
--------------------------------------------------------------------------------
MAIN FUNCTIONS :
    __init___ -> Initialize Solution
    build_m   -> builds the main matrix according to geometry
    build_k   -> modifies m matrix to include the CHI in the matrix
    solve     -> Actually solves the problem (H, B, Hm, Hc)
    find_xi   -> Finds the new CHI (susceptibility) according to H-Field
    plot      -> Plots the solution on the mesh         
--------------------------------------------------------------------------------
Note on compiled C Functions :
    A function was programmed in C language for speed. (MainMatrix2D)
    If one wants to use the C-Function and they are not compiled, he just has
    to run the compiler.py main function and it will call a batch file to
    automatically compile the C-Code. One must use Windows and have the cmingw
    compiler installed for the compilation works.
--------------------------------------------------------------------------------
Created By : Kevin Ratelle
Created at : École Polytechnique de Montréal
Date Created : June 29, 2011
Last mod : June 10, 2011
-----------------------------------------------------------------------------'''

class solution:

    #--------------------------------------------------------------------------
    # Initialization of  Object
    #--------------------------------------------------------------------------
    def __init__(self,mesh):
        '''
        Initialization of the solution, it only sets the magnetization and the
        magnetic field inside material to zero
        
        INPUT : mesh --> mesh on which the solution will be found
        '''
        from numpy import zeros
        
        self.dim = len(mesh.node[0])
        
        self.Hnorm = zeros(mesh.n_ele)
        self.Hx = zeros(mesh.n_ele)
        self.Hy = zeros(mesh.n_ele)
        
        self.Mnorm = zeros(mesh.n_ele)
        self.Mx = zeros(mesh.n_ele)
        self.My = zeros(mesh.n_ele)

        if self.dim==3:
            self.Hz = zeros(mesh.n_ele)
            self.Mz = zeros(mesh.n_ele)
        
        self.xi = zeros(mesh.n_ele)


    #--------------------------------------------------------------------------
    # Build Main Matrix M
    #--------------------------------------------------------------------------
    # mesh1 is affected mesh while mesh2 is the affecting mesh
    def build_m(self,mesh1,mesh2): 
        '''
        Creation of the main matrix to solve the problem. It contains all
        information on the geometry. Main matrix is called N
        
        INPUTS : mesh1 --> mesh on which we want to solution
                 mesh2 --> magnetized mesh affecting the mesh on which we want
                           the solution. When we do not use symmetries, these
                           two meshes are the same one
        '''
        
        # Import from Python
        import numpy as np
        
        #----------------------------------------------------------------------
        # Build Matrix from ALL C-Code
        #----------------------------------------------------------------------

          
        if type(mesh2.node) is not list:
            mesh2.node = mesh2.node.tolist()
        if type(mesh2.ele) is not list:
            mesh2.ele = mesh2.ele.tolist()
        if type(mesh1.cent) is not list:
            mesh1.cent = mesh1.cent.tolist()

        if self.dim==3:
            from Samia.MagneticField.extensions.MethodOfMoments import BuildMatrix3D
            self.N = np.array(BuildMatrix3D(mesh2.node, mesh2.ele, mesh1.cent))
        else:
            from Samia.MagneticField.extensions.MethodOfMoments import BuildMatrix2D
            self.N = np.array(BuildMatrix2D(mesh2.node, mesh2.ele, mesh1.cent))
        
    #--------------------------------------------------------------------------
    # Build K matrix to solve (only adds the local susceptibility to M)
    #--------------------------------------------------------------------------
    def build_k(self):
        '''
        Modification of the main matrix N into K, where K contains the effect
        of susceptibility and there is a -1 along the diagonal for the solution
        is the signal source when susceptibility is zero everywhere. K is the
        matrix used to solve the problem.
        
        No inputs are needed since the susceptibility is a field of the 
        solution object.
        '''

        import numpy
        
     
        # SOLVE LINE
        self.K = numpy.zeros((len(self.N),len(self.N)))
        
        if self.dim==3:
            mu0 = 4*numpy.pi*10**-7
            nSize = len(self.N)//3
            for k in range(nSize):
                self.K[:,3*k] = self.N[:,3*k]*(self.xi[k]/mu0)
                self.K[:,3*k+1] = self.N[:,3*k+1]*(self.xi[k]/mu0)
                self.K[:,3*k+2] = self.N[:,3*k+2]*(self.xi[k]/mu0)
                self.K[3*k][3*k] -= 1 + self.xi[k]
                self.K[3*k+1][3*k+1] -= 1 + self.xi[k]
                self.K[3*k+2][3*k+2] -= 1 + self.xi[k]
        else:
            # adds the effect of the susceptibility
            self.K = numpy.zeros((len(self.N),len(self.N)))
            for k in range(len(self.N)//2):
                self.K[:,2*k]   = self.N[:,2*k]  * self.xi[k]
                self.K[:,2*k+1] = self.N[:,2*k+1]* self.xi[k]
                
            # adds the effect of the source field    
            self.K -= numpy.eye(len(self.N))

    #--------------------------------------------------------------------------
    # Solve the Problem and Generate the Solutions
    #-------------------------------------------------------------------------- 
    def solve(self):
        '''
        Solves the linear system composed of matrix K and source field Hc.
        It generates the H-Field value inside the material. It then finds
        the magnetic flux inside material and magnetization of material from
        this value and the susceptibility. All data is stored in the
        solution object.
        
        This function also provides the self.diff value, which is the average
        difference in the magnetization for all elements from previous iteration
        of th GFUN method, divided by the maximal magnetization of an element.
        It is written in percentage, and this is the convergence value.
        '''
        from numpy import linalg, zeros
        from math import sqrt, pi

        mu0 = 4*pi*10**-7

       
        import copy
        M_save = copy.deepcopy(self.Mnorm)
        
        #***********
        # Solution *
        #***********
        self.H = linalg.solve(self.K , -self.Hc)

        #*************
        # DATA SAVES *
        #*************
        n = self.dim
        # Saves H (total Magnetic Field)
        self.Hx = zeros(len(self.H)//n)
        self.Hy = zeros(len(self.H)//n)
        self.Hnorm = zeros(len(self.H)//n)
        if n==3:
            self.Hz = zeros(len(self.H)//3)
            
        for i in range(len(self.H)//n):
            #a = 
            #b = 
            self.Hx[i] = self.H[n*i]
            self.Hy[i] = self.H[n*i+1]
            if n==3:
                self.Hz[i] = self.H[3*i+2]
                self.Hnorm[i] = sqrt(self.Hx[i]**2 + self.Hy[i]**2 + self.Hz[i]**2)
            else:
                self.Hnorm[i] = sqrt(self.Hx[i]**2 + self.Hy[i]**2)
            
            
        # Saves B (total Magnetic Flux inside Iron)
        self.Bx = zeros(len(self.H)//n)
        self.By = zeros(len(self.H)//n)
        if n==3:
            self.Bz = zeros(len(self.H)//3)
        self.Bnorm = zeros(len(self.H)//n)
        for i in range(len(self.H)//n):
            self.Bx[i] = self.Hx[i]*(mu0*(self.xi[i]+1))
            self.By[i] = self.Hy[i]*(mu0*(self.xi[i]+1))
            if n==3:
                self.Bz[i] = self.Hz[i]*(mu0*(self.xi[i]+1))
                self.Bnorm[i] = sqrt(self.Bx[i]**2 + self.By[i]**2 + self.Bz[i]**2)
            else:
                self.Bnorm[i] = sqrt(self.Bx[i]**2 + self.By[i]**2)
        
        # Saves M (Magnetization of Metal)
        self.Mx = zeros(len(self.H)//n)
        self.My = zeros(len(self.H)//n)
        if n==3:
            self.Mz = zeros(len(self.H)//3)
        self.Mnorm = zeros(len(self.H)//n)
        for i in range(len(self.H)//n):
            self.Mx[i] = self.Bx[i]/mu0 - self.Hx[i]
            self.My[i] = self.By[i]/mu0 - self.Hy[i]
            if n==3:
                self.Mz[i] = self.Bz[i]/mu0 - self.Hz[i]
                self.Mnorm[i] = sqrt(self.Mx[i]**2 + self.My[i]**2 + self.Mz[i]**2)
            else:
                self.Mnorm[i] = sqrt(self.Mx[i]**2 + self.My[i]**2)

        # Average difference between old and new susceptibilities -------------
        denominator = len(self.Mnorm)*max(self.Mnorm)
        if denominator > 0.0:
            self.diff = sum(abs(M_save[:]-self.Mnorm[:]))/(denominator)*100


    #--------------------------------------------------------------------------
    # This Function Generate New Values for the local susceptibility
    #--------------------------------------------------------------------------           
    def find_chi(self,section, material, *args):
        '''
        Finds the new susceptibility when a H-Field is given for all elements
        of the material. A B-H curve for the material must be provided inside
        the function.
    
        INPUTS : material --> material choice, for the moment the only choices
                              are 'Iron' and 'Linear', but more B-H curves can
                              be implemented in the function
                 lin_val --> when the material choice is 'Linear' the linear
                             relative permeability is asked as a second input
        '''
        from math import pi
        mu0 = 4*pi*10**-7
        from numpy import zeros
        from Samia.MagneticField.method_of_moments.materials import materials
                  
        # Finding the new susceptibility --------------------------------------
        # Values initialisation
        H = self.Hnorm

        xi = zeros(len(self.Hnorm))
        
        lin_count = 0
        
        for mat in range(len(material)):
                
            if args:
                H_data, B_data = materials(material[mat], args[0][lin_count])
            else:
                H_data, B_data = materials(material[mat])
                
            if material[mat] == 'Linear':
                lin_count += 1

            # value calculation
            for k in range(len(self.Hnorm)):
                
                if section[k] == mat:
                    if H[k] == 0:
                        xi[k] = (((H_data[1]/B_data[1])*mu0)**-1) - 1
                        
                    else:
                        closest = 0
                        for i in range(len(H_data)-1):
                            if abs(H[k]-H_data[i]) < abs(H[k]-H_data[closest]):
                                closest = i
                        
                        if (H[k] - H_data[closest]) >= 0:
                            frac = (H[k]-H_data[closest]) / (H_data[closest+1]-H_data[closest])
                            B = B_data[closest] + (B_data[closest+1]-B_data[closest])*frac
                            xi[k] = B/(mu0*H[k]) - 1
                        
                        else:
                            frac = (H_data[closest]-H[k]) / (H_data[closest]-H_data[closest-1])
                            B = B_data[closest] - (B_data[closest]-B_data[closest-1])*frac
                            xi[k] = B/(mu0*H[k]) - 1
        
        self.xi = xi
    

    #--------------------------------------------------------------------------
    # Function Plots required solution
    #--------------------------------------------------------------------------
    def plot(self, mesh, data, *args):
        '''
        Plots the wanted solution on a 2D plane cut into the 3D mesh.
    
        INPUTS : mesh --> Mesh on which the solution is to be plotted
                 axis --> axis of the plane on which to plot choices are
                             ('X','x','Y','y','Z' and 'z')
                             
                 height --> height of the plane to cut the 3D mesh
                 choice --> choice of solution to be plotted, choices are
                             ('M', 'B', 'H','Hc' and 'CHI')
        '''
        if args:
            from Samia.Mesh.Mesh3D.mesh2D_from3D import gen_2D_mesh, sol2D_plot
            #print axis
            mesh2D, sol2D = gen_2D_mesh(mesh, args[0], args[1], self)
            sol2D_plot(mesh2D, sol2D, args[1], args[0], data)
        
        else:
            from Samia.Support.plot import plot_mesh_data_2val, plot_mesh_data
            
            if data == 'H':
                plot_mesh_data_2val(mesh, self.Hx, self.Hy, 'H-Field [A/m]')
            elif data == 'B':
                plot_mesh_data_2val(mesh, self.Bx, self.By, 'B-Field [T]')
            elif data == 'M':
                plot_mesh_data_2val(mesh, self.Mx, self.My, 'Magnetization [A/m]')
            elif data == 'Hc':
                plot_mesh_data_2val(mesh, self.Hc_x, self.Hc_y, 'H from source [A/m]')
            elif data == 'CHI':
                plot_mesh_data(mesh, self.xi, 'Susceptiblity')
            else:
                print ('The chosen option is not allowed, \
                       choices are \'H\',\'B\',\'M\',\'Hc\' or \'CHI\'')
            
'''------------------------ END OF SOLUTION.PY FILE ------------------------'''
                
