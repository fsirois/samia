# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
GFUN
This file calculates the magnetic field inside a magnetic material subject to
the magnetic field caused by current passing in another conductor.

Functions included are :
    find_M : Calculates the magnetization of the back_iron
    
    B_from_M : Calculates the magnetic flux density anywhere in space
                      after magnetization has been computed for the back iron.
    find_mur    --> Finds the relative permeability of the material
-------------------------------------------------------------------------------
Method has been found in :
    Proceedings of the Fourth International Conference on Magnet Technology,
    Brookhaven, USA, PP 617 to 626, 1972
-------------------------------------------------------------------------------
Written by        : Kevin Ratelle
Inspired by       : GFUN.m (June 1, 2011)
Date Created      : June 27, 2011
Last modification : March 22, 2013
Location          : École Polytechnique de Montréal
-------------------------------------------------------------------------------
This work is partially supported by :
     - NASA
     - Advanced Magnet Lab
     - École Polytechnique de Montréal
----------------------------------------------------------------------------'''
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
GFUN 3D
This function calculates the magnetic field inside a magnetic material
subjected to a current passing in another conductor.

Functions included are :
    
    back_iron_M --> Finds the magnetization of a mesh, given a source field.
    B_from_M    --> Finds B from the magnetization of the magnetic material
    find_mur    --> Finds the relative permeability of the material
    
-------------------------------------------------------------------------------
Method was found in :
    Proceedings of the Fourth International Conference on Magnet Technology,
    Brookhaven, USA, PP 617 to 626, 1972
-------------------------------------------------------------------------------
Written by        : Kevin Ratelle
Translated from   : GFUN.m (June 1, 2011)
Date Created      : July 11, 2011
Last modification : August 25, 2011
Location          : École Polytechnique de Montréal
----------------------------------------------------------------------------'''

###############################################################################
# Finding the magnetization of the back iron
###############################################################################
def find_magnetization(mesh, B, tol, material, lin_val=None, n_sym_rot=0, sym_z=False):
    ''' This functions calculates the magnetization of a back-iron in a
        electrical motor. Steps included are :
        1 --> Mesh Generation of the Back-Iron.
    
        2 --> Source-Field Calculation with help of outside functions.
    
        3 --> Generation of the matrix that contains all the information on how
              the magnetization of an element in the mesh of the back-iron
              affects the field inside another element.
    
        4 --> Iteration on solution, calculating magnetization from given
              susceptibility, then susceptibility from given magnetization
              until convergence is reached.
              
        INPUTS : 
            motor  --> Main parameters of the motor.
            rotor  --> All rotor related parameters.
            stator --> All stator related parameters.
            bi     --> Back-Iron parameters.
            test   --> Parameters defining the test that is going on.
            *args  --> Optional arguments
                        None provided : Use the rotor ans stator as source
                        args[0] : Special source asked 
                                    - 'rotor'
                                    - 'sator'
                                    - 'stator_1_phase'
                                    - Anything else leads to rotor and stator
                        args[1] : If 'stator_1_phase' is chosen and args[1]
                                  is provided, tells the phase to chose.
            
        OUTPUTS : mesh --> Mesh of the back-iron
                  sol  --> Solution for the magnetization of the back-iron.
    '''
    from numpy import zeros, sqrt, Inf, pi
    from Samia.MagneticField.method_of_moments.solution import solution
    
    mu0 = 4 * pi * 10**-7
    
    BX = B[0]
    BY = B[1]
    
    n = len(B)
    if n==3:
        BZ = B[2]    
    
    #--------------------------------------------------------------------------
    # Adding source to 'sol' structure
    #--------------------------------------------------------------------------
    sol = solution(mesh)           # initialize the mesh
    
    if lin_val!=None:
        sol.find_chi(mesh.section, material, lin_val)
    else:
        sol.find_chi(mesh.section, material)

    
    # building right-side vector of the linear system to solve
    sol.Hc = zeros(n*mesh.n_ele)
    for i in range(int(mesh.n_ele)):
        sol.Hc[n*i] = BX[i]/mu0
        sol.Hc[n*i+1] = BY[i]/mu0
        if n==3:
            sol.Hc[n*i+2] = BZ[i]/mu0
        
    sol.Hc_x = zeros(mesh.n_ele)
    sol.Hc_y = zeros(mesh.n_ele)
    if n==3:
        sol.Hc_z = zeros(mesh.n_ele)
    sol.Hc_norm = zeros(mesh.n_ele)
    for i in range(int(mesh.n_ele)):
        sol.Hc_x[i] = BX[i]/mu0
        sol.Hc_y[i] = BY[i]/mu0
        if n==3:
            sol.Hc_z[i] = BZ[i]/mu0
            sol.Hc_norm[i] = sqrt(sol.Hc_x[i]**2 + sol.Hc_y[i]**2 + sol.Hc_z[i]**2)
        else:
            sol.Hc_norm[i] = sqrt(sol.Hc_x[i]**2 + sol.Hc_y[i]**2)
    
    #--------------------------------------------------------------------------
    # Main Matrix Construction
    #--------------------------------------------------------------------------
    if n_sym_rot!=0 or sym_z==True:
        from Samia.MagneticField.method_of_moments.symmetry import build_m_sym
        sol = build_m_sym(sol, mesh, n_sym_rot=n_sym_rot, sym_z=sym_z)
    else:
        sol.build_m(mesh,mesh)
    

    #--------------------------------------------------------------------------
    # Loop on Solution
    #--------------------------------------------------------------------------  
    sol.diff = Inf # initialize
    counter = 0    # shows the iteration
    
    # main loop
    while sol.diff>tol:
        
        # Display of current iteration ----------------------------------------
        counter = counter + 1
     
        # recompleting main matrix --------------------------------------------
        # adding new CHI to matrix
        sol.build_k()
    
        # Main Solution Calculations ------------------------------------------
        sol.solve()
        
        # Calculates the new CHI ----------------------------------------------
        if lin_val!=None:
            sol.find_chi(mesh.section, material, lin_val)
        else:
            sol.find_chi(mesh.section, material)
        
        if counter==30:
            sol.diff = 0
        
    sol.K = None
    sol.N = None
    
    return sol

def gen_complete_solution(mesh, sol, n_sym_rot=0, sym_z=False):
    from Samia.MagneticField.method_of_moments.symmetry import gen_complete_solution
    return gen_complete_solution(mesh, sol,n_sym_rot, sym_z)
        
###############################################################################
# Finding mu_r for a point inside the Back Iron
###############################################################################     
def find_mur(mesh, pos, xi):
    ''' TODO - DEPRECATED
        
        Find the relative permeability of the material at a given position.
        
        INPUTS :
            - sol     --> Solution on the given mesh
            - mesh    --> Mesh of the geometry
            - x, y, z --> Position where we want the permeability
            
        OUTPUT :
            - Susceptibility of material at 'x,y,z', if point is outside the
              given mesh, the output will be '1', like air.
    '''
    element = mesh.find_element(pos) # tells which element
    
    if element == -1:
        return 1
    else:
        return (xi[element]+1)
         

def find_value_local(mesh, pos, val_x, val_y, *args):
    '''
    TODO - DEPRECATED
    '''

    if args:
        val_z = args[0]
        
    element = mesh.find_element(pos) # tells which element

    if element == -1:
        if args:
            return 0.0,0.0,0.0
        else:
            return 0.0, 0.0
    else:
        if args:
            return val_x[element], val_y[element], val_z[element]
        else:
            return val_x[element], val_y[element] 

     
'''-------------------------END OF GFUN.PY FILE-----------------------------'''
        