
#pragma once

void FieldFromCurrent3D(double* Bx, double* By, double* Bz, double** node, double** ele, int n_ele, double** J, double *x, double *y,
	double *z, int n_point, int n_node);

void FieldFromCurrent2D(double** H, double** node, int** ele, int n_ele,
	double* J, double** point, int n_point);

void FieldFromMagnetization3D(double** B, double** node, int** ele, int n_ele, double** M, double** point, int n_point);
void FieldFromMagnetization2D(double** B, double** node, int** ele, int n_ele, double** M, double** point, int n_point);

void GFUN_BuildMatrix2D(double *N, double** node, int** ele, int n_ele, double** cent);
void GFUN_BuildMatrix3D(double *M, double** node, int** ele, int n_ele, double** cent);
