
/*
-build_m(N, node, ele, n_ele, cent) :
	build_m stands for : Build of Main Matrix for GFUN method.This is the
	stacking of a lot of 'submat' evaluations.Basically, build_m, does
	not only evaluates the effect of all the elements of a mesh on all
	centroids of this mesh.

	->N_ele stands for number of elements.
	->Ele is a matrix containing all information on elements, or how
	nodes of the mesh are linked.It is a n_eleX3 matrix since all
	elements are triangles.
	->Node is a matrix containing the position of the nodes of the mesh.
	It is a n_node X 2 matrix since all points have 2 coordinates.
	->Cent is a matrix containing the position of the centroids of the
	elements.It is a n_ele X 2 matrix.

	N is a matrix of dimension(2 * n_ele X 2 * n_ele).If multiplied by a
	vector of length(2 * n_ele) containing information on magnetization of
	all elements, it will give the H - Field for all the centroids of the
	mesh.Ex.N*M = H

	In the GFUN method, the construction of this main matrix is the
	central operation.If we recall that M = CHI * H, and that
	H = Hm + Hsource, we can build a linear system.
	*/

#include "FieldFromMagnetization.h"
#include "MagneticField.h"

void GFUN_BuildMatrix2D(double* N, double** node, int** ele,
             int n_ele, double** cent)
{	
	double x0, y0, x[3], y[3];

	// Loop on element feeling the field of all magnetized ones
	for (int k=0; k<n_ele; k++)
	{
		// place where we want the Hm from all other elements
		x0 = cent[k][0];
		y0 = cent[k][1];
		
		// all elements affecting the point
		for (int l=0; l<n_ele; l++)
		{
			double m[2][2] = {{0}};
			// position of  magnetized element
			for (int i=0; i<3; i++)
			{
				x[i] = node[ele[l][i]][0];
				y[i] = node[ele[l][i]][1];
			}
			
			// calculation of submatrix
			submat2D(m, x, y, x0, y0);

			// filling of the main matrix
			for (int i=0; i<2; i++)  //line
			{
				for (int j=0; j<2; j++)  //column
				{
					*(N + 2*n_ele * (2*k + i) + 2*l + j) = m[i][j];
				}
			}
		}
	}
}

//--------------------------------------------------
void GFUN_BuildMatrix3D(double *M, double** node, int** ele,
             int n_ele, double** cent)
{			 
	double ro[3], ri[4][3];

	// Loop on element feeling the field of all magnetized ones
	for (int k=0; k<n_ele; k++)
	{
		// place where we want the Hm from all other elements
		for (int i = 0; i < 3; i++)
		{
			ro[i] = cent[k][i];
		}
		
		// all elements affecting the point
		for (int l=0; l<n_ele; l++)
		{
			double m[3][3] = {{0}};
			// position of  magnetized element
			for (int i=0; i<4; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					ri[i][j] = node[ele[l][i]][j];
				}
			}
			
			// calculation of submatrix 
			submat3D(m, ri, ro);
			
			// filling of the main matrix
			for (int i=0; i<3; i++)  //line
			{
				for (int j=0; j<3; j++) //column
				{
					*(M + 3*n_ele * (3*k + i) + 3*l + j) = m[i][j];
				}
			}
		}
	}
}
