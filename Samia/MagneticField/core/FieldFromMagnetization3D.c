/**----------------------------------------------------------------------------
These functions are intended to calculate the effect of magnetization
of elements on the H-Field.
-------------------------------------------------------------------------------
	- submat(m, ri, ro) :
		  submat stands for submatrix for 3-Dimensional case. Given a
		  tetrahedron, and given the supposition this triangle is uniformly
		  magnetized, submat provides a 3X3 submatrix (m) that, multiplied
		  by the magnetization of the element (M), will give the magnetic
		  field (Hm) caused by the magnetisation at a given point (ro).
		  The triangle is defined by ri, which contains the position of
		  the 4 vertices of the element.
		  
		  For example m * M = Hm

	- build_m(N, node, ele, n_ele, cent) :
             build_m stands for : Build of Main Matrix for GFUN method. This
             is the stacking of a lot of 'submat' evaluations. Basically,
             build_m, does only evaluates the effect of all the elements of a
             mesh on all centroids of this mesh. 
		  
             -> n_ele stands for number of elements.
             -> ele is a matrix containing all information on elements, or how
                nodes of the mesh are linked. It is a n_eleX4 matrix since all
                elements are tetrahedrons.
             -> node is a matrix containing the position of the nodes of the
                mesh. It is a (n_node X 3) matrix since all points have 3
                coordinates.
             -> cent is a matrix containing the position of the centroids of
                the elements. It is a (n_ele X 3) matrix.
			  
             N is a matrix of dimension (3*n_ele X 3*n_ele). If multiplied by a
             vector of length (3*n_ele) containing information on magnetization
             of all elements, it will give the H-Field for all the centroids of
             the mesh. Ex. N*M = H
		  
             In the GFUN method, the construction of this main matrix is the
             central operation. If we recall that M = CHI * H, and that
             H = Hm + Hsource, we can build a linear system.
		  
	- find_b(B, node, ele, n_ele, M, point, n_point) :
		this function does not find matrix of submatrix linking M and Hm. It
		rather directly finds B once given a known magnetization on a mesh.
		
           -> N_ele stands for number of elements.
           -> Ele is a matrix containing all information on elements, or how
              nodes of the mesh are linked. It is a (n_eleX4) matrix since all
              elements are tetrahedrons.
           -> Node is a matrix containing the position of the nodes of the mesh.
              It is a (n_node X 3) matrix since all points have 3 coordinates.
           -> M stands for MAGNETIZATION. It is matrix of dimensions (n_ele*3)
              and gives the magnetization on the mesh once the solution is known
           -> N_point stands for the number of points where we want to find B
           -> point contains information on the coordinates of the points where
              we want B. It is a (n_point X 3) matrix
           -> B is a (n_point X 3) matrix giving the x and y values of B at the 
              given points

	- solid_angle(ri, ro, pt) :
            calculates the solid angle of a tetrahedron defined by ri, as seen
            by a point defined by ro

            ri is a double array of size [4][3], 4 for all apices of
            tetrahedron, and 3 for all coordinates
								
            pt is juste a double array helping to get data inside ri properly
		  
	- delta_W(delta_W, nf, ri, ro) :
            calculates the delta_W vector from Fabbri's paper. delta_W is the
            array where the answer will be written, nf is the normal vector to
            the current face of the tetrahdron observed, ri contains the 4
            apices of the tetrahedron [4][3] array, and ro is the point of
            observation, where the solution is wanted
-------------------------------------------------------------------------------
These analytical equations were found in :
    Massimo Fabbri, IEEE Transactions on Magnetics, vol. 44, no. 1,
    Magnetic Flux Density and Vector Potential of Uniform Polyhedral Sources,
    PP 32 to 36, January 2008
-----------------------------------------------------------------------------*/

#include "CommonMath.h"
#include "MagneticField.h"

// ***** Delta_W vector calculation *****
void deltaW(double delta_W[], double nf[], double ri[][3],
            double ro[], int pt[]){
	
	int next[4] = {0,1,2,0};
	
	for (int j=0; j<3; j++)
	{
		double r1, r2, r2o[3], r1o[3], r21[3];
		for (int k=0; k<3; k++)
		{
			r1 = ri[pt[j]][k];
			r2 = ri[pt[next[j+1]]][k];
			r2o[k] = r2 - ro[k];
			r1o[k] = r1 - ro[k];
			r21[k] = r2 - r1;
		}

		double nr2o = norm(r2o);
		double nr1o = norm(r1o);
		double nr21 = norm(r21);
		
		// sign here is important, it must be conform to normal's sign
		double ue[3];
		for (int k=0; k<3; k++)
		{
			ue[k] = r21[k]/nr21;
		}

		double tmp[3];
		cross(nf , ue, tmp);
		
		double we;
		if ((nr2o + nr1o - nr21) == 0 || (nr2o + nr1o + nr21) == 0)
		{
			we = 0;
		}
		else
		{
			we = log((nr2o + nr1o + nr21) / (nr2o + nr1o - nr21));
		}
		
		for (int k=0; k<3; k++)
		{
			delta_W[k] = delta_W[k] + tmp[k]*we;
		}
	}
	
	// Solid angle calculation
	double Omega = solid_angle(ri,ro,pt);
	
	for (int j=0; j<3; j++)
	{
		delta_W[j] = delta_W[j] + nf[j]*Omega;
	}
}

//--------------------------------------------------
void submat3D(double m[][3], double ri[][3], double ro[])
{
	// Useful values
	int pt[4][3] = {{0,1,2},{0,2,3},{0,3,1},{2,1,3}};
	int next[5]  = {0,1,2,0,1};
	double m_temp[3][3] = {{0}};
	
	// Loop on four faces of given tetrahedron
	int ptout;
	double a[3], b[3], c[3], nf[3], nf_out[3];
    for (int i=0; i<4; i++)
	{
        // Normal-Vector of the given face
		for (int j=0; j<3; j++)
		{
			a[j] = ri[pt[i][1]][j] - ri[pt[i][0]][j];
			b[j] = ri[pt[i][2]][j] - ri[pt[i][1]][j];
		}

		cross(a,b,nf);
		double n = norm(nf);
		for (int j=0; j<3; j++)
		{
			nf[j] = nf[j]/n;
		}
		
		// Normal towards outside of the tetrahedron
		ptout = setdiff(pt[i]);
		int sgn = 1;
		for (int j = 0; j < 3; j++)
		{
			c[j] = ri[ptout][j] - ri[pt[i][0]][j];
		}

		if (dot3D(nf, c) > 0)
		{
			sgn = -1;
		}

		for (int j = 0; j < 3; j++)
		{
			nf_out[j] = sgn * nf[j];
		}
		
		// Delta_W calculation
		double delta_W[3] = {0};
		deltaW(delta_W , nf , ri , ro , pt[i]);
		
		// Matrix update for all coefficients for different components of H
		// (normal must be pointing outsid tetrahedron)
		double t;
		for(int k1=0; k1<3; k1++)
		{
			for (int k2=0; k2<3; k2++)
			{
				if (k1==k2)
				{
					t = nf_out[next[k1+1]] * delta_W[next[k1+1]] + nf_out[next[k1+2]] * delta_W[next[k1+2]];
				}
				else
				{
					t = -nf_out[k2] * delta_W[k1];
				}
				m_temp[k2][k1] = t;
			}
		}
		
		for(int k1=0; k1<3; k1++)
		{
			for(int k2=0; k2<3; k2++)
			{
				m[k1][k2] = m[k1][k2] + m_temp[k1][k2];
			}
		}
	}
	
	// Arrange de matrix for it fits H
	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++)
		{
			m[i][j] = pow(10,-7) * m[i][j];
		}
	}
}


//************************************
// 3.3. Find B anywhere in space *****
//************************************
void FieldFromMagnetization3D(double** B, double** node, int** ele, int n_ele,
		    double** M, double** point, int n_point)
{	
	double ri[4][3];
	
	// For all points where the solution is required
	for (int k=0; k<n_point; k++)
	{
		double p[3] = {0};

		// Point observed
		for (int i=0; i<3; i++)
		{
			p[i] = point[k][i];
		}
	
		// For the effect of ALL elements
		for (int l=0; l<n_ele; l++)
		{
			double m[3][3] = {{0}};
			double B_temp[3] = {0};
			double M_temp[3] = {0};
			
			// position of  magnetized element
			for (int i=0; i<4; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					ri[i][j] = node[ele[l][i]][j];
				}
			}
			
			// Magnetization of observed element
			for (int i=0; i<3; i++)
			{
				M_temp[i] = M[l][i];
			}

			//----------------------------------------------
			submat3D(m, ri, p); // calculation of submatrix --
			//----------------------------------------------
			
			// adding the effect on B
			multiply3D(B_temp, m, M_temp);
			
			for (int i=0; i<3; i++)
			{
					B[k][i] += B_temp[i];
			}
		}
	}
}

