/**-----------------------------------------------------------------------------
Te code in this file is intended to calculate the effect of magnetization
of elements on the H-Field.
--------------------------------------------------------------------------------
These analytical equations were found in :
    Proceedings of the Fourth International Conference on Magnet Technology,
    Brookhaven, USA, PP 617 to 626, 1972
	
Equations modified to fit triangles by :
	Kevin Ratelle
------------------------------------------------------------------------------*/

#include "CommonMath.h"
#include "MagneticField.h"

	
//--------------------------------------------------
// sort a vector of length 3
void sort(double y_sort[], double y[])
{
	if (y[1]<y[0])
	{
		const double tmp = y_sort[0];
		y_sort[0] = y_sort[1];
		y_sort[1] = tmp;
	}
	
	if (y[2]<y[1] || y[2]<y[0])
	{
		const double tmp = y_sort[2];
		if (y[2]<y[1] && y[2]<y[0])
		{
			y_sort[2] = y_sort[1];
			y_sort[1] = y_sort[0];
			y_sort[0] = tmp;
		}
		else
		{
			y_sort[2] = y_sort[1];
			y_sort[1] = tmp;
		}
	}
}

//--------------------------------------------------
int inside_triangle(double x[], double y[], double x0, double y0)
{	
	// vectors
	double v0[2] = {x[2]-x[0],y[2]-y[0]};
	double v1[2] = {x[1]-x[0],y[1]-y[0]};
	double v2[2] = {x0-x[0],y0-y[0]};
	
	// dot products
	double dot00 = dot2D(v0, v0);
	double dot01 = dot2D(v0, v1);
	double dot02 = dot2D(v0, v2);
	double dot11 = dot2D(v1, v1);
	double dot12 = dot2D(v1, v2);
	
	// barycentric coordinates
	double invDenom = 1 / (dot00*dot11 - dot01*dot01);
	double u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	double v = (dot00 * dot12 - dot01 * dot02) * invDenom;
	
	return (u>0) && (v>0) && (u+v < 1);
}

//--------------------------------------------------
void CorrectionValues(double aC[][2], double x[], double y[], double x0, double y0, double phi[])
{
	/*
		This function, given a triangle and a point will create a 2X2 matrix that will correct
		the matrix obtained by the function submat.The submat, multiplied by
		a magnetization vector of a triangle will give at the 'point' in
		space, the value of the H - Field as a vector.

		But, submat2D, by itself, doest not find the correct matrix for
		inside the triangle and on the right of this triangle(positive x - axis)
		CorrectionValues provides the matrix that will correct this.

		Function example : (m + aC)*M = Hm
	*/

	double input_angle = 0.0;
	double output_angle = 0.0;

	// Initialization
	// Ordering
	double y_sort[3];
	double x_sort[3]; //sorted according to y
	double phi_sort[3];
	
		
	for (int i=0; i<3; i++)
	{
		if (phi[i]==-PI || phi[i]==PI)
		{
			phi[i]=0;
		}
	}
	
	for (int i=0; i<3; i++)
	{
		x_sort[i] = x[i];
		y_sort[i] = y[i];
		phi_sort[i] = phi[i];
	}

	sort(y_sort, y);
	sort(x_sort, y); // x is sorted according to y
	sort(phi_sort, phi);
	
	//marker
	int right = 0;
	
	// Initialize
	double aCxx = 0.0;
	double aCyy = 0.0;
	double aCyx = 0.0;
	double aCxy = 0.0;
	
	//--------------------------------------------------------------------------
	// Angle calculation
	//--------------------------------------------------------------------------
	// AVOID limit cases
	if (y_sort[2]>=y0 && y0>y_sort[0])
	{
		// 2 Sides On the left
		if ((phi[0]<0) + (phi[1]<0) + (phi[2]<0) == 2)
		{
			// angle calculation
			output_angle = phi_sort[2];
			if (y0>y_sort[1]) input_angle = phi_sort[0];
			else input_angle = phi_sort[1];
			
			//right verification
			double angle = atan2(y0-y_sort[0] , x0-x_sort[0]);
			if (angle < output_angle) right = 1;
		}
		
		// 2 Sides On the right
		else if((phi[0]>0) + (phi[1]>0) + (phi[2]>0) == 2)
		{
			input_angle = phi_sort[0];
			
			//Verify which side to use for output angle
			if (y0>y_sort[1])
			{
				output_angle = phi_sort[2];
				
				// right verification
				double angle = atan2(y0-y_sort[1] , x0-x_sort[1]);
				if (angle < output_angle)
				{
					right = 1;
				}
			}
			else
			{
				output_angle = phi_sort[1];
				
				// right verification
				double angle = atan2(y0-y_sort[0] , x0-x_sort[0]);
				if (angle<output_angle) right=1;
			}
		}
		
		//----------------------------------------------------------------------
		// One side on left and 1 side on right ONLY
		//----------------------------------------------------------------------
		else
		{
			// angle calculation
			input_angle = phi_sort[0];
			output_angle = phi_sort[2];
			
			// right verification
			double y_d = y_sort[0];
			double x_d;
			if (x_sort[1] > x_sort[0] && y_sort[0] == y_sort[1])
			{
				x_d = x_sort[1];
			}
			else
			{
				x_d = x_sort[0];
			}
			
			double angle = atan2(y0-y_d , x0-x_d);
			if (angle < output_angle)
			{
				right = 1;
			}
		}
	}
	//--------------------------------------------------------------------------
	// Correction Calculation
	//--------------------------------------------------------------------------
	// inside element
	
	if (inside_triangle(x,y,x0,y0)==1)
	{
		aCxx = - pow(sin(input_angle),2);
		aCyy = - pow(cos(input_angle),2);
		aCyx = sin(input_angle) * cos(input_angle);
		aCxy = aCyx;
	}
	
	if (right == 1)
	{
		aCxx = - pow(sin(input_angle),2) + pow(sin(output_angle),2);
		aCyy = - pow(cos(input_angle),2) + pow(cos(output_angle),2);
		aCyx = sin(input_angle) * cos(input_angle) - sin(output_angle) * cos(output_angle);
		aCxy = aCyx;
	}

	aC[0][0] = aCxx;
	aC[1][1] = aCyy;
	aC[1][0] = aCyx;
	aC[0][1] = aCxy;
	
}

//------------------------------------------------------------------------------
// Main Functions that can be directly called for a result
//------------------------------------------------------------------------------
void submat2D(double m[][2], double x[], double y[], double x0, double y0)
{
	/*
		submat stands for submatrix for 2 - Dimensional case.Given a
		triangle, and given the supposition this triangle is uniformly
		magnetized, SubMat2D provides a 2X2 submatrix(m) that, multiplied
		by the magnetization of the element(M), will give the magnetic
		field(Hm) caused by the magnetisation at a given point(x0, y0).

		For example m * M = Hm
	*/

	// vectors to help ordering vertices
	int v[3] = {0,1,2}; // for j
	int v_p[3] = {1,2,0}; // for j+1
	int v_m[3] = {2,0,1}; //for j-1
	
	//--------------------------------------------------------------------------
	// Coefficient Calculation
	//--------------------------------------------------------------------------
	// angle between next vertex and current vertex
	double phi[3] ={0};
	for (int i = 0; i < 3; i++)
	{
		phi[i] = atan2(y[v_p[i]] - y[v[i]], x[v_p[i]] - x[v[i]]);
	}
	
	// distance between point evaluated and current vertex
	double r[3] = {0};
	for (int i=0; i<3; i++)
	{
		r[i] = pow(pow(x0-x[v[i]] , 2) + pow(y0-y[v[i]] , 2) , 0.5);
	}

	// helpful functions to evaluate submatrix M
	double f_tan[3] = {0};
	double sin_cos[3] = {0};
	double sin_2[3] = {0};
	double cos_2[3] = {0};
	for (int i=0; i<3; i++)
	{
		f_tan[i] = atan2(y[v[i]]-y0 , x[v[i]]-x0);
		sin_cos[i] = sin(phi[v[i]])*cos(phi[v[i]]) - sin(phi[v_m[i]])*cos(phi[v_m[i]]);
		sin_2[i] = pow(sin(phi[v[i]]),2) - pow(sin(phi[v_m[i]]),2);
		cos_2[i] = pow(cos(phi[v[i]]),2) - pow(cos(phi[v_m[i]]),2);
	}
	
	
	// evaluation of C values
	double Cxx = 0;
	double Cxy = 0;
	for (int i=0; i<3; i++)
	{
		Cxx = Cxx + sin_2[i]*f_tan[i] + sin_cos[i]*log(r[i]);
		Cxy = Cxy - sin_cos[i]*f_tan[i] - cos_2[i]*log(r[i]);
	}

	Cxx = (1/(2*PI)) * Cxx;
	Cxy = (1/(2*PI)) * Cxy;
	double Cyx = Cxy;
	double Cyy = -Cxx;
	
	//------------------------------------------------------------------------------
	// Correcting Coefficients
	//------------------------------------------------------------------------------
	// evaluates parameters
	double aC[2][2] = {{0}};
	CorrectionValues(aC, x, y, x0, y0, phi);
	
	//correcting the coefficients
	Cxx = Cxx + aC[0][0];
	Cxy = Cxy + aC[0][1];
	Cyx = Cyx + aC[1][0];
	Cyy = Cyy + aC[1][1];
	
	m[0][0] = Cxx;
	m[0][1] = Cxy;
	m[1][0] = Cyx;
	m[1][1] = Cyy;
}

//************************************
// 3.3. Find B anywhere in space *****
//************************************
void FieldFromMagnetization2D(double** B, double** node, int** ele, int n_ele,
		    double** M, double** point, int n_point)
{
	/*
	-find_b(B, node, ele, n_ele, M, point, n_point) :
		this function does not find matrix of submatrix linking M and Hm.It
		rather directly finds B once given a known magnetization on a mesh.

		->N_ele stands for number of elements.
		->Ele is a matrix containing all information on elements, or how
		nodes of the mesh are linked.It is a n_eleX3 matrix since all
		elements are triangles.
		->Node is a matrix containing the position of the nodes of the mesh.
		It is a n_node X 2 matrix since all points have 2 coordinates.
		->M stands for MAGNETIZATION.It is matrix of dimensions(n_ele * 2)
		and gives the magnetization on the mesh once the solution is known
		->N_point stands for the number of points where we want to find B
		->point contains information on the coordinates of the points where
		we want B.It is a n_point X 2 matrix
		->B is a n_point X 2 matrix giving the x and y values of B at the
		given points

		*** WARNING !!!***
		find_b only finds B for a mu_r = 1 (ex.air)
	*/

	double x[3], y[3];
	double mu0 = 4 * PI * pow(10,-7);
	
	// For all points where the solution is required
	for (int k=0; k<n_point; k++)
	{
		// Point observed
		double x0 = point[k][0];
		double y0 = point[k][1];
	
		// For the effect of ALL elements
		for (int l=0; l<n_ele; l++)
		{
			double m[2][2] = {{0}};
			double B_temp[2] = {0};
			double M_temp[2] = {0};
			
			// position of  magnetized element
			for (int i=0; i<3; i++)
			{
				x[i] = node[ele[l][i]][0];
				y[i] = node[ele[l][i]][1];
			}
			
			// correction not to be at a singularity
			if (y[0]==y0 || y[1]==y0 || y[2]==y0)
			{
				y0 = y0 + pow(10,-10);
			}
			
			// Magnetization of observed element
			for (int i=0; i<2; i++)
			{
				M_temp[i] = M[l][i];
			}

			//-----------------------------------------------------
			submat2D(m, x, y, x0, y0); // calculation of submatrix --
			//-----------------------------------------------------
			
			// adding the effect on B
			multiply2D(B_temp, m, M_temp);
			for (int i=0; i<2; i++)
			{
				B[k][i] += mu0*B_temp[i];
			}
			
			//------------------------------------------------------------
			// Correction Calculation
			//------------------------------------------------------------
			// inside element
			if (inside_triangle(x,y,x0,y0)==1)
			{
				for (int i=0; i<2; i++)
				{
					B[k][i] += mu0*M_temp[i];
				}
			}
			
		}
	}
}





















