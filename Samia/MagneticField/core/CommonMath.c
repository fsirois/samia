
#include "CommonMath.h"


double dot3D(const double a[3], const double b[3])
{
	return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
}

double dot2D(const double a[2], const double b[2])
{
	return (a[0] * b[0] + a[1] * b[1]);
}

void cross(const double a[], const double b[], double out[])
{
	out[0] = a[1] * b[2] - a[2] * b[1];
	out[1] = a[2] * b[0] - a[0] * b[2];
	out[2] = a[0] * b[1] - a[1] * b[0];
}

void add(const double a[], const double b[], double out[])
{
	out[0] = a[0] + b[0];
	out[1] = a[1] + b[1];
	out[2] = a[2] + b[2];
}

void substract(const double a[], const double b[], double out[])
{
	out[0] = a[0] - b[0];
	out[1] = a[1] - b[1];
	out[2] = a[2] - b[2];
}

double norm(double a[])
{
	const double outSq = a[0] * a[0] + a[1] * a[1] + a[2] * a[2];
	const double out = pow(outSq, 0.5);
	return out;
}

void multiply3D(double output[], const double a[3][3], const double b[3])
{
	output[0] = a[0][0] * b[0] + a[0][1] * b[1] + a[0][2] * b[2];
	output[1] = a[1][0] * b[0] + a[1][1] * b[1] + a[1][2] * b[2];
	output[2] = a[2][0] * b[0] + a[2][1] * b[1] + a[2][2] * b[2];
}

void multiply2D(double output[], const double a[2][2], const double b[2])
{
	output[0] = a[0][0] * b[0] + a[0][1] * b[1];
	output[1] = a[1][0] * b[0] + a[1][1] * b[1];
}

double solid_angle(double ri[][3], double ro[], int pt[])
{
	double r1o[3], r2o[3], r3o[3];
	for (int i = 0; i < 3; i++)
	{
		r1o[i] = ri[pt[0]][i] - ro[i];
		r2o[i] = ri[pt[1]][i] - ro[i];
		r3o[i] = ri[pt[2]][i] - ro[i];
	}

	double nr1o = norm(r1o);
	double nr2o = norm(r2o);
	double nr3o = norm(r3o);

	double D = nr1o * nr2o*nr3o + nr3o * dot3D(r1o, r2o) + nr2o * dot3D(r1o, r3o) + nr1o * dot3D(r2o, r3o);
	double cr[3];
	cross(r2o, r3o, cr);
	double Omega = 2 * atan2(dot3D(r1o, cr), D);
	return Omega;
}

int setdiff(const int pt[3])
{
	int count = 0;
	int cond = 1;
	while (cond)
	{
		if (pt[0] != count && pt[1] != count && pt[2] != count)
		{
			cond = 0;
		}
		else
		{
			count++;
		}
	}
	return count;
}

