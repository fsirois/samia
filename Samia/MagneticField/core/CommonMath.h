
#pragma once

#include <math.h>

#define PI 3.141592653589793238462643383279

double norm(double a[]);

void add(const double a[], const double b[], double out[]);
void substract(const double a[], const double b[], double out[]);

double dot3D(const double a[3], const double b[3]);
double dot2D(const double a[2], const double b[2]);
void cross(const double a[], const double b[], double out[]);

void multiply3D(double output[], const double a[3][3], const double b[3]);
void multiply2D(double output[], const double a[2][2], const double b[2]);

double solid_angle(double ri[][3], double ro[], int pt[]);
int setdiff(const int pt[3]);