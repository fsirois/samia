# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 15:50:56 2013

@author: ratelle
"""

from Samia.MagneticField.extensions import FieldFromMagnetization

###############################################################################
# Solution at any given point
###############################################################################
def field_from_magnetization(node, element, M, X, Y, *args):
    ''' Finding B anywhere
        Calculates B everywhere once the magnetization of the iron is known.

        INPUTS : 
            motor  --> Main parameters of the motor.
            rotor  --> All rotor related parameters.
            stator --> All stator related parameters.
            bi     --> Back-Iron parameters.
            test   --> Parameters defining the test that is going on.
            pos_vec --> Array of positions where B has to be known. Every
                        position of the array is an array of length 2.
            *args  --> Optional arguments
                        None provided : Use the rotor ans stator as source
                        args[0] : Special source asked 
                                    - 'rotor'
                                    - 'sator'
                                    - 'stator_1_phase'
                                    - Anything else leads to rotor and stator
                        args[1] : If 'stator_1_phase' is chosen and args[1]
                                  is provided, tells the phase to chose.                 
        OUTPUTS : B_tot_x --> List of x-component of B at all given points.
                  B_tot_y --> List of y-component of B at all given points.
    '''
    
    if type(X) is not list:
        X = X.tolist()
        Y = Y.tolist()
    
    if args:
        Z = args[0]
        if type(Z) is not list:
            Z = Z.tolist()
    
    if type(X[0]) is not list:
        pos_vec = [0] * len(X)
    else:
        pos_vec = [0] * len(X) * len(X[0])

    for i in range(len(X)):
        if type(X[0]) is not list:
            if args:
                pos_vec[i] = [X[i], Y[i], Z[i]]
            else:
                pos_vec[i] = [X[i], Y[i]]
        else:
            for j in range(len(X[0])):
                if args:
                    pos_vec[i * len(X[0]) + j] = [X[i][j], Y[i][j], Z[i][j]]
                else:
                    pos_vec[i * len(X[0]) + j] = [X[i][j], Y[i][j]]

    if type(node) is not list:
        node = node.tolist()
    if type(element) is not list:
        element = element.tolist()
        
    if args:
        B_m_vec = FieldFromMagnetization.B_from_M_3D(node, element, M, pos_vec)
    else:
        B_m_vec = FieldFromMagnetization.B_from_M_2D(node, element, M, pos_vec)

    BX = [0] * len(X)
    BY = [0] * len(X)
    if args:
        BZ = [0] * len(X)

    for i in range(len(X)):

        if type(X[0]) is not list:
            BX[i] = B_m_vec[i][0]
            BY[i] = B_m_vec[i][1]
            if args:
                BZ[i] = B_m_vec[i][2]
        else:
            BX[i] = [0] * len(X[0])
            BY[i] = [0] * len(X[0])
            if args:
                BZ[i] = [0] * len(X[0])
                
            for j in range(len(X[0])):
                BX[i][j] = B_m_vec[i*len(X[0]) + j][0]
                BY[i][j] = B_m_vec[i*len(X[0]) + j][1]
                if args:
                    BZ[i][j] = B_m_vec[i*len(X[0]) +j][2]
        
    if args:
        return BX, BY, BZ
    else:
        return BX, BY
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
