# -*- coding: utf-8 -*-

''' field_from_source module '''

from Samia.MagneticField.field_from_source.field_from_current import field_from_current
from Samia.MagneticField.field_from_source.field_from_magnetization import field_from_magnetization