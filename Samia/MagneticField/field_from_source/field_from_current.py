# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MAGNECTIC FLUX DENSITY FROM CURRENT SOURCE
This file generates the magnetic flux density from a current source. Both 2D
and 3D cases are included.

INPUTS
 - node --> Nodes of the mesh of the current source. It is either a list of
            lists or a two dimensional array, giving the position of all the
            elements. Example : [[x1,y1],[x2,y2],[x3,y3]] for three two-
            dimensional nodes. The values are floats.
            
 - ele  --> Elements of the mesh. It is either a list of lists or a two
            dimensional array, linking the nodes together in triangles or
            tetrahedrons. Example : [[n1,n2,n3],[n1,n3,n4]] for a two-
            dimensional mesh containing 2 elements.
            
 - J    --> Current density at centroids of all elements. For the 2D case, it
            is a one dimensional array or a list. For the 3D case, it is
            a list of lists or a two-dimensional array containing the three
            components of the field.
            3D example : [[j1_x, j1_y, j1_z],[j2_x, j2_y, j2_z]]
            2D example : [j1,j2,j3,j4,j5]
            The current densities are floats.
            
 - X, Y  --> X and Y positions where the field is wanted. Either lists, lists
             of lists, one-dimensional array or two dimensional arrays.
             X and Y must have the same dimensions. Values are floats.

 - *args --> Z positions where the field is wanted (added to X and Y) for the
             three dimensional case.

OUTPUTS
 - Bx, By --> Magnetic flux densities (x and y components) at the given X, Y
              and maybe Z positions.
 
 - Bz --> For the 3D case, z component of magnetic flux density is added.
 
-------------------------------------------------------------------------------
Call example :
    from field_from_current importe field_from_current
    node = [[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0]]
    element = [[0, 1, 2],[0,2,3]]
    J = [10**7, 10**7]
    X = [0.0, ]
    Y = [0.0]
    Bx, By = field_from_current(node, element, J, X, Y)
    
-------------------------------------------------------------------------------
Creation date : April 13, 2012
Modification  : June 2013
Created at    : University of Houston
Written by    : Kevin Ratelle
----------------------------------------------------------------------------'''

from Samia.MagneticField.extensions import FieldFromCurrent

def field_from_current(node, element, J, X, Y, *args):

    # Handling X and Y inputs
    if type(X) is not list:
        X = X.tolist()
        Y = Y.tolist()


    #--------------------------------------------------------------------------
    # Handling 3D case
    #--------------------------------------------------------------------------      
    if args:
        Z = args[0]
        
        if type(Z) is not list:
            Z = Z.tolist()
            
        if type(Z[0]) is not list:
            x = X
            y = Y
            z = Z
        else:
            x = [0] * len(X) * len(X[0])
            y = [0] * len(X) * len(X[0])
            z = [0] * len(X) * len(X[0])
            for i in range(len(X)):
                if type(Z[0]) is list:
                    for j in range(len(X[0])):
                       x[i * len(X[0]) + j] = X[i][j]
                       y[i * len(X[0]) + j] = Y[i][j]
                       z[i * len(X[0]) + j] = Z[i][j]


        #***** Call to 3D function ********************************************
        [Bx, By, Bz] = FieldFromCurrent.B_from_J_3D(x, y, z, node, element, J)
    
        if type(Z[0]) is not list:
            BX = Bx
            BY = By
            BZ = Bz
        else:
            BX = [0] * len(X)
            BY = [0] * len(X)
            BZ = [0] * len(X)
            for i in range(len(X)):
                BX[i] = [0] * len(X[0])
                BY[i] = [0] * len(X[0])
                BZ[i] = [0] * len(X[0])
                for j in range(len(X[0])):
                    BX[i][j] = Bx[i*len(X[0]) + j]
                    BY[i][j] = By[i*len(X[0]) + j]
                    BZ[i][j] = Bz[i*len(X[0]) + j]


    #--------------------------------------------------------------------------
    # Handling 2D case
    #--------------------------------------------------------------------------
    else:
    
        if type(X[0]) is not list:
            pos_vec = [0] * len(X)
        else:
            pos_vec = [0] * len(X) * len(X[0])
        for i in range(len(X)):
            if type(X[0]) is not list:
                pos_vec[i] = [X[i],Y[i]]
            else:
                for j in range(len(X[0])):
                    pos_vec[i * len(X[0]) + j] = [X[i][j], Y[i][j]]


        #***** Call to 2D function ********************************************
        B = FieldFromCurrent.B_from_J_2D(node, element, J, pos_vec)

        BX = [0] * len(X)
        BY = [0] * len(X)
        for i in range(len(X)):
            
            if type(X[0]) is list:
                BX[i] = [0] * len(X[0])
                BY[i] = [0] * len(X[0])
                for j in range(len(X[0])):
                    BX[i][j] = B[i*len(X[0]) + j][0]
                    BY[i][j] = B[i*len(X[0]) + j][1]
            else:
                BX[i] = B[i][0]
                BY[i] = B[i][1]


    # Returning values
    if args:
        return BX, BY, BZ
    else:
        return BX, BY



'''-------------------------------- EOF ------------------------------------'''


