# -*- coding: utf-8 -*-

from distutils.core import setup, Extension
import sys

# most functionalities are inside Core
coreDir = '../core/'
sys.path.append(coreDir)

# source files
currentSources = [
	coreDir + 'CommonMath.c',
	coreDir + 'FieldFromCurrent2D.c',
	coreDir + 'FieldFromCurrent3D.c',
	'FieldFromCurrent_Extension.c'
]

magnetizationSources = [
	coreDir + 'CommonMath.c',
	coreDir + 'FieldFromMagnetization2D.c',
	coreDir + 'FieldFromMagnetization3D.c',
	'FieldFromMagnetization_Extension.c'
]

methodOfMomentSources = [
	coreDir + 'CommonMath.c',
	coreDir + 'FieldFromMagnetization2D.c',
	coreDir + 'FieldFromMagnetization3D.c',
	coreDir + 'MethodOfMoments.c',
	'MethodOfMoments_Extension.c'
]

# modules listing
currentModule = Extension('FieldFromCurrent', sources = currentSources, include_dirs =[coreDir])
magnetizationModule = Extension('FieldFromMagnetization',sources = magnetizationSources, include_dirs=[coreDir])
methodOfMomentsModule = Extension('MethodOfMoments',sources = methodOfMomentSources, include_dirs=[coreDir])

modules = [
	currentModule,
	magnetizationModule,
	methodOfMomentsModule
]

setup (ext_modules = modules)
