

#include "Python.h"
#include "MagneticField.h"

enum Dimension
{
	Is2D = 0,
	Is3D
};

int GetDimensionCount(int dim)
{
	return dim == Is2D ? 2 : 3;
}

int GetNodesPerElement(int dim)
{
	return dim == Is2D ? 3 : 4;
}

PyObject* BuildMatrix(const int dim, PyObject* self, PyObject* args)
{
	// Declarations ------------------------------------------------------------
	int n_node, n_ele;
	PyListObject *list_node, *list_ele, *list_cent;
	PyObject *list; // line mod
	PyObject *py_value; // helps extracting all the data

	// Getting Inputs ----------------------------------------------------------
	PyArg_ParseTuple(args, "OOO", &list_node, &list_ele, &list_cent);
	n_node = PyList_GET_SIZE(list_node);
	n_ele = PyList_GET_SIZE(list_ele);

	const int nDim = GetDimensionCount(dim);
	const int nNodePerElement = GetNodesPerElement(dim);

	double** node = PyMem_Malloc(sizeof(void*) * n_node);
	int** ele = PyMem_Malloc(sizeof(void*) * n_ele);
	double** cent = PyMem_Malloc(sizeof(void*) * n_ele);

	for (int i = 0; i < n_node; i++)
	{
		node[i] = PyMem_Malloc(sizeof(double) * nDim);
	}

	for (int i = 0; i < n_ele; i++)
	{
		ele[i] = PyMem_Malloc(sizeof(int) * nNodePerElement);
		cent[i] = PyMem_Malloc(sizeof(double) * nDim);
	}

	// Get Node data
	for (int i = 0; i < n_node; i++)
	{
		list = PyList_GET_ITEM(list_node, i);
		for (int j = 0; j < nDim; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			node[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// Get element, centroid
	for (int i = 0; i < n_ele; i++)
	{
		// element
		list = PyList_GET_ITEM(list_ele, i);
		for (int j = 0; j < nNodePerElement; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			ele[i][j] = PyLong_AsLong(py_value);
		}

		// centroid
		list = PyList_GET_ITEM(list_cent, i);
		for (int j = 0; j < nDim; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			cent[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// Actual Operations -------------------------------------------------------
	const int nMatrixSize = nDim * n_ele;
	double* M = PyMem_Malloc(nMatrixSize * nMatrixSize * sizeof(double));

	if (dim == Is2D)
	{
		GFUN_BuildMatrix2D(M, node, ele, n_ele, cent);
	}
	else
	{
		GFUN_BuildMatrix3D(M, node, ele, n_ele, cent);
	}

	for (int i = 0; i < n_node; i++)
	{
		PyMem_Free(node[i]);
	}

	for (int i = 0; i < n_ele; i++)
	{
		PyMem_Free(ele[i]);
		PyMem_Free(cent[i]);
	}

	PyMem_Free(node);
	PyMem_Free(ele);
	PyMem_Free(cent);


	// Output Array : ----------------------------------------------------------
	// Main Matrix Output
	PyObject *list_matrix = PyList_New(nMatrixSize);
	for (int i = 0; i < nMatrixSize; i++)
	{
		PyObject *list_out = PyList_New(nMatrixSize);
		for (int j = 0; j < nMatrixSize; j++)
		{
			const int nMatrixIndex = i * nMatrixSize + j;
			const double fMatrixValue = M[nMatrixIndex];
			PyList_SetItem(list_out, j, PyFloat_FromDouble(fMatrixValue));
		}
		PyList_SetItem(list_matrix, i, list_out);
	}

	PyMem_Free(M);
	M = NULL;

	PyObject *output;
	output = Py_BuildValue("N", list_matrix);

	return output;
}

PyObject* BuildMatrix2D(PyObject* self, PyObject* args)
{
	return BuildMatrix(Is2D, self, args);
}

PyObject* BuildMatrix3D(PyObject* self, PyObject* args)
{
	return BuildMatrix(Is3D, self, args);
}

//------------------------------------------------------------------------------
// Module creation
//------------------------------------------------------------------------------

PyDoc_STRVAR(BuildMatrix2D_doc, "TODO");
PyDoc_STRVAR(BuildMatrix3D_doc, "TODO");

// Binds Python to C functions names
static PyMethodDef MethodOfMoments_Functions[] = {
	{ "BuildMatrix2D", (PyCFunction)BuildMatrix2D, METH_VARARGS, BuildMatrix2D_doc },
	{ "BuildMatrix3D", (PyCFunction)BuildMatrix3D, METH_VARARGS, BuildMatrix3D_doc },
	{ NULL, NULL, 0, NULL }
};

// Initialize Edges module.
int exec_MethodOfMoments(PyObject *module)
{
	PyModule_AddFunctions(module, MethodOfMoments_Functions);

	PyModule_AddStringConstant(module, "__author__", "Kevin Ratelle");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2019);

	return 0;
}


PyDoc_STRVAR(MethodOfMoments_doc, "TODO");

static PyModuleDef_Slot MethodOfMoments_slots[] = {
	{ Py_mod_exec, exec_MethodOfMoments },
	{ 0, NULL }
};

static PyModuleDef MethodOfMoments_def = {
	PyModuleDef_HEAD_INIT,
	"MethodOfMoments",
	MethodOfMoments_doc,
	0,
	NULL,
	MethodOfMoments_slots
};

PyMODINIT_FUNC PyInit_MethodOfMoments()
{
	return PyModuleDef_Init(&MethodOfMoments_def);
}
