/**----------------------------------------------------------------------------
 *-----------------------------------------------------------------------------
 * FIELD FROM CURRENT (WRAP FOR PYTHON)
 * This file calculates the magnetic flux density coming from a current density
 * in a material. It is a wrap for c functions, callable by Python. This
 * wrapper handles both the 2D and 3D case.
 * 
 * Import example : from B_from_J import B_from_J_3D
 *-----------------------------------------------------------------------------
 * Compilation : To compile this wrapper, run the file compile_current.bat in
 *               the same directory or the file compile.bat in the root.
 *-----------------------------------------------------------------------------
 * Written by   : Kevin Ratelle
 * Creation     : August 2012
 * Modification : September 2012
 * Created at   : University of Houston
 *---------------------------------------------------------------------------*/

#include "Python.h"

#include "MagneticField.h"

//-----------------------------------------------------------------------------
// Function Called By Python
//-----------------------------------------------------------------------------
static PyObject* B_from_J_3D(PyObject* self, PyObject* args)
{

	/*
	 * Call example : Bx, By, Bz = B_from_J(X, Y, Z, node, ele, J)
	 *
	 * INPUTS
	 *  - X, Y, Z --> Lists containing the positions where field is wanted
	 *  - node    --> Lists containing lists of length 3 giving the position of all
	 *                nodes of the mesh.
	 *  - ele     --> Lists containing lists of length 4 pointing to nodes, linking
	 *                them into tetrahedrical elements
	 *  - J       --> Lists containing lists of length 3 giving the current density
	 *                inside every element
	 *
	 * OUTPUTS
	 *  - BX, BY, BZ --> Magnetic flux density at the positions X, Y, Z.
	*/
	/* INPUTS : x, y, z, node, ele, J */

	//-------------------------------------------------------------------------
	// Declarations
	//-------------------------------------------------------------------------
	int i, j;
	int n_ele, n_pts, n_node;
	PyListObject *l_x, *l_y, *l_z, *l_node, *l_ele, *l_J;
	PyObject *list, *py_value;

	//-------------------------------------------------------------------------
	// Getting Inputs
	//-------------------------------------------------------------------------
	// All inputs
	PyArg_ParseTuple(args, "OOOOOO", &l_x, &l_y, &l_z, &l_node, &l_ele, &l_J);

	// Integers
	n_ele = PyList_GET_SIZE(l_ele);
	n_node = PyList_GET_SIZE(l_node);
	n_pts = PyList_GET_SIZE(l_x);

	double** ele = malloc(n_ele * sizeof(void*)); // TODO : this should be an int
	double* x = malloc(n_pts * sizeof(double));
	double* y = malloc(n_pts * sizeof(double));
	double* z = malloc(n_pts * sizeof(double));
	double** node = malloc(sizeof(void*) * n_node);
	double** J = malloc(sizeof(void*) * n_ele);

	for (int i = 0; i < n_node; i++)
	{
		node[i] = malloc(sizeof(double) * 3);
	}

	for (int i = 0; i < n_ele; i++)
	{
		J[i] = malloc(sizeof(double) * 3);
		ele[i] = malloc(sizeof(double) * 4);
	}

    // Elements and currents
    for (i=0; i<n_ele; i++){

        // Elements
        list = PyList_GET_ITEM(l_ele, i);
        for (j=0; j<4; j++){
            py_value = PyList_GET_ITEM(list, j);
            ele[i][j] = PyFloat_AsDouble(py_value);
        }

        // Current density
        list = PyList_GET_ITEM(l_J, i);
        for (j=0; j<3; j++){
            py_value = PyList_GET_ITEM(list, j);
            J[i][j] = PyFloat_AsDouble(py_value);
        }
    }



    // Points
    for (i=0; i<n_pts; i++){

        // X
        py_value = PyList_GET_ITEM(l_x,i);
        x[i] = PyFloat_AsDouble(py_value);
    
        // Y
        py_value = PyList_GET_ITEM(l_y,i);
        y[i] = PyFloat_AsDouble(py_value);

        // Z
        py_value = PyList_GET_ITEM(l_z,i);
        z[i] = PyFloat_AsDouble(py_value);
    }

    // Nodes
    for (i=0; i<n_node; i++){
        list = PyList_GET_ITEM(l_node, i);
        for (j=0; j<3; j++){
            py_value = PyList_GET_ITEM(list, j);
            node[i][j] = PyFloat_AsDouble(py_value);
        }

    }


    //-------------------------------------------------------------------------
    // Actual Operations
    //-------------------------------------------------------------------------
	double* Bx = malloc(sizeof(double) * n_pts);
	double* By = malloc(sizeof(double) * n_pts);
	double* Bz = malloc(sizeof(double) * n_pts);

    FieldFromCurrent3D(Bx, By, Bz, node, ele, n_ele, J, x,y,z, n_pts, n_node);

	for (int i = 0; i < n_node; i++)
	{
		free(node[i]);
	}

	for (int i = 0; i < n_ele; i++)
	{
		free(J[i]);
		free(ele[i]);
	}

	free(ele);
	free(x);
	free(y);
	free(z);
	free(node);
	free(J);

    //-------------------------------------------------------------------------
    // Output Array
    //-------------------------------------------------------------------------
    // constructing edge output
    PyObject *lst_Bx = PyList_New(n_pts);
    PyObject *lst_By = PyList_New(n_pts);
    PyObject *lst_Bz = PyList_New(n_pts);

    for (i = 0; i<n_pts; i++)
	{
        PyList_SET_ITEM(lst_Bx, i, PyFloat_FromDouble(Bx[i]));
        PyList_SET_ITEM(lst_By, i, PyFloat_FromDouble(By[i]));
        PyList_SET_ITEM(lst_Bz, i, PyFloat_FromDouble(Bz[i]));
    }

	free(Bx);
	free(By);
	free(Bz);

    // Final Output
    return Py_BuildValue("NNN", lst_Bx, lst_By, lst_Bz);
}


static PyObject* B_from_J_2D(PyObject* self, PyObject* args)
{
	/*
	 * Call example : Bx, By, Bz = B_from_J(X, Y, Z, node, ele, J)
	 *
	 * INPUTS
	 *  - X, Y, Z --> Lists containing the positions where field is wanted
	 *  - node    --> Lists containing lists of length 3 giving the position of all
	 *                nodes of the mesh.
	 *  - ele     --> Lists containing lists of length 4 pointing to nodes, linking
	 *                them into tetrahedrical elements
	 *  - J       --> Lists containing lists of length 3 giving the current density
	 *                inside every element
	 *
	 * OUTPUTS
	 *  - BX, BY, BZ --> Magnetic flux density at the positions X, Y, Z.
	*/
	int n_node, n_ele, n_point, i, j;
	PyListObject *list_node, *list_ele, *list_J, *list_point;
	PyObject *py_value, *list;

	// Getting Inputs ----------------------------------------------------------
	PyArg_ParseTuple(args, "OOOO", &list_node, &list_ele, &list_J, &list_point);

	n_node = PyList_GET_SIZE(list_node);
	n_ele = PyList_GET_SIZE(list_ele);
	n_point = PyList_GET_SIZE(list_point);

	double** node = malloc(sizeof(void*) * n_node);
	for (int i = 0; i < n_node; i++)
	{
		node[i] = malloc(sizeof(double) * 2);
	}

	int** ele = malloc(sizeof(void*) * n_ele);
	for (int i = 0; i < n_ele; i++)
	{
		ele[i] = malloc(sizeof(int) * 3);
	}

	double* J = malloc(sizeof(double) * n_ele);
	double** point = malloc(sizeof(void*) * n_point);
	for (int i = 0; i < n_point; i++)
	{
		point[i] = malloc(sizeof(double) * 2);
	}

	// Get Node data
	for (i=0; i<n_node; i++){
		list = PyList_GET_ITEM(list_node, i);
		for (j=0; j<2; j++){
			py_value = PyList_GET_ITEM(list, j);
			node[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// Get element and magnetization
    for (i=0; i<n_ele; i++){
		// element
        list = PyList_GET_ITEM(list_ele, i);
        for (j=0; j<3; j++){
            py_value = PyList_GET_ITEM(list, j);
            ele[i][j] = PyLong_AsLong(py_value);
        }
		// magnetization
		py_value = PyList_GET_ITEM(list_J, i);
		J[i] = PyFloat_AsDouble(py_value);
    }
	
	// point where we want the solution
	for (i=0; i<n_point; i++){
		list = PyList_GET_ITEM(list_point, i);
		for (j=0; j<2; j++){
			py_value = PyList_GET_ITEM(list, j);
			point[i][j] = PyFloat_AsDouble(py_value);
		}
	}

    // Actual Operations -------------------------------------------------------
	double** B = malloc(sizeof(void*) * n_point);
	for (i=0; i<n_point; i++)
	{
		B[i] = malloc(sizeof(double) * 2);
		B[i][0] = 0;
		B[i][1] = 0;
	}

	FieldFromCurrent2D(B, node, ele, n_ele, J, point, n_point);

	for (int i = 0; i < n_node; i++)
	{
		free(node[i]);
	}

	for (int i = 0; i < n_ele; i++)
	{
		free(ele[i]);
	}

	for (int i = 0; i < n_point; i++)
	{
		free(point[i]);
	}

	free(ele);
	free(J);
	free(point);
	free(node);
	

    // Output Array is ---------------------------------------------------------
    // B Output
    PyObject *list_B = PyList_New(n_point);
    for (i = 0; i<n_point; i++)
	{
		PyObject *list = PyList_New(2);
        for (j = 0; j<2; j++)
		{
            PyObject *num = PyFloat_FromDouble(B[i][j]);
            PyList_SET_ITEM(list, j, num);   // reference to num stolen
        }
        PyList_SET_ITEM(list_B, i, list);
    }

	for (i = 0; i < n_point; i++)
	{
		free(B[i]);
	}

	free(B);

    // Final Output
    return Py_BuildValue("N", list_B);
}


//------------------------------------------------------------------------------
// Module definition
//------------------------------------------------------------------------------

PyDoc_STRVAR(FieldFromCurrent_2D_doc, "B_from_J_2D(node, element, J, pos_vec) -> B \n\n\
  - node: list of nodes (X,Y) positions of the mesh \n\
  - element: list of elements (n1,n2,n3) nodes of all the elements in the mesh \n\
  - J: current density in all elements \n\
  - pos_vec: list of positions (X,Y) where the magnetic field is wanted \n\n\
  - B (output): nx2 array of magnetic field at wanted positions.");

PyDoc_STRVAR(FieldFromCurrent_3D_doc, "B_from_J_3D(x, y, z, node, element, J) -> B \n\n\
  - node: list of nodes (X,Y,Z) positions of the mesh \n\
  - element: list of elements (n1,n2,n3) nodes of all the elements in the mesh \n\
  - J: current density in all elements \n\
  - x,y,z: lists of positions (3 arrays of same length) where the magnetic field is wanted \n\n\
  - B (output): nx3 array of magnetic field at wanted positions.");

// List of functions to add to Edges module
// Binds Python to C functions names
static PyMethodDef FieldFromCurrent_Functions[] = {
	{ "B_from_J_2D", (PyCFunction)B_from_J_2D, METH_VARARGS, FieldFromCurrent_2D_doc },
	{ "B_from_J_3D", (PyCFunction)B_from_J_3D, METH_VARARGS, FieldFromCurrent_3D_doc },
	{ NULL, NULL, 0, NULL }
};

// Initialize Edges module.
int exec_FieldFromCurrent(PyObject *module)
{
	PyModule_AddFunctions(module, FieldFromCurrent_Functions);

	PyModule_AddStringConstant(module, "__author__", "Kevin Ratelle");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2019);

	return 0;
}


PyDoc_STRVAR(FieldFromCurrent_doc, "The B from J module gives 2 functions that allows to compule the magnetic field B \
 at different positions in space to ba calculated when the source is current. \n\
 Note that a mesh must be provided with uniform current inside each element.");

static PyModuleDef_Slot FieldFromCurrent_slots[] = {
	{ Py_mod_exec, exec_FieldFromCurrent },
	{ 0, NULL }
};

static PyModuleDef FieldFromCurrent_def = {
	PyModuleDef_HEAD_INIT,
	"FieldFromCurrent",
	FieldFromCurrent_doc,
	0,
	NULL,
	FieldFromCurrent_slots
};

PyMODINIT_FUNC PyInit_FieldFromCurrent()
{
	return PyModuleDef_Init(&FieldFromCurrent_def);
}

