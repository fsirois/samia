
/**------------------------------------------------------------------------
---------------------------------------------------------------------------
C_WRAPS
These are wraps to be able to use the functions in the library :
C_functions.c
With C_WRAPS, when compiled with the help of setup_solution2D.py, one can
call functions inside C_functions directly from Python. To do so, he will
have to import the functions SubMat2D, MainMatrix2D and/or FindB2D from
the library Solution2D.

Import example : from Solution2D import MainMatrix2D
---------------------------------------------------------------------------
FUNCTIONS

SubMat2D -> This function generates a 2X2 matrix linking the H-Field
			created a given point by a uniform magnetization of an element.
			For more details, see the paper pointer below, or comments in
			the file C_functions.c. It is a wrap for 'submat'.

MainMatrix2D -> Function generates a Main Matrix containing information on
				the effect of the geometry on the H-Field caused by a
				current source. It is only a stack of SubMat2D. It is a
				wrap from build_m in C_functions.c

FindB2D -> Function generating Magnetic Flux a many points in space once
		   the magnetization is known. It is a wrap for find_b in
		   C_functions.c
---------------------------------------------------------------------------
NOTE : Analytical equations used were found in :
	Proceedings of the Fourth International Conference on Magnet Technology,
	Brookhaven, USA, PP 617 to 626, 1972
---------------------------------------------------------------------------
Written by : Kevin Ratelle
Created at : �cole Polytechnique de Montr�al
Created on : August 4, 2011
Last modification : August 8, 2011
-------------------------------------------------------------------------*/
/**----------------------------------------------------------------------------
 *-----------------------------------------------------------------------------
 * MAGNETIZATION_WRAP_3D
 * These are wraps to be able to use the functions in the library :
 * MAGNETIZATION_3D.c
 *
 * With MAGNETIZATION_WRAP_3D, when compiled with the help of
 * compile_solution3D.bat, one can call functions inside magnetization_3D.c
 * directly from Python. To do so, he will have to import the functions
 * SubMat3D, MainMatrix3D and/or FindB3D from the library Solution3D.
 *
 * Import example : from Solution3D import MainMatrix3D
 *-----------------------------------------------------------------------------
 * FUNCTIONS
 *
 * SubMat3D -> This function generates a 3X3 matrix linking the H-Field
 *             created a given point by a uniform magnetization of an element.
 *             For more details, see the paper pointer below, or comments in
 *             the file magnetization_3D.c. It is a wrap for 'submat'.
 *
 * MainMatrix3D -> Function generates a Main Matrix containing information on
 *                 the effect of the geometry on the H-Field caused by a
 *                 current source. It is only a stack of SubMat3D. It is a
 *                 wrap from build_m in magnetization_3D.c
 *
 * FindB3D -> Function generating Magnetic Flux a many points in space once
 *            the magnetization is known. It is a wrap for find_b in
 *            magnetization_3D.c
 *-----------------------------------------------------------------------------
 * NOTE : Analytical equations used were found in :
 *  Massimo Fabbri, IEEE Transactions on Magnetics, vol. 44, no. 1,
 *  Magnetic Flux Density and Vector Potential of Uniform Polyhedral Sources,
 *  PP 32 to 36, January 2008
 *-----------------------------------------------------------------------------
 * Written by : Kevin Ratelle
 * Created at : �cole Polytechnique de Montr�al
 * Created on : August 4, 2011
 * Last modification : August 25, 2011
 *---------------------------------------------------------------------------*/

#include "Python.h"
#include "MagneticField.h"

enum Dimension
{
	Is2D = 0,
	Is3D
};

int GetDimensionCount(int dim)
{
	return dim == Is2D ? 2 : 3;
}

int GetNodesPerElement(int dim)
{
	return dim == Is2D ? 3 : 4;
}

PyObject* B_from_M(int dim, PyObject* self, PyObject* args)
{
	// Getting Inputs ----------------------------------------------------------
	PyListObject *list_node, *list_ele, *list_M, *list_point;
	PyArg_ParseTuple(args, "OOOO", &list_node, &list_ele, &list_M, &list_point);

	int n_node = PyList_GET_SIZE(list_node);
	int n_ele = PyList_GET_SIZE(list_ele);
	int n_point = PyList_GET_SIZE(list_point);


	int dimensionCount = GetDimensionCount(dim);
	int nodePerElement = GetNodesPerElement(dim);

	double** node = malloc(sizeof(void*) * n_node);
	for (int i = 0; i < n_node; i++)
	{
		node[i] = malloc(sizeof(double) * dimensionCount);
	}

	int** ele = malloc(sizeof(void*) * n_ele);
	double** M = malloc(sizeof(void*) * n_ele);
	for (int i = 0; i < n_ele; i++)
	{
		ele[i] = malloc(sizeof(int) * nodePerElement);
		M[i] = malloc(sizeof(double) * dimensionCount);
	}

	double** point = malloc(sizeof(void*) * n_point);
	for (int i = 0; i < n_point; i++)
	{
		point[i] = malloc(sizeof(double) * dimensionCount);
	}



	PyObject *py_value, *list;
	// Get Node data
	for (int i = 0; i < n_node; i++)
	{
		list = PyList_GET_ITEM(list_node, i);
		for (int j = 0; j < dimensionCount; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			node[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// Get element and magnetization
	for (int i = 0; i < n_ele; i++)
	{
		// element
		list = PyList_GET_ITEM(list_ele, i);
		for (int j = 0; j < nodePerElement; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			ele[i][j] = PyLong_AsLong(py_value);
		}

		// magnetization
		list = PyList_GET_ITEM(list_M, i);
		for (int j = 0; j < dimensionCount; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			M[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// point where we want the solution
	for (int i = 0; i < n_point; i++)
	{
		list = PyList_GET_ITEM(list_point, i);
		for (int j = 0; j < dimensionCount; j++)
		{
			py_value = PyList_GET_ITEM(list, j);
			point[i][j] = PyFloat_AsDouble(py_value);
		}
	}

	// Actual Operations -------------------------------------------------------
	double** B = malloc(sizeof(void*) * n_point);
	for (int i = 0; i < n_point; i++)
	{
		B[i] = malloc(sizeof(double) * dimensionCount);
		B[i][0] = 0;
		B[i][1] = 0;
		if (dim == Is3D)
		{
			B[i][2] = 0;
		}
	}

	if (dim == Is2D)
	{
		FieldFromMagnetization2D(B, node, ele, n_ele, M, point, n_point);
	}
	else
	{
		FieldFromMagnetization3D(B, node, ele, n_ele, M, point, n_point);
	}

	for (int i = 0; i < n_node; i++)
	{
		free(node[i]);
	}
	free(node);

	for (int i = 0; i < n_ele; i++)
	{
		free(ele[i]);
		free(M[i]);
	}
	free(ele);
	free(M);

	for (int i = 0; i < n_point; i++)
	{
		free(point[i]);
	}
	free(point);

	// Output Array is ---------------------------------------------------------
	// B Output
	PyObject *list_B = PyList_New(n_point);
	for (int i = 0; i < n_point; i++)
	{
		PyObject *list = PyList_New(dimensionCount);
		for (int j = 0; j < dimensionCount; j++)
		{
			PyObject *num = PyFloat_FromDouble(B[i][j]);
			PyList_SET_ITEM(list, j, num);   // reference to num stolen
		}
		PyList_SET_ITEM(list_B, i, list);
	}

	// Final Output
	return Py_BuildValue("N", list_B);
}

PyObject* B_from_M_2D(PyObject* self, PyObject* args)
{
	return B_from_M(Is2D, self, args);
}

PyObject* B_from_M_3D(PyObject* self, PyObject* args)
{
	return B_from_M(Is3D, self, args);
}


//------------------------------------------------------------------------------
// These Always have to be there to link
//------------------------------------------------------------------------------
PyDoc_STRVAR(FieldFromMagnetization_2D_doc, "TODO");

PyDoc_STRVAR(FieldFromMagnetization_3D_doc, "TODO");


// List of functions to add to Edges module
// Binds Python to C functions names
static PyMethodDef FieldFromMagnetization_Functions[] = {
	{ "B_from_M_2D", (PyCFunction)B_from_M_2D, METH_VARARGS, FieldFromMagnetization_2D_doc },
	{ "B_from_M_3D", (PyCFunction)B_from_M_3D, METH_VARARGS, FieldFromMagnetization_3D_doc },
	{ NULL, NULL, 0, NULL }
};

// Initialize Edges module.
int exec_FieldFromMagnetization(PyObject *module)
{
	PyModule_AddFunctions(module, FieldFromMagnetization_Functions);

	PyModule_AddStringConstant(module, "__author__", "Kevin Ratelle");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2019);

	return 0;
}


PyDoc_STRVAR(FieldFromMagnetization_doc, "TODO");

static PyModuleDef_Slot FieldFromMagnetization_slots[] = {
	{ Py_mod_exec, exec_FieldFromMagnetization },
	{ 0, NULL }
};

static PyModuleDef FieldFromMagnetization_def = {
	PyModuleDef_HEAD_INIT,
	"FieldFromMagnetization",
	FieldFromMagnetization_doc,
	0,
	NULL,
	FieldFromMagnetization_slots
};

PyMODINIT_FUNC PyInit_FieldFromMagnetization()
{
	return PyModuleDef_Init(&FieldFromMagnetization_def);
}




