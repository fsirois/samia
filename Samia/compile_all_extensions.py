# -*- coding: utf-8 -*-

import subprocess
import os
import sys

# List of directories where there is a C extension for python
# all these paths must contain a compile_setup.py file
directories = [
	'Samia/Mesh/Edges',
	'Samia/MagneticField/extensions',
	'Samia/Superconductor/superconductor_support'
]

# Whether on linux or windows, the command remains the same
command = 'python compile_setup.py build_ext --inplace'

# used to hide output from current shell
nullOut = open(os.devnull, 'w') 

print("There are %i modules to build" % len(directories))
for directory in directories:
	print("   - Building %s" % (directory))
	try:
		subprocess.check_call(command, cwd=directory, shell=True, stdout=nullOut)
	except subprocess.CalledProcessError:
		sys.exit(' Compilation Error in %s module' % directory)

print("\nAll C modules compiled.")