/*------------------------------------------------------------------------------
 * CREATE EDGES
 * Creates edges with information a given mesh. Needs nodes and elements to be
 * created prior to this function call.
 * 
 * Call example : t = Edges(ele)
 *------------------------------------------------------------------------------
 * INPUTS
 *     - ele : matrix with elements, it must be a n_elements X 3 matrix since
 *             elements are triangles. Ele must be a list to be exact with
 *             length = n_elements. Each list element must be a list of length 3
 *
 * OUTPUTS
 *     - t : tuple with solution
 *          t[0] -> edge matrix. It is a n_edges long list, and each item is a
 *                  2 long list for the two nodes.
 *          t[1] -> ele2edge matrix. It a n_ele long list, and each item is a 3
 *                  items long list, for each edge linked to the element
 *          t[2] -> n_edge is the number of edges in the mesh
 *----------------------------------------------------------------------------*/
 
 #include "Edges.h"

int create_edges2D(int** ele, int** edge, int n_ele, int** ele2edge)
{
	int ind_edge = 0;

	for (int ind_ele=0; ind_ele<n_ele; ind_ele++)
	{
		const int node1 = ele[ind_ele][0];
		const int node2 = ele[ind_ele][1];
		const int node3 = ele[ind_ele][2];

		// Ordering Nodes
		NodePair edgesNodesForElement[3];
		edgesNodesForElement[0] = OrderNode(node1, node2);
		edgesNodesForElement[1] = OrderNode(node2, node3);
		edgesNodesForElement[2] = OrderNode(node1, node3);
		
		// Creating edges for all 3 sides of triangle
		for (int k=0; k<3; k++)
		{
			NodePair edgeNodes = edgesNodesForElement[k];

			// Finding if edges have these values, and wich
			char bEdgeExists = 0;
			int ind = 0;
			for (int m=0; m < ind_edge; m++)
			{
				if (edge[m][0] == edgeNodes.m_node1 && edge[m][1] == edgeNodes.m_node2)
				{
					bEdgeExists = 1; // we already have this edge
					ind=m; // finding index of edge
				}
			}
			
			// If it does not exist, we create it
			if (!bEdgeExists)
			{
				edge[ind_edge][0] = edgeNodes.m_node1;
				edge[ind_edge][1] = edgeNodes.m_node2;
				ele2edge[ind_ele][k] = ind_edge;
				ind_edge = ind_edge+1; // increment edge counter
			}
			else
			{
				ele2edge[ind_ele][k] = ind;
			}
		}
	}

	// Save number of edges
	return ind_edge;
}