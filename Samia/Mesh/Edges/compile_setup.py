from distutils.core import setup, Extension

import numpy as np

cSources = ['Edges_2D.c','Edges_3D.c','Edges.c']
extensions = Extension('Edges', sources = cSources, include_dirs=['.',np.get_include()])
 
setup ( name = 'Edges',
		ext_modules = [extensions])