/**------------------------------------------------------------------------
 * CREATE_EDGES
 * This code generates the edges information based on a given mesh
 *
 * Call example : [n_edge, ele2edge, edge] = create_edge_full(n_ele, ele);
 *-------------------------------------------------------------------------
 * INPUTS
 *    + n_ele : number of elements in the mesh
 *    + ele   : matrix linking elements to nodes (n_ele X 4) matrix
 * 
 * OUTPUTS
 *    + n_edge   : number of edges in the mesh
 *    + ele2edge : linking each elements to its 6 edges (n_ele X 6) matrix
 *    + edge     : linking edges to nodes (n_edge X 2) matrix
 *-----------------------------------------------------------------------*/
 
 #include "Edges.h"

int create_edges3D(int** ele, int** edge, int n_ele, int** ele2edge)
{
	int ind_edge = 0;

	for (int ind_ele = 0; ind_ele < n_ele; ind_ele++)
	{
		int node1 = ele[ind_ele][0];
		int node2 = ele[ind_ele][1];
		int node3 = ele[ind_ele][2];
		int node4 = ele[ind_ele][3];

		// Ordering nodes for EACH edge of the given element
		NodePair edgesNodesForElement[6];
		edgesNodesForElement[0] = OrderNode(node1, node2);
		edgesNodesForElement[1] = OrderNode(node1, node3);
		edgesNodesForElement[2] = OrderNode(node1, node4);
		edgesNodesForElement[3] = OrderNode(node2, node3);
		edgesNodesForElement[4] = OrderNode(node2, node4);
		edgesNodesForElement[5] = OrderNode(node3, node4);

		// Loop on all 6 edges of the tetrahedron
		for (int k=0; k<6; k++)
		{
			// Nodes of the edge
			const NodePair edgeNodes = edgesNodesForElement[k];
			
			// We check if an edge already has these nodes
			int counter=0;
			for (int i=0; i<ind_edge; i++)
			{
				if (edge[i][0]==edgeNodes.m_node1 && edge[i][1]==edgeNodes.m_node2)
				{
					counter = 1; // Edge already exists
					ele2edge[ind_ele][k] = i;
				}
			}
			
			// If edge still does not exist, we create it
			if (counter!=1)
			{
				edge[ind_edge][0] = edgeNodes.m_node1;
				edge[ind_edge][1] = edgeNodes.m_node2;
				ele2edge[ind_ele][k] = ind_edge;
				
				ind_edge=ind_edge+1; // Increment edge counter
			}
		}
	}
	
	// returns number of edges
	return ind_edge;
}


