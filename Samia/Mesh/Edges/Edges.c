#include <Python.h>

#include "Edges.h"

#define EDGES_PER_ELEMENT_3D 6
#define EDGES_PER_ELEMENT_2D 3

enum Dimension
{
	Is2D = 0,
	Is3D
};

int GetEdgesPerElement(const int dim)
{
	return dim == Is2D ? EDGES_PER_ELEMENT_2D : EDGES_PER_ELEMENT_3D;
}

//------------------------------------------------------------------------------
// Functions Called By Python
//------------------------------------------------------------------------------
PyObject* CreateEdges(const int dim, PyObject* self, PyObject* args)
{
	PyListObject* liste = NULL;

	//----- Getting Inputs -----
	// Only input is Ele
	PyArg_ParseTuple(args, "O", &liste);
	const int n_ele = PyList_GET_SIZE(liste);

	PyObject* list = PyList_GET_ITEM(liste, 0);
	const int node_per_ele = PyList_GET_SIZE(list);
	int** ele = malloc(sizeof(void*) * n_ele);

	for (int i = 0; i < n_ele; i++)
	{
		ele[i] = malloc(sizeof(int) * node_per_ele);

		list = PyList_GET_ITEM(liste, i);
		for (int j = 0; j < node_per_ele; j++)
		{
			PyObject* py_value = PyList_GET_ITEM(list, j);
			ele[i][j] = PyLong_AsLong(py_value);
		}
	}

	//----- Actual operations -----
	int n_edge_temp = GetEdgesPerElement(dim) * n_ele; // allocate for worst case scenario
	int** edge = malloc(sizeof(void*) * n_edge_temp);
	for (int i = 0; i < n_edge_temp; i++)
	{
		edge[i] = malloc(sizeof(int) * 2);
	}

	const int nEdgePerElement = GetEdgesPerElement(dim);
	int** ele2edge = malloc(sizeof(void*) * n_ele);
	for (int i = 0; i < n_ele; i++)
	{
		ele2edge[i] = malloc(sizeof(int) * nEdgePerElement);
	}

	int n_edge = 0;
	if (dim == Is2D)
	{
		n_edge = create_edges2D(ele, edge, n_ele, ele2edge);
	}
	else
	{
		n_edge = create_edges3D(ele, edge, n_ele, ele2edge);
	}

	//----- Output Array -----
	// constructing edge output
	PyObject *lst_edge = PyList_New(n_edge);
	for (int i = 0; i < n_edge; i++)
	{
		PyObject *lst = PyList_New(2);
		for (int j = 0; j < 2; j++)
		{
			PyObject *num = PyLong_FromLong(edge[i][j]);

			PyList_SET_ITEM(lst, j, num);   // reference to num stolen
		}
		PyList_SET_ITEM(lst_edge, i, lst);
	}

	for (int i = 0; i < n_edge_temp; i++)
	{
		free(edge[i]);
	}
	free(edge);

	// constructing ele2edge output
	PyObject *lst_ele2edge = PyList_New(n_ele);
	for (int i = 0; i < n_ele; i++)
	{
		PyObject *lst = PyList_New(nEdgePerElement);
		for (int j = 0; j < nEdgePerElement; j++)
		{
			PyObject *num = PyLong_FromLong(ele2edge[i][j]);

			PyList_SET_ITEM(lst, j, num);   // reference to num stolen
		}
		PyList_SET_ITEM(lst_ele2edge, i, lst);
		free(ele[i]);
		free(ele2edge[i]);
	}
	free(ele);
	free(ele2edge);

	// Final Output
	return Py_BuildValue("OOi", lst_edge, lst_ele2edge, n_edge);
}

//--------------------------------------------------
PyObject* CreateEdges2D(PyObject* self, PyObject* args)
{
	return CreateEdges(Is2D, self, args);
}

//--------------------------------------------------
PyObject* CreateEdges3D(PyObject* self, PyObject* args)
{
	return CreateEdges(Is3D, self, args);
}

//------------------------------------------------------------------------------
// Module definition
//------------------------------------------------------------------------------

PyDoc_STRVAR(Edges2D_doc, "CreateEdges2D(elements) -> edges \n\n\
  - elements: array of n elements, where each element contains a length 3 array \
 containing node index for the triangle \n\
  - structure containing \n\
	 edges: array of n edges, where each edge contains a length 2 array containing node index for the edge \n\
     ele2edge: array of n elements, where each element contains a length 3 array containing edge index for the element \n\
	 n_edge: count of edges");

PyDoc_STRVAR(Edges3D_doc, "CreateEdges3D(elements) -> edges \n\n\
  - elements: array of n elements, where each element contains a length 4 array \
 containing node index for the tetrahedron \n\
  - structure containing \n\
	 edges: array of n edges, where each edge contains a length 2 array containing node index for the edge \n\
     ele2edge: array of n elements, where each element contains a length 6 array containing edge index for the element \n\
	 n_edge: count of edges");

// List of functions to add to Edges module
static PyMethodDef EdgesFunctions[] = {
	{ "CreateEdges2D", (PyCFunction)CreateEdges2D, METH_VARARGS, Edges2D_doc },
	{ "CreateEdges3D", (PyCFunction)CreateEdges3D, METH_VARARGS, Edges3D_doc },
	{ NULL, NULL, 0, NULL }
};

// Initialize Edges module.
int exec_Edges(PyObject *module)
{
	PyModule_AddFunctions(module, EdgesFunctions);

	PyModule_AddStringConstant(module, "__author__", "Kevin Ratelle");
	PyModule_AddStringConstant(module, "__version__", "1.0.0");
	PyModule_AddIntConstant(module, "year", 2019);

	return 0;
}


PyDoc_STRVAR(Edges_doc, "The Edges module creates edges information for a mesh out of the elements \
 information. This works both in 2D and 3D, but specifically for triangles and tetrahedrons.");

static PyModuleDef_Slot Edges_slots[] = {
	{ Py_mod_exec, exec_Edges },
	{ 0, NULL }
};

static PyModuleDef Edges_def = {
	PyModuleDef_HEAD_INIT,
	"Edges",
	Edges_doc,
	0,
	NULL,
	Edges_slots
};

PyMODINIT_FUNC PyInit_Edges()
{
	return PyModuleDef_Init(&Edges_def);
}
