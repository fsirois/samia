#pragma once

int create_edges2D(int** ele, int** edge, int n_ele, int** ele2edge);
int create_edges3D(int** ele, int** edge, int n_ele, int** ele2edge);

//--------------------------------------------------
typedef struct
{
	int m_node1;
	int m_node2;
} NodePair;

//--------------------------------------------------
inline NodePair OrderNode(int node1, int node2)
{
	if (node2 > node1)
	{
		int temp = node1;
		node1 = node2;
		node2 = temp;
	}

	return (NodePair) { node1, node2 };
}