# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 02:12:52 2013

@author: Kevin
"""

def mesh(dim, mesher, *max_dimension):

    if dim==2:
        from Samia.Mesh.Mesh2D import mesh_py_2D, mesh_reg_2D
        if mesher == 'meshpy':
            mesh = mesh_py_2D(max_dimension[0])
        else: # regular
            mesh = mesh_reg_2D()
    else:
        from Samia.Mesh.Mesh3D import mesh_py_3D, mesh_reg_3D
        if mesher == 'meshpy':
            mesh = mesh_py_3D(max_dimension[0])
        else: # regular
            mesh = mesh_reg_3D()
            
    return mesh
