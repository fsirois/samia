# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 01:48:07 2013

@author: Kevin
"""

class mesh2D():
    
    def __init__(self):
        
        # Materials
        self.n_sections = 1
        self.section = []
        
        # Sets mesh to zero
        self.node = None
        self.n_node = None
        self.ele = None
        self.n_ele = None
        self.edge = None
        self.cent = None
        
    #---------------------------------------------------------------------------
    # Create Edges
    #---------------------------------------------------------------------------    
    def create_edges(self):
        '''Function creating edges from a given mesh. Mesh must include at least
           the mesh.node and mesh.ele fields to work. The values are written
           in the fields mesh.edge, mesh.n_ele and mesh.ele2edge (linking
           elements to associated edges)
           
           Call example : mesh.create_edges()
           
           1 - This function must be called AFTER the mesh is already generated
           2 - The code is actually a C function wrapped in Python. To compile
               it if needed, go to the command prompt, in the right directory.
               Type : python setup.py build -cmingw32 (if using windows)
               to compile the file. Then go in the 'build' folder created, then
               in the lib.win32-2.6 folder, and the compiled file is called
               Edges.pyd
        '''
        from Samia.Mesh.Edges import CreateEdges2D

        m = list(self.ele)
        for i in range(len(m)):
            m[i] = list(m[i])

        t = CreateEdges2D(m);
        self.edge = t[0]
        self.ele2edge = t[1]
        self.n_edge = t[2]


    #---------------------------------------------------------------------------
    # Adding Mesh to existing mesh
    #---------------------------------------------------------------------------
    def add_mesh(self, mesh):
        ''' Adds mesh to existing mesh.

            Call example : mesh.add_mesh(mesh_tmp)
            
            1 - A mesh must already exists to call this function, but the edges
                are not necessary
        '''
        self.n_sections += 1
            
        for i in range(mesh.n_ele):
            self.section.append(self.n_sections-1)
            self.ele.append([mesh.ele[i][0]+self.n_node, mesh.ele[i][1]+self.n_node, mesh.ele[i][2]+self.n_node])
            
        for i in range(mesh.n_node):
            self.node.append(mesh.node[i])
            
        self.n_ele += mesh.n_ele
        self.n_node += mesh.n_node
        
        self.add_area()
        self.add_cent()
        self.create_edges()
        
    def plot(self):
        '''Plots the mesh once created. Edges must be created for the function
           works.
           
           Call example : mesh.plot
        '''        
        from matplotlib.pyplot import figure, plot
        import pylab    
    
        figure()
        for i in range(self.n_edge):
            pos_x1 = self.node[self.edge[i][0]][0]
            pos_y1 = self.node[self.edge[i][0]][1]
            pos_x2 = self.node[self.edge[i][1]][0]
            pos_y2 = self.node[self.edge[i][1]][1]
            plot([pos_x1,pos_x2],[pos_y1,pos_y2],color='k')
    
        pylab.axis('equal');
        pylab.ylabel('Y component (m)')
        pylab.xlabel('X component (m)')
        
    def add_area(self):

        self.area = [0] * self.n_ele
        
        m = [0]*3
        for i in range(self.n_ele):
            for j in range(3):
                m[j] = [self.node[self.ele[i][j]][0], self.node[self.ele[i][j]][1], 1.0]
            t1 = m[0][0]*(m[1][1]*m[2][2]-m[2][1]*m[1][2])
            t2 = m[0][1]*(m[1][0]*m[2][2]-m[2][0]*m[1][2])
            t3 = m[0][2]*(m[1][0]*m[2][1]-m[2][0]*m[1][1])
            self.area[i] = 0.5 * abs(t1-t2+t3)
            
    def add_cent(self):
        ''' Adds centroids to the mesh in the mesh.cent field.

            Call example : mesh.add_cent
            
            1 - A mesh must already exists to call this function, but the edges
                are not necessary
        '''
        from numpy import array
        
        self.cent = [0] * self.n_ele
        redo = 1
        count = 0
        while redo == 1:
            cent_count = 0
            redo = 0
            
            ele = array(self.ele) # TO BE FIXED
            node = array(self.node) # TO BE FIXED
            for i in range(int(self.n_ele)):
                self.cent[i] = [0] * 2
                for j in range(3):
                    self.cent[i][0] += 1./3 * self.node[self.ele[i][j]][0]
                    self.cent[i][1] += 1./3 * self.node[self.ele[i][j]][1]
                
                # DEPLACE node (protection against singularities)
                if self.cent[i][1] == self.node[self.ele[i][0]][1]:
                    self.node[self.ele[i][0]][1] += 0.001 * (max(node[list(ele[i,:]),1])-min(node[list(ele[i,:]),1]))
                    redo = 1
                    cent_count += 1
                elif self.cent[i][1] == self.node[self.ele[i][1]][1]:
                    self.node[self.ele[i][1]][1] += 0.001 * (max(node[list(ele[i,:]),1])-min(node[list(ele[i,:]),1]))
                    redo = 1
                    cent_count += 1
                elif self.cent[i][1] == self.node[self.ele[i][2]][1]:
                    self.node[self.ele[i][2]][1] += 0.001 * (max(node[list(ele[i,:]),1])-min(node[list(ele[i,:]),1]))
                    redo = 1
                    cent_count += 1
            
            #if cent_count > 0:
                #print '  --> Warning :',cent_count,'centroid(s) moved'
                
            count += redo
            if count == 10:
                print ('  --> It is highly recommanded to change the mesh used')
        
        
    def translate(self, geom):
        x = geom[0]
        y = geom[1]
        
        for i in range(self.n_node):
            self.node[i][0] += x
            self.node[i][1] += y
            
        if self.cent != None:
            for i in range(self.n_ele):
                self.cent[i][0] += x
                self.cent[i][1] += y
                
    #---------------------------------------------------------------------------
    # Rotate Geometry
    #---------------------------------------------------------------------------
    def rotate(self, geom):
        ''' Rotate the geometry by a given angle.
        
            Call example : mesh.rotate(pi/2)
            
            1 - Geometry must already exists before this
            2 - Mesher can be called after this
        '''
        from math import cos, sin        
        
        angle = geom[0]
        pos = geom[1]

        for i in range(len(self.node)):
            a = (self.node[i][0]-pos[0])*cos(angle) -(self.node[i][1]-pos[1])*sin(angle)+ pos[0]
            b = (self.node[i][0]-pos[0])*sin(angle) +(self.node[i][1]-pos[1])*cos(angle)+ pos[1]
            self.node[i][0] = a
            self.node[i][1] = b
            
        for i in range(len(self.ele)):
            a = (self.cent[i][0]-pos[0])*cos(angle) -(self.cent[i][1]-pos[1])*sin(angle)+ pos[0]
            b = (self.cent[i][0]-pos[0])*sin(angle) +(self.cent[i][1]-pos[1])*cos(angle)+ pos[1]
            self.cent[i][0] = a
            self.cent[i][1] = b
        
    #---------------------------------------------------------------------------
    # Find element associated to point
    #---------------------------------------------------------------------------    
    def find_element(self,position):
        ''' Function that finds in wich element a point is.
        
            INPUTS :
                - mesh object --> Contains all information on mesh
                - position --> Position verified
                
            OUTPUT :
                - elem --> Index of element where the point is. If it is -1, it
                           means the point is not inside the mesh.
        '''
        
        from numpy import array

        position = array(position)

        elem = -1        
        for i in range(self.n_ele):
            a = array(self.node[self.ele[i][0]])
            b = array(self.node[self.ele[i][1]])
            c = array(self.node[self.ele[i][2]])
            if inside_triangle(a,b,c,position):
                elem = i
        
        return elem

################################################################################
# Find if a point is inside a triangle
################################################################################
def inside_triangle(a,b,c,p):
    ''' Find if a point is inside a given triangle.
    
        INPUTS : a, b, c --> Three lists giving the position of three vertices.
                       p --> Point verified.
        
        OUTPOUT : boolean operator (1 = inside, 2 = outside)
    '''

    from numpy import dot
    # compute vectors
    v0 = c-a
    v1 = b-a
    v2 = p-a

    # compute dot products
    dot00 = dot(v0,v0)
    dot01 = dot(v0,v1)
    dot02 = dot(v0,v2)
    dot11 = dot(v1,v1)
    dot12 = dot(v1,v2)
    
    # compute barycentric coordinates
    invDenom = 1.0 / (dot00*dot11 - dot01*dot01)
    u = (dot11*dot02 - dot01*dot12)*invDenom
    v = (dot00*dot12 - dot01*dot02)*invDenom
    
    # verification
    return (u>=0) and (v>=0) and (u+v<1)