
'''
Mesh2D functionalities
    - For MeshPy
    - For Regular and stand-alone meshes
'''

from Samia.Mesh.Mesh2D.mesh_py_2D import mesh_py_2D
from Samia.Mesh.Mesh2D.mesh_reg_2D import mesh_reg_2D