# -*- coding: utf-8 -*-
"""
Created on Thu Aug 02 13:56:04 2012

@author: Roi des Marios
"""

from Samia.Mesh.Mesh2D.mesh_2D import mesh2D

class mesh_reg_2D(mesh2D):
    '''kk'''
    def __init__(self):
        mesh2D.__init__(self)
           
    def rectangle(self, geometry, n):
        '''craps'''
        
        from math import sqrt, ceil
        
        h = geometry[1][1]-geometry[1][0]
        w = geometry[0][1]-geometry[0][0]
        
        xc = (geometry[0][1]+geometry[0][0])/2.0
        yc = (geometry[1][1]+geometry[1][0])/2.0
        
        area = h*w
        optimal_dim = sqrt(area/float(0.5*n))
        
        n_h = int(ceil(h/optimal_dim))
        n_w = int(ceil(w/optimal_dim))
        
        x_step = float(w) / n_w
        y_step = float(h) / n_h
        
        # Définition des noeuds ----------------------------
        self.n_node = (n_w+1) * (n_h+1)
        
        self.node = [0] * self.n_node
     
        for i in range(n_w+1):
            val_x = i*x_step
            for j in range(n_h+1):
                val_y = j*y_step
                self.node[i*(n_h+1) + j] = [val_x+xc-0.5*w, val_y+yc-0.5*h]
        
        # Définition des éléments ------------------------------------------------------------------
        self.n_ele = 2 * n_w * n_h
        self.ele = [0] * self.n_ele
        for i in range(n_w):
            for j in range(n_h):                        
                self.ele[2*i*n_h + 2*j] = [i*(n_h+1) + j, (i+1)*(n_h+1) + j, i*(n_h+1) + j+1]
                self.ele[2*i*n_h + 2*j+1] = [(i+1)*(n_h+1) + j, (i+1)*(n_h+1) + j +1, i*(n_h+1) + j+1]
    
        self.section = [0] * len(self.ele)
        self.add_area()
        self.add_cent()
        self.create_edges()


    def curved_rectangle(self, r_in, r_out, angle, n):
        '''
        '''
        from math import sin, cos
        h = r_out - r_in
        yc = (r_out + r_in) / 2.0
        w = angle * yc
        
        posx = [-w/2.0, w/2.0]
        posy = [yc - h/2.0, yc + h/2.0]
    
        self.rectangle([posx, posy], n)
        
        for i in range(self.n_node):
            ratio = 2*self.node[i][1]/(r_in+r_out)
            l = self.node[i][0]*ratio
            angle = l/self.node[i][1]
            self.node[i][0] = self.node[i][1]*sin(angle)
            self.node[i][1] = self.node[i][1]*cos(angle)
        
        for i in range(self.n_ele):
            ratio = 2*self.cent[i][1]/(r_in+r_out)
            l = self.cent[i][0]*ratio
            angle = l/self.cent[i][1]
            self.cent[i][0] = self.cent[i][1]*sin(angle)
            self.cent[i][1] = self.cent[i][1]*cos(angle)


    def empty_disk(self, n_ele, in_rad, out_rad):
        '''craps'''
        import numpy as np
        
        self.inner_radius = in_rad
        self.outer_radius = out_rad
        
        tot_area = np.pi*(self.outer_radius**2-self.inner_radius**2)
    
        sub_area = 2.*tot_area / n_ele
        
        optimal_dim = np.sqrt(sub_area)
        
        #choice of radius
        n_rad = int(np.ceil((self.outer_radius - self.inner_radius)/optimal_dim))
        rad = float(self.outer_radius-self.inner_radius)/n_rad
        
        #choice of angle
        ang_len = np.pi*(self.outer_radius+self.inner_radius)
        n_ang = int(np.ceil(ang_len/optimal_dim))
        ang = 2*np.pi/n_ang
            
        # node creation
        self.n_node = n_ang * (n_rad+1)
        self.node   = np.zeros([self.n_node,2])
        self.frontier = np.zeros(self.n_node)
        for i in range(int(n_ang)):
            for j in range(int(n_rad+1)):
                x = (self.inner_radius+j*rad)*np.cos(i*ang)
                y = (self.inner_radius+j*rad)*np.sin(i*ang)
                
                self.node[(n_rad+1)*i+j, 0] = x
                self.node[(n_rad+1)*i+j, 1] = y
                if j==0 or j==n_rad:
                    self.frontier[(n_rad+1)*i+j] = 1
    
        # element creation
        self.n_ele = int(2*n_ang*n_rad)
        self.ele = np.zeros([self.n_ele,3])
        
        for i in range(n_ang):
            for j in range(n_rad):
                # first element
                self.ele[2*n_rad*i + 2*j][0] = j+(n_rad+1)*i
                self.ele[2*n_rad*i + 2*j][1] = j+1+(n_rad+1)*i
                self.ele[2*n_rad*i + 2*j][2] = j+(n_rad+1)*(i+1)
                # second element
                self.ele[2*n_rad*i + 2*j+1][0] = j+1+(n_rad+1)*i
                self.ele[2*n_rad*i + 2*j+1][1] = j+1+(n_rad+1)*(i+1)
                self.ele[2*n_rad*i + 2*j+1][2] = j+(n_rad+1)*(i+1)
                
        # adjustement
        for j in range(2*n_rad):
            for k in range(3):
                if self.ele[self.n_ele-j-1][k]>self.n_node-1:
                    self.ele[self.n_ele-j-1][k] -= self.n_node
               
        self.section = [0] * len(self.ele)
        self.add_area()
        self.add_cent()
        self.create_edges()


        
        