# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MESH CONDUCTOR
This file contains the mesh_conductor class that generates meshes for saddle
coils and racetrack coils, and contains all the methods linked to the mesh.

Included methods are :
    
  - __init__          --> Initializes the object with zeros and empty lists
  - mesh_racetrack    --> Meshes a racetrack with given dimensions
  - mesh_saddle       --> Meshes a saddle coil with given dimensions
  - translate         --> Translates a given mesh
  - rotate            --> Rotates a given mesh
  - plot              --> Plots a 2D cut of a given mesh
  - add_cent          --> Adds the centroids to a mesh
  - create_edges      --> Creates the edges of the mesh
  - current_racetrack --> Adds current densities to a racetrack
  
Also, the element function is a support function used in mesh_racetrack.
    
-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation   : August 2012
Modification : September 2012
Written at : University of Houston
----------------------------------------------------------------------------'''

from Samia.Mesh.Mesh3D.mesh_3D import mesh3D

class mesh_reg_3D(mesh3D):
    
    #--------------------------------------------------------------------------
    # Initialization of an object
    #--------------------------------------------------------------------------
    def __init__(self):

        mesh3D.__init__(self)
        
        self.n_node = 0
        self.node = []
        self.n_ele = 0
        self.ele = []
        self.cent = []
        
    def block(self, height, width, length, n_ele):
        import numpy
        height = float(height)
        length = float(length)
        width = float(width)
        tot_vol = height*width*length
        sub_vol = 6*tot_vol/n_ele
        optimal_dim = sub_vol**(1./3)
        n_hei = int(numpy.ceil(height/optimal_dim))
        hei = float(height)/n_hei
        
        n_wid = int(numpy.ceil(width/optimal_dim))
        wid = float(width)/n_wid
        
        n_len = int(numpy.ceil(length/optimal_dim))
        leng = float(length)/n_len
        
    #------------------------------------------------------------------------------
    # Nodes creation
    #------------------------------------------------------------------------------
        self.n_node = (n_wid+1) * (n_len+1) * (n_hei+1)
        self.node   = [0] * self.n_node
    
        for k in range(n_hei+1):
        
            # second part of the racetrack (1st straight part) -------------------
            for j in range(n_wid+1):
                for i in range(n_len+1):
                    
                    # Getting node positions
                    x = i*leng
                    y = j*wid
                    z = k*hei
                    
                    # Placing positions at proper place
                    pos = i + j*(n_len+1) + k*(n_len+1)*(n_wid+1)
                    self.node[pos] = [x,y,z]
    
        #*******************
        # element creation *
        #*******************
        # The decomposition of the cubes into 6 tetrahedrons follows -->
        # https://en.wikipedia.org/wiki/Marching_tetrahedrons
        self.cube2tet(n_len+1, n_len, n_wid, n_hei)
        
        self.section = [0] * len(self.ele)
        self.create_edges()
        self.add_cent()
    
    def empty_cylinder(self, n_ele, geometry, *args):
        '''
        Constructor of the mesh class. Generates the nodes and elements of an
        empty cylinder or an arc of an empty cylinder.
    
        Call example : from back_iron_mesh import mesh
                       geometry = [3.0,1.0,2.0,[0,0,0]]
                       mesh = mesh(2000, geometry, 2, 2)
        
        INPUTS :
            - n_ele    : Wanted number of elements in the mesh. The mesh 
                         generator will try to stay close to that value.
            - p        : number of pair of poles from the source
            - option   : option on symmetries
                          0 --> no symmetries, mesh all geometry
                          1 --> z_symmetry, mesh upper part of the geometry
                          2 --> all symmetries, mesh upper part of an arc
            - geometry : geometry of the back_iron
                          geometry[0] --> height of the back_iron
                          geometry[1] --> inner radius
                          geometry[2] --> outer radius
                          geometry[3] --> center of the geometry
                          
        OUTPUTS :
            - mesh : mesh of the back_iron
                mesh.n_node --> number of nodes in the mesh
                mesh.node   --> list of nodes in the mesh, each item of the list
                                is the position of the node
                mesh.n_ele  --> number of elements in the mesh
                mesh.ele    --> list of elements in the mesh, each item of the
                                list points to three lines of mesh.node, they
                                are pointers to nodes
        '''
    
        import numpy as np
        
        height = geometry[0]
        inner_radius = geometry[1]
        outer_radius = geometry[2]
        center = geometry[3]
    
    
        #************************************
        # Choice of dimensions for the mesh *
        #************************************
        # Separation of symmetry cases
        if args:
            n = 2*args[0]
        else:
            n = 1
                
        tot_vol = (1.0/n)*np.pi*(outer_radius**2-inner_radius**2)*height        
        sub_vol = 6*tot_vol / n_ele
        optimal_dim = sub_vol**(1.0/3)
        
        # choice of height
        n_hei = int(np.ceil(height / optimal_dim))
        hei = height / n_hei
        
        #choice of radius
        n_rad = int(np.ceil((outer_radius - inner_radius)/optimal_dim))
        rad = (outer_radius-inner_radius)/n_rad
        
        #choice of angle
        if n==1:
            ang_len = np.pi*(outer_radius+inner_radius)
            n_ang = int(np.ceil(ang_len/optimal_dim))
            ang = 2*np.pi/n_ang
            max_ang = n_ang
            
        else:
            ang_len = (1.0/n)*np.pi*(outer_radius+inner_radius)
            n_ang = int(np.ceil(ang_len / optimal_dim))
            ang = 2*np.pi/(n*n_ang)
            max_ang = n_ang+1
    
        #*****************
        # Nodes creation *
        #*****************
        self.n_node = max_ang * (n_rad+1) * (n_hei + 1)
        self.node   = np.zeros([self.n_node,3])
    
        for k in range(n_hei+1):
            for i in range(max_ang):
                for j in range(n_rad+1):
                    x = (inner_radius+j*rad)*np.cos(i*ang) + center[0]
                    y = (inner_radius+j*rad)*np.sin(i*ang) + center[1]
                    z = 0.5*height - k*hei + center[2]
                    pos = k*max_ang*(n_rad+1)+i*(n_rad+1)+j
                    self.node[pos, 0] = x
                    self.node[pos, 1] = y
                    self.node[pos, 2] = z
                
        #*******************
        # element creation *
        #*******************
        # The decomposition of the cubes into 6 tetrahedrons follows -->
        # https://en.wikipedia.org/wiki/Marching_tetrahedrons
        self.n_ele = int(6*n_ang*n_rad*n_hei)
        self.ele = [0] * self.n_ele
        
        for k in range(n_hei):
            for i in range(n_ang):
                for j in range(n_rad):
                    
                    pos = 6*n_ang*n_rad*k + 6*n_rad*i + 6*j
                    
                    # first element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos] = e
    
                    # second element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos+1] = e
                    
                    # third element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos+2] = e
                    
                    # fourth element
                    jv = [j,j,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos+3] = e
                    
                    # fifth element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i,i]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos+4] = e
                    
                    # sixth element
                    jv = [j,j,j,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_rad, max_ang, iv,jv,kv)
                    self.ele[pos+5] = e
                    
        self.section = [0] * len(self.ele)       
        self.create_edges()
        self.add_cent()

            
    def racetrack(self, height, active_length, inner_radius, outer_radius, n_ele):
        ''' '''
        from math import pi, ceil, cos, sin
        #************************************
        # Choice of dimensions for the mesh *
        #************************************
        # Separation of symmetry cases
    
        rad_vol = pi*(outer_radius**2-inner_radius**2)*height
        tot_vol = rad_vol
    
        sub_vol = 6*tot_vol / n_ele
        optimal_dim = sub_vol**(1.0/3.0)
    
        #choice of height
        n_hei = 1
        hei = height / n_hei
    
        #choice of radius
        n_rad = int(ceil((outer_radius - inner_radius)/optimal_dim))
        rad = (outer_radius-inner_radius)/n_rad
    
        #choice of angle
        ang_len = 0.5*pi*(outer_radius+inner_radius)
        n_ang = int(ceil(ang_len/optimal_dim))
        ang = pi/n_ang
    
        #choice of length
        n_len = int(ceil(active_length/optimal_dim));
        n_len = int(round((n_len - 2)/5))
        #n_len = 1
        if n_len <= 0:
            n_len = 1
        length = active_length/n_len;
    
        #*****************
        # Nodes creation *
        #*****************
        self.n_node = 2*(n_ang + n_len) * (n_rad+1) * (n_hei + 1)
        self.node   = [0] * self.n_node
    
        for k in range(n_hei+1):
    
            # first part of the circle
            for i in range(n_ang+1):
                for j in range(n_rad+1):
                    x = (inner_radius + j*rad)*cos(i*ang)
                    y = (inner_radius + j*rad)*sin(i*ang) + 0.5*active_length
                    z = 0.5*height - k*hei
                    pos = 2*k*(n_ang+n_len)*(n_rad+1)+i*(n_rad+1)+j
                    self.node[pos] = [x, z, y]
    
            # second part of the circle
            for i in range(n_len-1):
                for j in range(n_rad+1):
                    x = -inner_radius-j*rad
                    y = 0.5*active_length - (i+1)*length
                    z = 0.5*height - k*hei
                    fac = (n_rad+1)*(n_ang+1)
                    pos = 2*k*(n_ang+n_len)*(n_rad+1)+i*(n_rad+1)+j + fac
                    self.node[pos] = [x, z, y]
    
            # third part of the circle
            for i in range(n_ang+1):
                for j in range(n_rad+1):
                    x = (inner_radius + j*rad)*cos(i*ang+pi)
                    y = (inner_radius + j*rad)*sin(i*ang+pi) - 0.5*active_length
                    z = 0.5*height - k*hei
                    fac = (n_rad+1)*(n_ang+n_len)
                    pos = 2*k*(n_ang+n_len)*(n_rad+1)+i*(n_rad+1)+j + fac
                    self.node[pos] = [x, z, y]
    
    
            for i in range(n_len-1):
                for j in range(n_rad+1):
                    x = inner_radius+j*rad;
                    y = -0.5*active_length + (i+1)*length
                    z = 0.5*height - k*hei
                    fac = (n_rad+1)*(2*n_ang+1+n_len)
                    pos = 2*k*(n_ang+n_len)*(n_rad+1)+i*(n_rad+1)+j + fac
                    self.node[pos] = [x, z, y]
    
        #*******************
        # element creation *
        #*******************
        # The decomposition of the cubes into 6 tetrahedrons follows -->
        # https://en.wikipedia.org/wiki/Marching_tetrahedrons
        n_around = 2*n_ang + 2*n_len
        self.n_ele = 6*n_around*n_rad*n_hei
        self.ele = [0] * self.n_ele
    
        for k in range(n_hei):
            for i in range(n_around):
                for j in range(n_rad):
    
                    pos = 6*n_around*n_rad*k + 6*n_rad*i + 6*j
    
                    # first element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos] = e
    
                    # second element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos+1] = e
    
                    # third element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos+2] = e
    
                    # fourth element
                    jv = [j,j,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos+3] = e
    
                    # fifth element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i,i]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos+4] = e
    
                    # sixth element
                    jv = [j,j,j,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_rad, n_around, iv,jv,kv)
                    self.ele[pos+5] = e
                    
        self.section = [0] * len(self.ele)
        self.add_cent()
        self.create_edges()

    
    def saddle(self, Ri, Ro, angle_in, angle_out, active_length, n_ele):
        ''' '''
        from math import sin, cos
        inner_radius = angle_in*(Ro+Ri)/2.0
        outer_radius = angle_out*(Ro+Ri)/2.0
        height = Ro - Ri
    
        self.racetrack(height, active_length, inner_radius, outer_radius, n_ele)
        
        self.translate([0,(Ri+Ro)/2.0,0])
    
    
        for i in range(self.n_node):
            ratio = 2*self.node[i][1]/(Ri+Ro)
            l = self.node[i][0]*ratio
            angle = l/self.node[i][1]
            self.node[i][0] = self.node[i][1]*sin(angle)
            self.node[i][1] = self.node[i][1]*cos(angle)
        
        
        for i in range(self.n_ele):
            ratio = 2*self.cent[i][1]/(Ri+Ro)
            l = self.cent[i][0]*ratio
            angle = l/self.cent[i][1]
            self.cent[i][0] = self.cent[i][1]*sin(angle)
            self.cent[i][1] = self.cent[i][1]*cos(angle)
            #self.J[i][1] = -self.J[i][0]*sin(angle)
            #self.J[i][0] = self.J[i][0]*cos(angle)

    
    def cube2tet(self, n_len_max, n_len, n_wid, n_hei):
        ''' TO DO '''
        self.n_ele = int(6*n_len*n_wid*n_hei)
        self.ele = [0] * self.n_ele
        
        for k in range(n_hei):
            for i in range(n_len):
                for j in range(n_wid):
                    
                    pos = 6*n_len*n_wid*k + 6*n_wid*i + 6*j
                    
                    # first element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos] = e
    
                    # second element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos+1] = e
                    
                    # third element
                    jv = [j,j+1,j+1,j+1]
                    iv = [i,i,i,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos+2] = e
                    
                    # fourth element
                    jv = [j,j,j,j+1]
                    iv = [i,i+1,i+1,i+1]
                    kv = [k,k,k+1,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos+3] = e
                    
                    # fifth element
                    jv = [j,j+1,j,j+1]
                    iv = [i,i+1,i,i]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos+4] = e
                    
                    # sixth element
                    jv = [j,j,j,j+1]
                    iv = [i,i,i+1,i+1]
                    kv = [k,k+1,k+1,k+1]
                    e = element(n_wid, n_len_max, iv,jv,kv)
                    self.ele[pos+5] = e

################################################################################
# Create an element
###############################################################################
def element(n_rad, max_ang, iv,jv,kv):
    ''' Generates the pointers to nodes for 1 element, according to element
        defined by the position of its nodes, iv, jv and kv.
     
     INPUTS :
     - n_rad   : number of elements in the radial direction
     - max_ang : number of elements in the angular direction for the full
                 circle. Add 1 for only an arc.
     - iv      : position of the 4 nodes in the angular direction (list)
     - jv      : position of the 4 nodes in the radial direction (list)
     - kv      : position of the 4 nodes in the height direction (list)
    '''
    e = [0] * 4
    
    # Position in the X-Y plane (empty circle or disk)
    # The modulo connects to the first nodes when the mesh is a full circle
    e[0] = jv[0] + (n_rad+1)*iv[0] % (max_ang*(n_rad+1))
    e[1] = jv[1] + (n_rad+1)*iv[1] % (max_ang*(n_rad+1))
    e[2] = jv[2] + (n_rad+1)*iv[2] % (max_ang*(n_rad+1))
    e[3] = jv[3] + (n_rad+1)*iv[3] % (max_ang*(n_rad+1))
    
    # Adds the effect of the height position of the nodes
    e[0] += max_ang*(n_rad+1)*kv[0]
    e[1] += max_ang*(n_rad+1)*kv[1]
    e[2] += max_ang*(n_rad+1)*kv[2]
    e[3] += max_ang*(n_rad+1)*kv[3]
    
    return e
    

'''-------------------------------- EOF ------------------------------------'''



