# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 02:21:56 2013

@author: Kevin
"""

class mesh3D():
    
    def __init__(self):
        
        # Materials
        self.n_sections = 1
        self.section = []

    #---------------------------------------------------------------------------
    # Adding Mesh to existing mesh
    #---------------------------------------------------------------------------
    def add_mesh(self, mesh_tmp):
        ''' Adds mesh to existing mesh.

            Call example : mesh.add_mesh(mesh_tmp)
            
            1 - A mesh must already exists to call this function, but the edges
                are not necessary
        '''
        self.n_sections += 1
        
        for i in range(mesh_tmp.n_ele):
            for k in range(4):
                mesh_tmp.ele[i][k] += self.n_node
                
        for i in range(mesh_tmp.n_node): 
            self.section.append(self.n_sections-1)
            self.node.append(mesh_tmp.node[i])
        
        for i in range(mesh_tmp.n_ele):
            self.ele.append(mesh_tmp.ele[i])
            self.cent.append(mesh_tmp.cent[i])
        
        self.n_ele += mesh_tmp.n_ele
        self.n_node += mesh_tmp.n_node
        
        self.add_cent()
        self.create_edges()
        
    def extrude_2D_mesh(self, n, z1, z2, mesh_2D):
        ''''''
    
        self.n_ele = 3*n*mesh_2D.n_ele
        self.n_node = (n+1)*mesh_2D.n_node
    
        self.ele = [0] * self.n_ele
        self.node = [0] * self.n_node
        
        for i in range(n+1):
            z_pos = z1 + i*float(z2-z1)/n
            for j in range(mesh_2D.n_node):
                self.node[j+i*mesh_2D.n_node] = [mesh_2D.node[j][0],mesh_2D.node[j][1],z_pos]
                
        for l in range(n):
            nm1 = l*mesh_2D.n_node
            nm2 = (l+1)*mesh_2D.n_node
            for i in range(mesh_2D.n_ele):
                ele = mesh_2D.ele[i]
                self.ele[3*i + l*3*mesh_2D.n_ele] = [ele[0]+nm1, ele[1]+nm1, ele[2]+nm1, ele[0]+nm2]
                self.ele[3*i+1+l*3*mesh_2D.n_ele] = [ele[2]+nm1, ele[0]+nm2, ele[1]+nm2, ele[2]+nm2]
                self.ele[3*i+2+l*3*mesh_2D.n_ele] = [ele[1]+nm1, ele[2]+nm1, ele[0]+nm2, ele[1]+nm2]
    
        self.section = [0] * len(self.ele)
        self.create_edges()
        self.add_cent()
    
    def find_element(self,position):
        ''' Function that finds in wich element a point is.
        
            INPUTS :
                - mesh object --> Contains all information on mesh
                - position --> Position verified
                
            OUTPUT :
                - elem --> Index of element where the point is. If it is -1, it
                           means the point is not inside the mesh.
        '''
        
        from numpy import array
        elem = -1        
        for i in range(self.n_ele):
            a = array(self.node[self.ele[i][0]])
            b = array(self.node[self.ele[i][1]])
            c = array(self.node[self.ele[i][2]])
            d = array(self.node[self.ele[i][3]])
            if inside_tetrahedron(a,b,c,d,position):
                elem = i
        
        return elem
        
    #--------------------------------------------------------------------------
    # Translation
    #--------------------------------------------------------------------------
    def translate(self, vec):
        ''' Translatation of a mesh, given a displacement vector.
        
            vec --> vector giving (dep_x, dep_y, dep_z)
        '''
        
        for i in range(self.n_node):
            self.node[i][0] += vec[0]
            self.node[i][1] += vec[1]
            self.node[i][2] += vec[2]
        
        for i in range(self.n_ele):
            self.cent[i][0] += vec[0]
            self.cent[i][1] += vec[1]
            self.cent[i][2] += vec[2]
    
    #--------------------------------------------------------------------------
    # Rotation of the mesh
    #--------------------------------------------------------------------------
    def rotate(self, rot):
        ''' Makes the rotation of a given mesh given 3 angles of rotation
            around the 3 axis.
            
            rot --> vector giving (angle_x, angle_y, angle_z)
        '''    
        
        from math import sin, cos
        
        mat = [[cos(rot[1])*cos(rot[2]),
                -cos(rot[0])*sin(rot[2])+sin(rot[0])*sin(rot[1])*cos(rot[2]),
                sin(rot[0])*sin(rot[2]) + cos(rot[0])*sin(rot[1])*cos(rot[2])],
               [cos(rot[1])*sin(rot[2]),
                cos(rot[0])*cos(rot[2])+sin(rot[0])*sin(rot[1])*sin(rot[2]),
                -sin(rot[0])*cos(rot[2])+cos(rot[0])*sin(rot[1])*sin(rot[2])],
               [-sin(rot[1]),
                sin(rot[0])*cos(rot[1]),
                cos(rot[0])*cos(rot[1])]]
    
        import copy
        for i in range(self.n_node):
            tmp = copy.copy(self.node[i])
            for k in range(3):
                self.node[i][k] = mat[k][0]*tmp[0] + mat[k][1]*tmp[1] + mat[k][2]*tmp[2]
         
        for i in range(self.n_ele):
            tmp_c = copy.copy(self.cent[i])
            #tmp_J = copy.copy(self.J[i])
            for k in range(3):
                self.cent[i][k] = mat[k][0]*tmp_c[0] + mat[k][1]*tmp_c[1] + mat[k][2]*tmp_c[2]
                #self.J[i][k] = mat[k][0]*tmp_J[0] + mat[k][1]*tmp_J[1] + mat[k][2]*tmp_J[2]


    #--------------------------------------------------------------------------
    # Plot of the mesh
    #--------------------------------------------------------------------------
    def plot(self, height, axis):
        '''Function to plot a 2D cut of the mesh, given a height and an axis
           
           Call examples :
               - mesh.plot(0, 'Y')
               - mesh.plot(2, 'Z')
           
           INPUTS : 
               - axis : axis of the plane wanted, can be : 'Z' or 'z'
                                                           'Y' or 'y'
                                                           'X' or 'x'
               - height : height value wanted for the plot
                            
           Warning : edges must exist for the mesh to be plotted
        '''
        from Samia.Mesh.Mesh3D.mesh2D_from3D import gen_2D_mesh, mesh2D_plot
    
        [mesh2D, _useless_val_] = gen_2D_mesh(self,height,axis,None)
        mesh2D_plot(mesh2D, axis, height)
    
    
    #--------------------------------------------------------------------------
    # Creating Centroid
    #--------------------------------------------------------------------------
    def add_cent(self):
        '''Function to create centroids of all elements.
           
           Call example : mesh.add_cent()
           
           OUTPUTS :
               - mesh.cent : matrix of dimension (n_eleX3) giving coodinates of
                             centroids of all elements
        '''
        #from numpy import zeros        
        
        #self.cent = zeros((self.n_ele, 3))
        self.cent = [0] * self.n_ele
        for k in range(self.n_ele):
            self.cent[k] = [0]*3
        for k in range(4):
            for i in range(int(self.n_ele)):
                self.cent[i][0] += 1./4 * self.node[int(self.ele[i][k])][0]
                self.cent[i][1] += 1./4 * self.node[int(self.ele[i][k])][1]
                self.cent[i][2] += 1./4 * self.node[int(self.ele[i][k])][2]
    

    #--------------------------------------------------------------------------
    # Create Edges
    #--------------------------------------------------------------------------    
    def create_edges(self):
        '''Function to create edges. The code behind this function is written
           in the C programming language.
           The compiled file is the Edges3D function in the Edges3D library.
           
           OUTPUTS :
               - mesh.n_edge : number of edges in the mesh
               - mesh.edge : edges in the mesh, it is a matrix of dimenstion
                             (n_edgeX2), linking the nodes to the edges
               - mesh.ele2edge : matrix of dimension (n_eleX6) linking all
                                 all elements with their relative edges
        '''
        
        from Samia.Mesh.Edges import CreateEdges3D
        from numpy import array
        
        # creating elements
        ele = list(self.ele)
        for i in range(self.n_ele):
            ele[i] = list(ele[i])
        t = CreateEdges3D(ele)
        
        # extracting data
        self.edge = array(t[0])
        self.ele2edge = array(t[1])
        self.n_edge = t[2]
    

    
###############################################################################
# Find if a point is inside a tetrahedron
###############################################################################
def inside_tetrahedron(a,b,c,d,p):
    ''' Find if a point is inside a given tetrahedron.
    
        INPUTS : a, b, c, d --> lists giving the position of four vertices.
                          p --> Point verified
        
        OUTPOUT : boolean operator (1 = inside, 2 = outside)
    '''
    
    from numpy.linalg import det

    # Compute determinants
    D0 = det([[a[0],a[1],a[2],1.0], \
              [b[0],b[1],b[2],1.0], \
              [c[0],c[1],c[2],1.0], \
              [d[0],d[1],d[2],1.0]])
              
    D1 = det([[p[0],p[1],p[2],1.0], \
              [b[0],b[1],b[2],1.0], \
              [c[0],c[1],c[2],1.0], \
              [d[0],d[1],d[2],1.0]])
              
    D2 = det([[a[0],a[1],a[2],1.0], \
              [p[0],p[1],p[2],1.0], \
              [c[0],c[1],c[2],1.0], \
              [d[0],d[1],d[2],1.0]])
              
    D3 = det([[a[0],a[1],a[2],1.0], \
              [b[0],b[1],b[2],1.0], \
              [p[0],p[1],p[2],1.0], \
              [d[0],d[1],d[2],1.0]])
              
    D4 = det([[a[0],a[1],a[2],1.0], \
              [b[0],b[1],b[2],1.0], \
              [c[0],c[1],c[2],1.0], \
              [p[0],p[1],p[2],1.0]])

    # if all determinants have the same sign, the point is inside
    if D0 >= 0:
        return (D1>=0 and D2>=0 and D3>=0 and D4>=0)
    else:
        return (D1<0 and D2<0 and D3<0 and D4<0)
        
        
'''----------------- End of back_iron_mesh.py file -------------------------'''