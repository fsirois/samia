# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MESH2D GENERATION OUT OF A 3D ONE
This set of functions creates a 2D mesh from a 3D one. A plane (belonging to X,
Y, Z) must be provided with the 'height' at which the mesh as to be generated.
The value of a function, given on the element of the 3D mesh can be reported to
the 2D mesh, allowing a 2D view of a solution on a 3D mesh.

-------------------------------------------------------------------------------
INPUTS :
    - mesh --> 3-D mesh of the geometry (including elements and nodes)
    - axis --> axis to plot the mesh (can be 'X','x','Z,'z','Y' or 'y')
    - z_val --> 'height' value to create the 2-D mesh
    - sol --> solution object from GFUN solution
              if sol is set to None, only the mesh will be taken into account

OUTPUTS :
    - mesh2D --> 2 dimensional mesh created (has 3 and 4 sides polygons)
    - sol2D --> new solution object, but for the 2D mesh

-------------------------------------------------------------------------------
FUNCTIONS
    - gen_2D_mesh    --> generates the 2D mesh
    - rec_node_order --> reorganize the node order in the 2D element list in
                         order the 4 side elements are anticlockwise
    - add_value   --> Adds a solution to the 2D mesh from the 3D one
    - mesh2D_plot --> plots the 2D-Mesh
    - sol2D_plot  --> plots the 2D-Solution on the 2D-Mesh
    
-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation date : July 15, 2011
Modification date : August 4, 2011
Created at : Advanced Magnet Lab
----------------------------------------------------------------------------'''

###############################################################################
#
###############################################################################
def gen_2D_mesh(mesh , z_val , axis, sol):
    '''
    This function generates a 2D mesh out of a 3D-one. It also includes a
    sol2D object to include the solution transfered to this mesh.
    
    INPUTS :
        - mesh --> 3-D mesh of the geometry (including elements and nodes)
        - axis --> axis to plot the mesh (can be 'X','x','Z,'z','Y' or 'y')
        - z_val --> 'height' value to create the 2-D mesh
        - sol --> solution object from GFUN solution
                  if sol is set to None, only the mesh will be considered
    
    OUTPUTS :
        - mesh2D --> 2 dimensional mesh created (has 3 and 4 sides polygons)
        - sol2D --> 2 dimensional solution object created
    '''
    import numpy as np
    import math

    # classes definition
    class mesh2D:
        '''Structure class to save mesh2D data'''
    
    class sol2D:
        '''Structure class to save sol2D data'''
        
    #--------------------------------------------------------------------------
    # Initialization
    #--------------------------------------------------------------------------
    
    # values initialization
    tolerance = 10**-4 # if the element does not go through the plane more
                       # than tolerance, it will not considered
    counter_ele = 0    # counter of elements in the 2D mesh
    counter_node = 0   # counter of nodes in the 2D mesh
    mesh2D.node = None # sets the node list to None
    mesh2D.ele = None  # sets the element list to None
    sol2D.Hx = None    # sets a field of the solution to None
    
    nextv = [0,1,2,0,1] # useful vector
    
    # choosing axis
    if axis == 'Z' or axis == 'z':
        ax = 2
    elif axis == 'Y' or axis == 'y':
        ax = 1
    elif axis == 'X' or axis == 'x':
        ax = 0
    else:
        print ('Wrong choice for the AXIS')
    
    #--------------------------------------------------------------------------
    # Generating 2D mesh at z = height
    #--------------------------------------------------------------------------
    # loop on all 3D elements
    for k in range(mesh.n_ele):

        # z-values for the given 3D element
        z_node = [0] * 4
        for i in range(4):
            z_node[i] = mesh.node[mesh.ele[k][i]][ax]

        #----------------------------------------------------------------------
        # The z-plane passes through the element
        #----------------------------------------------------------------------
        if (max(z_node)-z_val) > tolerance and (z_val-min(z_node)) > tolerance:
            
            # find which edges pass through z-plane
            edges = [0] * 6
            nodes = [0] * 6
            for i in range(6):
                edges[i] = mesh.ele2edge[k,i]
                nodes[i] = [mesh.edge[edges[i]][0],mesh.edge[edges[i]][1]]
                    
            counter_edge = 0 # number of edges passing through z-plane
            for i in range(6):
                
                pos_a = mesh.node[nodes[i][0]][ax] - z_val
                pos_b = mesh.node[nodes[i][1]][ax] - z_val
                
                if (pos_a>=0 and pos_b <= 0) or (pos_a <= 0 and pos_b >= 0):
                    
                    # number of edges passing through z-plane
                    counter_edge = counter_edge + 1
                
                    x_val = mesh.node[mesh.edge[edges[i]][0]][nextv[ax+1]]
                    y_val = mesh.node[mesh.edge[edges[i]][0]][nextv[ax+2]]
                    if (mesh.node[mesh.edge[edges[i]][0]][ax] - mesh.node[mesh.edge[edges[i]][1]][ax]) != 0:
                        
                        slope = (mesh.node[mesh.edge[edges[i]][0]][ax]-z_val) / \
                        (mesh.node[mesh.edge[edges[i]][0]][ax] - mesh.node[mesh.edge[edges[i]][1]][ax])
                        
                        x_val += -slope*(mesh.node[mesh.edge[edges[i]][0]][nextv[ax+1]]-mesh.node[mesh.edge[edges[i]][1]][nextv[ax+1]])
                        y_val += -slope*(mesh.node[mesh.edge[edges[i]][0]][nextv[ax+2]]-mesh.node[mesh.edge[edges[i]][1]][nextv[ax+2]])
                        
                        
                    # Adding node ---------------------------------------------
                    # test on wether or not the node exists
                    v_test = None
                    if mesh2D.node != None:
                        for n_test in range(len(mesh2D.node)):
                            val1 = math.fabs(mesh2D.node[n_test][0]-x_val)<tolerance
                            val2 = math.fabs(mesh2D.node[n_test][1]-y_val)<tolerance
                            
                            if val1+val2 == 2:
                                v_test = n_test
                    
                    if v_test == None: # does not exist
                        if mesh2D.node == None:
                            mesh2D.node = [[x_val,y_val]]
                            mesh2D.ele = [[counter_node]]
                            counter_node = counter_node + 1
                        else:
                            mesh2D.node.append([x_val,y_val])
                            if counter_edge==1:
                                mesh2D.ele.append([counter_node])
                                counter_node = counter_node + 1
                            else:
                                mesh2D.ele[counter_ele].append(counter_node)
                                counter_node = counter_node + 1
                    else: # already exists
                        if counter_edge==1:
                            mesh2D.ele.append([v_test])
                        else:
                            add = 0
                            for m in range(len(mesh2D.ele[counter_ele])):
                                add = add + (mesh2D.ele[counter_ele][m]==v_test)
                            if add==0:
                                mesh2D.ele[counter_ele].append(v_test)
                            else:
                                counter_edge = counter_edge - 1
            
            # if a 3D solution is given, add it to the 2D one
            if sol != None:
                sol2D = add_value(sol, sol2D, axis, k)    
            counter_ele = counter_ele + 1
            
        #----------------------------------------------------------------------
        # The Z-plane passes ON an element face
        #----------------------------------------------------------------------           
        elif ((math.fabs(z_node[0]-z_val)<tolerance) + (math.fabs(z_node[1]-z_val)<tolerance) + \
             (math.fabs(z_node[2]-z_val)<tolerance) + (math.fabs(z_node[3]-z_val)<tolerance))==3:
            ele_temp = [0] * 3
            point_place = [0] * 3
            count_point = 0
            for m in range(4):
                if math.fabs(z_node[m]-z_val) < tolerance:
                    point_place[count_point] = m
                    count_point = count_point+1
            ele_exist = 0
            for m in range(3):
                x_val = mesh.node[mesh.ele[k][point_place[m]]][nextv[ax+1]]
                y_val = mesh.node[mesh.ele[k][point_place[m]]][nextv[ax+2]]
                
                #adding node---------------------------------------------------
                v_test = 0
                if mesh2D.node != None:
                    for mm in range(len(mesh2D.node)):
                        if math.fabs(mesh2D.node[mm][0]-x_val)<tolerance and \
                            math.fabs(mesh2D.node[mm][1]-y_val)<tolerance:
                            v_test = mm
                if v_test > 0: #it already exists
                    ele_temp[m] = v_test
                else: # it does not already exists
                    if mesh2D.node == None:
                        mesh2D.node = [[x_val, y_val]]
                    else:
                        mesh2D.node.append([x_val, y_val])
                    ele_temp[m] = counter_node
                    counter_node = counter_node + 1
                
            # checking if element already exists (since 2 3D elements have this
            # 2D face)
            if mesh2D.ele != None:
                for m in range(len(mesh2D.ele)):
                    val = 0
                    for jk in range(3):
                        if (ele_temp[0] == mesh2D.ele[m][jk]) or (ele_temp[1]==mesh2D.ele[m][jk]) \
                            or (ele_temp[2] == mesh2D.ele[m][jk]):
                                val = val+1
                    if val == 3:
                        ele_exist = 1
                    
            # if element does not exist, create it
            if ele_exist == 0:
                if mesh2D.ele == None:
                    mesh2D.ele = [ele_temp]
                else:
                    mesh2D.ele.append(ele_temp)
            
            # if a 3D solution is given, add the value to the sol2D
            if sol != None:
                sol2D = add_value(sol, sol2D, axis, k)
            counter_ele = counter_ele + 1
    
    # polish the mesh2D
    mesh2D.n_ele = len(mesh2D.ele)
    mesh2D.n_node = len(mesh2D.node)   
    mesh2D.ele = np.array(mesh2D.ele)
    mesh2D.node = np.array(mesh2D.node)

    # reorganize nodes in mesh.ele antriclockwise (the 4 sides ones)
    mesh2D = rec_node_order(mesh2D)
    
    return mesh2D, sol2D        

###############################################################################
# Organize the nodes anticlockwise
###############################################################################   
def rec_node_order(mesh2D):
    ''' Function organizing nodes in a 2D mesh.
        For the mesh to be properly plotted, the 4-sides elements must be
        modified for their vertices to be anti-clockwise organized.
        
        Call example mesh2D = rec_node_order(mesh2D)
        
        1 - The 2-D mesh must be already created for this function to be used.
        2 - MUST be used before the plotting of this mesh
    '''
    
    from numpy import cross    
    
    # loop on all elements
    for i in range(mesh2D.n_ele):
        
        # if element is a rectangle
        if len(mesh2D.ele[i]) == 4:
            
            # nodes for element
            nodes = mesh2D.ele[i]
            
            #------------------------------------------------------------------
            # Placing second node
            #------------------------------------------------------------------
            # First node remains the first since it had no importance
            out = 0
            count = 0
            while out == 0:
                vec1 = [mesh2D.node[nodes[1],0] - mesh2D.node[nodes[0],0],
                        mesh2D.node[nodes[1],1] - mesh2D.node[nodes[0],1], 0]
                vec2 = [mesh2D.node[nodes[2],0] - mesh2D.node[nodes[0],0],
                        mesh2D.node[nodes[2],1] - mesh2D.node[nodes[0],1], 0]
                vec3 = [mesh2D.node[nodes[3],0] - mesh2D.node[nodes[0],0],
                        mesh2D.node[nodes[3],1] - mesh2D.node[nodes[0],1], 0]
                count = count+1
                prod1 = cross(vec1,vec2)
                prod2 = cross(vec1,vec3)
                if prod1[2]>=0 and prod2[2]>=0:
                    out = 1
                else:
                    if count==2:
                        out = 1
                        count = 1
                    temp = nodes[1+count]
                    nodes[1+count] = nodes[1]
                    nodes[1] = temp
                    
            #------------------------------------------------------------------
            # Placing third node
            #------------------------------------------------------------------
            # Placing third node also places fourth node
            vec1 = [mesh2D.node[nodes[2],0] - mesh2D.node[nodes[1],0],
                    mesh2D.node[nodes[2],1] - mesh2D.node[nodes[1],1], 0]
            vec2 = [mesh2D.node[nodes[3],0] - mesh2D.node[nodes[1],0],
                    mesh2D.node[nodes[3],1] - mesh2D.node[nodes[1],1], 0]
            prod1 = cross(vec1,vec2)
            if prod1[2] < 0:
                temp = nodes[2]
                nodes[2] = nodes[3]
                nodes[3] = temp
            mesh2D.ele[i] = nodes
            
    return mesh2D

###############################################################################
# Add a solution to the 2D mesh out of the 3D one
###############################################################################
def add_value(sol, sol2D, axis, k):
    ''' 
        Adds all the solution values to the sol2D, including magnetic field,
        magnetic flux, magnetization and magnetic source field. For all those,
        the 2 components in the plane plotting are wrote in an X and a Y
        components. Also, the norm of these is included. Finally, the
        susceptibility value is saved also.
        
        Call example : sol2D = add_value(sol, sol2D, axis, k)
        
        INPUTS :
            - sol --> 3D solution with ALL fields
            - 2Dsol --> 2D incomplete solution
            - axis --> axis of the plane to be plotted
            - k --> 3D element where the solution is take to be put in the
                    new element of the 2D mesh
        
        1 - This function is only intended to be a support function for the
            function gen_2D_mesh
    '''
    import math
    
    # gets the x and y value for all fields, depending on the axis choosen
    if axis == 'Z' or axis == 'z':
        valHx = sol.Hx[k]
        valHy = sol.Hy[k]
        #valHcx = sol.Hc_x[k]
        #valHcy = sol.Hc_y[k]
        valBx = sol.Bx[k]
        valBy = sol.By[k]
        valMx = sol.Mx[k]
        valMy = sol.My[k]
        
    elif axis == 'Y' or axis == 'y':
        valHx = sol.Hz[k]
        valHy = sol.Hx[k]
        #valHcx = sol.Hc_z[k]
        #valHcy = sol.Hc_x[k]
        valBx = sol.Bz[k]
        valBy = sol.Bx[k]
        valMx = sol.Mz[k]
        valMy = sol.Mx[k]
        
    elif axis == 'X' or axis == 'x':
        valHx = sol.Hy[k]
        valHy = sol.Hz[k]
        #valHcx = sol.Hc_y[k]
        #valHcy = sol.Hc_z[k]
        valBx = sol.By[k]
        valBy = sol.Bz[k]
        valMx = sol.My[k]
        valMy = sol.Mz[k]
    
    # adds the susceptibility (independant of axis chosen)
    valchi = sol.xi[k]
    
    # if it is the first value to be put inside 2D solution
    if sol2D.Hx == None:
        sol2D.Hx = [valHx]
        sol2D.Hy = [valHy]
        sol2D.Hnorm = [math.sqrt(valHx**2+valHy**2)]
        
        #sol2D.Hc_x = [valHcx]
        #sol2D.Hc_y = [valHcy]
        #sol2D.Hc_norm = [math.sqrt(valHcx**2+valHcy**2)]
        
        sol2D.Bx = [valBx]
        sol2D.By = [valBy]
        sol2D.Bnorm = [math.sqrt(valBx**2+valBy**2)]
        
        sol2D.Mx = [valMx]
        sol2D.My = [valMy]
        sol2D.Mnorm = [math.sqrt(valMx**2+valMy**2)]
        
        sol2D.chi = [valchi]
    
    # if it is NOT the first value, append the new value
    else:
        sol2D.Hx.append(valHx)
        sol2D.Hy.append(valHy)
        sol2D.Hnorm.append(math.sqrt(valHx**2+valHy**2))
        
        #sol2D.Hc_x.append(valHcx)
        #sol2D.Hc_y.append(valHcy)
        #sol2D.Hc_norm.append(math.sqrt(valHcx**2+valHcy**2))
        
        sol2D.Bx.append(valBx)
        sol2D.By.append(valBy)
        sol2D.Bnorm.append(math.sqrt(valBx**2+valBy**2))
        
        sol2D.Mx.append(valMx)
        sol2D.My.append(valMy)
        sol2D.Mnorm.append(math.sqrt(valMx**2+valMy**2))
        
        sol2D.chi.append(valchi)
        
    return sol2D

###############################################################################
# Plots the 2D mesh
###############################################################################          
def mesh2D_plot(mesh2D , axis , z_val):
    ''' This function plots the 2D mesh created.
    
        Call example : mesh2D_plot(mesh2D)
    '''
    from matplotlib.pyplot import figure, plot
    import pylab    
    
    figure()
    for i in range(len(mesh2D.ele)):
        if len(mesh2D.ele[i])==3:
            nex = [1,2,0]
            for k in range(3):
                pos_x1 = mesh2D.node[mesh2D.ele[i][k]][0]
                pos_y1 = mesh2D.node[mesh2D.ele[i][k]][1]
                pos_x2 = mesh2D.node[mesh2D.ele[i][nex[k]]][0]
                pos_y2 = mesh2D.node[mesh2D.ele[i][nex[k]]][1]
                plot([pos_x1,pos_x2],[pos_y1,pos_y2],color='k')
        elif len(mesh2D.ele[i])==4:
            nex = [1,2,3,0]
            for k in range(4):
                pos_x1 = mesh2D.node[mesh2D.ele[i][k]][0]
                pos_y1 = mesh2D.node[mesh2D.ele[i][k]][1]
                pos_x2 = mesh2D.node[mesh2D.ele[i][nex[k]]][0]
                pos_y2 = mesh2D.node[mesh2D.ele[i][nex[k]]][1]
                plot([pos_x1,pos_x2],[pos_y1,pos_y2],color='k')
            
    pylab.axis('equal');
    if axis == 'Z' or axis == 'z':
        pylab.ylabel('Y-axis (m)')
        pylab.xlabel('X-axis (m)')
        title = 'XY Plane Mesh, Z = '
    elif axis == 'Y' or axis == 'y':
        pylab.ylabel('X-axis (m)')
        pylab.xlabel('Z-axis (m)')
        title = 'ZX Plane mesh, Y = '
    elif axis == 'X' or axis == 'x':
        pylab.ylabel('Z-axis (m)')
        pylab.xlabel('Y-axis (m)')
        title = 'YZ Plane mesh, X = '
    title += str(z_val)
    pylab.title(title)

###############################################################################
# Plots one of the solutions on the 2D mesh
###############################################################################
def sol2D_plot(mesh2D, sol2D, axis, z_val, choice):
    ''' This function plots the 2D solution created.
    
        Call example : sol2D_plot(mesh2D, sol2D, axis, z_val, choice)
        
        INPUTS :
            - mesh2D --> 2D mesh generated by gen_2D_mesh
            - sol2D --> 2D solution generated by gen_2D_mesh
            - axis --> axis of the plane the mesh2D is created
            - z_val --> height of the plane on the mesh2D is created
            - choice --> choice of what field to be plotted
                         choices are : 'H'   (magnetic field)
                                       'Hc'  (source magnetic field)
                                       'M'   (magnetization)
                                       'B'   (magnetic flux density)
                                       'CHI' (susceptibility)
    '''
    from numpy import transpose, zeros
    from matplotlib.pyplot import colorbar, quiver, figure
    from matplotlib.patches import Polygon
    from matplotlib.collections import PatchCollection
    import pylab
    
    # depending on choice, get the good value to plot
    if choice == 'H':
        sol2Dn = sol2D.Hnorm
        sol2Dx = sol2D.Hx
        sol2Dy = sol2D.Hy
    #elif choice == 'Hc':
        #sol2Dn = sol2D.Hc_norm
        #sol2Dx = sol2D.Hc_x
        #sol2Dy = sol2D.Hc_y
    elif choice == 'M':
        sol2Dn = sol2D.Mnorm
        sol2Dx = sol2D.Mx
        sol2Dy = sol2D.My
    elif choice == 'B':
        sol2Dn = sol2D.Bnorm
        sol2Dx = sol2D.Bx
        sol2Dy = sol2D.By
    elif choice == 'CHI':
        sol2Dn = sol2D.chi
     
    # create the patch plot
    fig = figure()
    Z = transpose(sol2Dn)

    ax = fig.add_subplot(111)
    patches = []
    for x in range(int(mesh2D.n_ele)):
        
        # 3 sides elements
        if len(mesh2D.ele[x])==3:
            matrix = zeros((3,2))
            for i in range(3):
                matrix[i,:] = mesh2D.node[mesh2D.ele[x][i],:]
            polygon = Polygon(matrix, True)
            patches.append(polygon)
            
        # 4 sides elements
        elif len(mesh2D.ele[x])==4:
            matrix = zeros((4,2))
            for i in range(4):
                matrix[i,:] = mesh2D.node[mesh2D.ele[x][i],:]
            polygon = Polygon(matrix, True)
            patches.append(polygon)
            
        # 2 sides elements (bad elements)
        elif len(mesh2D.ele[x])==2:
            print ('Bad number of nodes for an element : 2')
            matrix = zeros((2,2))
            for i in range(2):
                matrix[i,:] = mesh2D.node[mesh2D.ele[x][i],:]
            polygon = Polygon(matrix, True)
            patches.append(polygon)
    p = PatchCollection(patches)
    p.set_array(Z)
    ax.add_collection(p)
    colorbar(p)
    
    # plot of arrows
    if choice != 'CHI':
        # quiver part
        x = zeros(mesh2D.n_ele)
        y = zeros(mesh2D.n_ele)
        for i in range(int(mesh2D.n_ele)):
            x[i] = sum(mesh2D.node[list(mesh2D.ele[i]),0])/len(mesh2D.ele[i])
            y[i] = sum(mesh2D.node[list(mesh2D.ele[i]),1])/len(mesh2D.ele[i])
        
        quiver(x, y, sol2Dx, sol2Dy , linewidth=0.1)

    # write plot information
    pylab.axis('equal')
    if axis == 'Z' or axis == 'z':
        pylab.ylabel('Y-axis (m)')
        pylab.xlabel('X-axis (m)')
        title = 'XY Plane solution, Z = '
    elif axis == 'Y' or axis == 'y':
        pylab.ylabel('X-axis (m)')
        pylab.xlabel('Z-axis (m)')
        title = 'ZX Plane solution, Y = '
    elif axis == 'X' or axis == 'x':
        pylab.ylabel('Z-axis (m)')
        pylab.xlabel('Y-axis (m)')
        title = 'YZ Plane solution, X = '
    title += str(z_val)
    title += '\n'
    title += choice
    pylab.title(title)

'''--------------------------------- EOF -----------------------------------'''

        