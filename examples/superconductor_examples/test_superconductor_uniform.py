# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
Created on Wed May 22 14:59:15 2013

-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation : May 2013
Modification : May 23rd, 2013
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh
from Samia.Superconductor import superconductor

from numpy import pi
from matplotlib.pylab import show

print ('-------------------------------------------------------------')
print ('--- Démonstration du module de calcul de champ magnétique ---')
print ('-------------------------------------------------------------')
mu0 = 4*pi*10**-7
#------------------------------------------------------------------------------
# Section 1 : Entrées
#------------------------------------------------------------------------------
# Paramètres de base
a = 0.15 # Demi-largeur et demi-hauteur de l'aimant et du supra
b = 0.05


# Supracondcuteur
nx_supra = 50
ny_supra = 20

#------------------------------------------------------------------------------
# Section 2 : Création de la géométrie
#------------------------------------------------------------------------------
Jc = 10**7

#------------------------------------------------------------------------------
# Section 3 : Solution du problème
#------------------------------------------------------------------------------
# Supraconducteur

geom = [[-a/2.0, a/2.0],[-b/2.0, b/2.0]]
supra = superconductor(geom, nx_supra, ny_supra)

By = 0.0
Bx = 0.0
# Descemte du supra -----------------------------------------------------------
Bm_x = [0] * len(supra.X_air)
Bm_y = [0] * len(supra.X_air)
for i in range(len(supra.X_air)):
    Bm_x[i] = [Bx] * len(supra.X_air[0])
    Bm_y[i] = [By] * len(supra.X_air[0])
    
supra.set_Jc(Jc)
supra.create_matrix()

supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle


supra.minimize()      # Minimisation de l'énergie
supra.set_B_old()

supra.plot_current() # Affichage du courant



supra.reset_current_field()
supra.reset_temp_current_field()

step = 0.1
n_step = 1

for i in range(n_step):
    
    By += step
    print ("Movement",i+1,"of",n_step)
    
    Bm_x = [0] * len(supra.X_air)
    Bm_y = [0] * len(supra.X_air)
    for i in range(len(supra.X_air)):
        Bm_x[i] = [Bx] * len(supra.X_air[0])
        Bm_y[i] = [By] * len(supra.X_air[0])
    
    supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle
    supra.minimize() # Minimisation de l'énergie 
    
    supra.set_B_old()

    supra.reset_current_field()
    supra.reset_temp_current_field()
    
supra.plot_current() # Affichage du courant
supra.plot_field()
    
for i in range(n_step):
    
    By -= step
    print ("Movement",i+1,"of",n_step)
    
    Bm_x = [0] * len(supra.X_air)
    Bm_y = [0] * len(supra.X_air)
    for i in range(len(supra.X_air)):
        Bm_x[i] = [Bx] * len(supra.X_air[0])
        Bm_y[i] = [By] * len(supra.X_air[0])
    
    supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle
    supra.minimize() # Minimisation de l'énergie 
    
    supra.set_B_old()

    supra.reset_current_field()
    supra.reset_temp_current_field()

supra.plot_current() # Affichage du courant
supra.plot_field()
show()


#--------------------------------- EOF ----------------------------------------






