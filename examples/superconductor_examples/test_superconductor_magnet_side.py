# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
Created on Wed May 22 14:59:15 2013

-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation : May 2013
Modification : May 23rd, 2013
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization
from Samia.Superconductor import superconductor

from numpy import pi, linspace, meshgrid, zeros

from matplotlib.pylab import show


print ('-------------------------------------------------------------')
print ('--- Démonstration du module de calcul de champ magnétique ---')
print ('-------------------------------------------------------------')

#------------------------------------------------------------------------------
# Section 1 : Entrées
#------------------------------------------------------------------------------
# Paramètres de base
b = 0.05 # Demi-largeur et demi-hauteur de l'aimant et du supra
d = 1.0*b # Distance supra - aimant
dc = 2.0*b
a = 3*b


# Aimant permanent (PM --> permanent magnet)
pm1_x1 = - 3*b/2.0      # [m] Position x de gauche
pm1_x2 = -b /2.0       # [m] Position x de droite

pm1_y2 = 0   # [m] Position y du haut
pm1_y1 = -b  # [m] Position y du bas

pm2_x1 = b/2.0      # [m] Position x de gauche
pm2_x2 = 3 * b /2.0       # [m] Position x de droite

pm2_y2 = 0   # [m] Position y du haut
pm2_y1 = -b  # [m] Position y du bas

max_area = 0.0001 # [m^2] Taille maximale d'un élément 
magnetization_x = 7.95*10**5      # [A/m] composante X
magnetization_y = 0.0 # [A/m] composante Y

# Supracondcuteur
nx_supra = 66
ny_supra = 22

#------------------------------------------------------------------------------
# Section 2 : Création de la géométrie
#------------------------------------------------------------------------------
# Aimant permanent
mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm1_x1, pm1_x2],[pm1_y1, pm1_y2]])
mesh_pm.mesh()
mesh_pm.plot()
M = [[magnetization_x, magnetization_y]] * mesh_pm.n_ele

# Supraconducteur

geom = [[-a/2.0, a/2.0],[dc, dc+b]]
supra = superconductor(geom, nx_supra, ny_supra)

# Adaptation du courant critique
print (-b/2.0)
Btx, Bty = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, [[-b/2.0+0.0000001]],[[-b/2.0]])

Jc = 2 * Btx[0][0] / (4*pi*10**-7) / a # ?
print (Jc)
supra.set_Jc(Jc)

supra.create_matrix()

mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm1_x1, pm1_x2],[pm1_y1, pm1_y2]])
mesh_pm.add_geom(mesh_pm.rectangle, [[pm2_x1,pm2_x2],[pm2_y1,pm2_y2]])
mesh_pm.mesh()

M = [0] * mesh_pm.n_ele
for i in range(mesh_pm.n_ele):
    
    if mesh_pm.cent[i][0]<0.0:
        M[i] = [magnetization_x, magnetization_y]
    else:
        M[i] = [-magnetization_x, magnetization_y]

#------------------------------------------------------------------------------
# Section 3 : Solution du problème
#------------------------------------------------------------------------------

# Descemte du supra -----------------------------------------------------------
Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # B de l'aimant

supra.set_B_source(Bm_x, Bm_y)
supra.set_B_old()

supra.translate([0.0, d-dc])

Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # B de l'aimant
supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle


supra.minimize()
#supra.minimize_energy()        # Minimisation de l'énergie

supra.plot_current() # Affichage du courant
supra.plot_field()
supra.reset_current_field()
supra.reset_temp_current_field()


moving_loop = [[0.01, 0.0],[-0.01, 0.0],[-0.01, 0.0],[0.01, 0.0],[0.01, 0.0],[-0.01, 0.0]]

for i in range(len(moving_loop)):
    print ("Movement",i+1,"of",len(moving_loop))
    # Remontée du supra -----------------------------------------------------------
    supra.translate(moving_loop[i])
    Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # Nouveau B source
    
    supra.set_B_old()
    supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle
    supra.minimize() # Minimisation de l'énergie 
    
    supra.set_B_old()
    supra.plot_current() # Affichage du courant
    supra.reset_current_field()
    supra.reset_temp_current_field()
    

show()


#--------------------------------- EOF ----------------------------------------






