# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
Created on Wed May 22 14:59:15 2013

-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation : May 2013
Modification : May 23rd, 2013
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization
from Samia.Superconductor import superconductor

from numpy import pi
from matplotlib.pylab import show

print ('-------------------------------------------------------------')
print ('--- Démonstration du module de calcul de champ magnétique ---')
print ('-------------------------------------------------------------')
mu0 = 4*pi*10**-7
#------------------------------------------------------------------------------
# Section 1 : Entrées
#------------------------------------------------------------------------------
# Paramètres de base
a = 0.05 # Demi-largeur et demi-hauteur de l'aimant et du supra
d = 0.15 # Distance supra - aimant

b = 0.05

# Aimant permanent (PM --> permanent magnet)
pm_x1 = -a      # [m] Position x de gauche
pm_x2 = a       # [m] Position x de droite

pm_y2 = 0   # [m] Position y du haut
pm_y1 = -2*a  # [m] Position y du bas
max_area = 0.0001 # [m^2] Taille maximale d'un élément 
magnetization_x = 0.0      # [A/m] composante X
magnetization_y = 7.95*10**5 # [A/m] composante Y

# Supracondcuteur
nx_supra = 40
ny_supra = 40

#------------------------------------------------------------------------------
# Section 2 : Création de la géométrie
#------------------------------------------------------------------------------
# Aimant permanent
mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm_x1, pm_x2],[pm_y1, pm_y2]])
mesh_pm.mesh()

M = [0] * mesh_pm.n_ele
for i in range(mesh_pm.n_ele):
    M[i] = [magnetization_x, magnetization_y]



# Adaptation du courant critique
Btx, Bty = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, [0],[pm_y2])

Jc = Bty[0] / (mu0 * a) 

#Jc = 2.81 * 10**6
#print Jc

#------------------------------------------------------------------------------
# Section 3 : Solution du problème
#------------------------------------------------------------------------------
# Supraconducteur

geom = [[-a, a],[d, d+2*b]]
supra = superconductor(geom, nx_supra, ny_supra)

# Descemte du supra -----------------------------------------------------------
Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # B de l'aimant

supra.set_Jc(Jc)
supra.create_matrix()

supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle


supra.minimize()      # Minimisation de l'énergie
supra.set_B_old()

supra.plot_current() # Affichage du courant

mesh_pm.plot()

supra.reset_current_field()
supra.reset_temp_current_field()

moving_loop = [[0.00, -0.05],[0.00, -0.05],[0.00, -0.05],
               [0.00, 0.05],[0.00, 0.05],[0.00, 0.05]]

for i in range(len(moving_loop)):
    print ("Movement",i+1,"of",len(moving_loop))
    # Remontée du supra -----------------------------------------------------------
    supra.translate(moving_loop[i])
    Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # Nouveau B source
    
    
    supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle
    supra.minimize() # Minimisation de l'énergie 
    
    supra.set_B_old()
    supra.plot_current() # Affichage du courant
    supra.reset_current_field()
    supra.reset_temp_current_field()

show()

'''
import cProfile
cProfile.run('supra.minimize(HX_source, HY_source)')
'''


#--------------------------------- EOF ----------------------------------------






