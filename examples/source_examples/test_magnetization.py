# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""

from Samia.Mesh import mesh
from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi

from Samia.Support.plot import plot_square_2val
from Samia.MagneticField.field_from_source import field_from_magnetization

r = 1.0
MY0 = -10.0**7
npts=50

x = list(linspace(0.5,2.0,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]


max_vol = 0.001
mesh3 = mesh(3, 'meshpy', max_vol)
mesh3.add_geom(mesh3.sphere, [r,[0,0,0]])
mesh3.mesh()
    
 # Creating magnetization ARRAY
M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [0, MY0, 0]

mesh3.node = mesh3.node.tolist()
mesh3.ele = mesh3.ele.tolist()
BX_3D, BY_3D, BZ = field_from_magnetization(mesh3.node, mesh3.ele, M, X, Y, Z)

mu0 = 4 * pi * 10**-7
Bth = [0] * len(x)
for i in range(len(x)):
    if x[i]<=r:
        Bth[i] = 2.0/3.0 * mu0 * MY0
    else:
        Bth[i] =  - mu0 * MY0 / (3 * x[i]**3)
        
figure()
p1, = plot(x,BY_3D[0],'k',linewidth=4)
p4, = plot(x,Bth,'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['Program','Analytic Equation '])
grid('on')



max_area = 0.005
mesh2 = mesh(2,'meshpy',max_area)
mesh2.circle([r , [0,0]])
mesh2.mesh()

# Creating magnetization ARRAY
M = [0] * mesh2.n_ele
for i in range(mesh2.n_ele):
    M[i] = [0, MY0]

x = list(linspace(0.5,2.0,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]

BX_2D, BY_2D = field_from_magnetization(mesh2.node, mesh2.ele, M, X, Y)


h = 100.0
mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(5, -h/2.0, h/2.0, mesh2)

 # Creating magnetization ARRAY
M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [0, MY0, 0]

BX_3D, BY_3D, BZ_3D = field_from_magnetization(mesh3.node, mesh3.ele, M, X, Y, Z)

figure()
p1, = plot(x,BY_3D[0],'k',linewidth=4)
p4, = plot(x,BY_2D[0],'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['Program 3D','Program 2D'])
grid('on')



show()



