# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""

from Samia.Mesh import mesh
from matplotlib.pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi

from Samia.Support.plot import plot_square_2val
from Samia.MagneticField.field_from_source import field_from_current

r = 1.0
J0 = 10.0**7

max_area = 0.005
mesh2 = mesh(2,'meshpy',max_area)
mesh2.circle([r , [0,0]])
mesh2.mesh()
mesh2.plot()

# On calcule une résultat selon une ligne donnée
npts=50

x = list(linspace(0.9,2.0,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]

I = J0 * pi * r**2

mu0 = 4 * pi * 10**-7
Bth = [0] * len(x)
for i in range(len(x)):
    Bth[i] = mu0 * I / (2 * pi * x[i])

J2D= [J0] * mesh2.n_ele
BX_2D, BY_2D = field_from_current(mesh2.node, mesh2.ele, J2D, X, Y)

figure()
p1, = plot(x,BY_2D[0],'k',linewidth=4)
p4, = plot(x,Bth,'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['Calculated 2D','Analytic'])
grid('on')

h = 100.0
mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(5, -h/2.0, h/2.0, mesh2)

print (mesh3.n_ele)
J = [0] * mesh3.n_ele
for i in range(len(J)):
    J[i] = [0.0, 0.0, J0]
    
BX_3D, BY_3D, BZ = field_from_current(mesh3.node, mesh3.ele, J, X, Y, Z)


figure()
p1, = plot(x,BY_3D[0],'k',linewidth=4)
p4, = plot(x,Bth,'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['Calculated 3D','Analytic Infinite Conductor'])
grid('on')


n_pts = 20
xv = linspace(-2, 2, n_pts)
yv = linspace(-2, 2, n_pts)
[X,Y] = meshgrid(xv,yv)
Z = zeros((len(X),len(X[0])))

BX, BY = field_from_current(mesh2.node, mesh2.ele, J2D, X, Y)
plot_square_2val(X, Y, BX, BY, '2D Field')

BX, BY, BZ = field_from_current(mesh3.node, mesh3.ele, J, X, Y, Z)
plot_square_2val(X, Y, BX, BY, '3D Field')

show()