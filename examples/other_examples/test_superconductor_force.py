# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
Created on Wed May 22 14:59:15 2013

-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation : May 2013
Modification : May 23rd, 2013
----------------------------------------------------------------------------'''


from Samia.MagneticField.field_from_source import field_from_magnetization
from Samia.MagneticField.method_of_moments import find_magnetization
from Samia.Superconductor import superconductor, mesh_superconductor
from Samia.Mesh import mesh
from Samia.Support.plot import plot_square_2val

from numpy import pi, linspace, meshgrid, zeros
from matplotlib.pylab import show

from Samia.Support.Force import maxwell_force, lorentz_force


#------------------------------------------------------------------------------
# Section 1 : Entrées
#------------------------------------------------------------------------------
# Paramètres de base
b = 0.05 # Demi-largeur et demi-hauteur de l'aimant et du supra
d = 1.0*b # Distance supra - aimant
dc = 2.0*b
a = 3*b


# Aimant permanent (PM --> permanent magnet)
pm1_x1 = - 3*b/2.0      # [m] Position x de gauche
pm1_x2 = -b /2.0       # [m] Position x de droite

pm1_y2 = 0   # [m] Position y du haut
pm1_y1 = -b  # [m] Position y du bas

pm2_x1 = b/2.0      # [m] Position x de gauche
pm2_x2 = 3 * b /2.0       # [m] Position x de droite

pm2_y2 = 0   # [m] Position y du haut
pm2_y1 = -b  # [m] Position y du bas

max_area = 0.00002 # [m^2] Taille maximale d'un élément 
magnetization_x = 7.95*10**5      # [A/m] composante X
magnetization_y = 0.0 # [A/m] composante Y

# Supracondcuteur
nx_supra = 66
ny_supra = 22

#------------------------------------------------------------------------------
# Section 2 : Création de la géométrie
#------------------------------------------------------------------------------
# Aimant permanent
mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm1_x1, pm1_x2],[pm1_y1, pm1_y2]])
mesh_pm.mesh()
mesh_pm.plot()
M = [[magnetization_x, magnetization_y]] * mesh_pm.n_ele

# Supraconducteur

geom = [[-a/2.0, a/2.0],[dc, dc+b]]
supra = superconductor(geom, nx_supra, ny_supra)

# Adaptation du courant critique
print (-b/2.0)
Btx, Bty = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, [[-b/2.0+0.0000001]],[[-b/2.0]])

Jc = 2 * Btx[0][0] / (4*pi*10**-7) / a # ?
print (Jc)
supra.set_Jc(Jc)

supra.create_matrix()

mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm1_x1, pm1_x2],[pm1_y1, pm1_y2]])
mesh_pm.add_geom(mesh_pm.rectangle, [[pm2_x1,pm2_x2],[pm2_y1,pm2_y2]])
mesh_pm.mesh()

M = [0] * mesh_pm.n_ele
for i in range(mesh_pm.n_ele):
    
    if mesh_pm.cent[i][0]<0.0:
        M[i] = [magnetization_x, magnetization_y]
    else:
        M[i] = [-magnetization_x, magnetization_y]

#------------------------------------------------------------------------------
# Section 3 : Solution du problème
#------------------------------------------------------------------------------

# Descemte du supra -----------------------------------------------------------
Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # B de l'aimant

supra.set_B_source(Bm_x, Bm_y)
supra.set_B_old()

supra.translate([0.0, d-dc])

Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # B de l'aimant
supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle


supra.minimize()
#supra.minimize_energy()        # Minimisation de l'énergie

supra.plot_current() # Affichage du courant
supra.plot_field()
supra.reset_current_field()
supra.reset_temp_current_field()


moving_loop = [[0.01, 0.0],[-0.01, 0.0],[-0.01, 0.0],[0.01, 0.0],[0.01, 0.0],[-0.01, 0.0],
               [-0.01, 0.0],[0.01, 0.0],[0.01, 0.0],[-0.01, 0.0],[-0.01, 0.0],[0.01, 0.0]]

#moving_loop = [[0.00, 0.00]]

mesh_sc = mesh_superconductor(supra.X_sc, supra.Y_sc)
    
Fx1 = [0] * (len(moving_loop)+1)
Fy1 = [0] * (len(moving_loop)+1)
Fx2 = [0] * (len(moving_loop)+1)
Fy2 = [0] * (len(moving_loop)+1)
x = [0] * (len(moving_loop)+1)
act_pos = 0

J = [0] * len(supra.J)
for k in range(len(J)):
    J[k] = supra.J[k] * supra.Jc
    
mesh_sc.set_J(J)

Fx1[0], Fy1[0] = lorentz_force(mesh_sc.cent, mesh_sc.area, mesh_sc.J, [2], mesh_pm.node, mesh_pm.ele, M)
geometry = [[supra.limits[0],supra.limits[1]],[supra.limits[2],supra.limits[3]]]
Fx2[0], Fy2[0] = maxwell_force(geometry, [2,1], mesh_pm.node, mesh_pm.ele, M, mesh_sc.node, mesh_sc.ele, mesh_sc.J)

for i in range(len(moving_loop)):
    
    act_pos += moving_loop[i][0]
    
    x[i+1] = act_pos
    
    print ("Movement",i+1,"of",len(moving_loop))
    # Remontée du supra -----------------------------------------------------------
    supra.translate(moving_loop[i])
    mesh_sc.translate(moving_loop[i])
    Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, supra.X_air, supra.Y_air) # Nouveau B source
    
    supra.set_B_old()
    supra.set_B_source(Bm_x, Bm_y) # Champ source appliqué au modèle
    supra.minimize() # Minimisation de l'énergie 
    
    supra.set_B_old()
    supra.plot_current() # Affichage du courant
    supra.reset_current_field()
    supra.reset_temp_current_field()
    
    J = [0] * len(supra.J)
    for k in range(len(J)):
        J[k] = supra.J[k] * supra.Jc
    
    mesh_sc.set_J(J)
        
    geometry = [[supra.limits[0],supra.limits[1]],[supra.limits[2],supra.limits[3]]]
    Fx2[i+1], Fy2[i+1] = maxwell_force(geometry, [2,1], mesh_pm.node, mesh_pm.ele, M, mesh_sc.node, mesh_sc.ele, mesh_sc.J)
    Fx1[i+1], Fy1[i+1] = lorentz_force(mesh_sc.cent, mesh_sc.area, mesh_sc.J, [2], mesh_pm.node, mesh_pm.ele, M)
    #Fy1[i+1] *= (1.0/Fy1[0])

print (Fy1[0])

from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot

figure()
plot(x,Fy1,'k',linewidth=4)
plot(x,Fy2,'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
grid('on')

show()






