# -*- coding: latin-1 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
Created on Wed May 22 14:59:15 2013

-------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation : May 2013
Modification : May 23rd, 2013
----------------------------------------------------------------------------'''

from Samia.MagneticField.field_from_source import field_from_magnetization
from Samia.MagneticField.method_of_moments import find_magnetization
from Samia.Superconductor import superconductor, mesh_superconductor
from Samia.Mesh import mesh
from Samia.Support.plot import plot_square_2val
from Samia.Superconductor.magnetic_material_interaction import Superconductor_with_MagneticMaterial


from numpy import pi, linspace, meshgrid, zeros
from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pylab import show
from matplotlib.pyplot import plot

#------------------------------------------------------------------------------
# Section 1 : Entr�es
#------------------------------------------------------------------------------
# Param�tres de base
a = 0.02223
b = 0.02777
c = 0.0127
d = 0.025
e = 0.0254
f = 0.05

w_sc = 0.130
h_sc = 0.013

ch = 0.035
hl = 0.020

# Aimant permanent (PM --> permanent magnet)
pm1_x1 = -f/2.0      # [m] Position x de gauche
pm1_x2 = f/2.0       # [m] Position x de droite
pm2_x1 = pm1_x1 - e
pm2_x2 = pm1_x1
pm3_x1 = pm1_x2
pm3_x2 = pm1_x2 + e
pm4_x1 = pm2_x1 - d
pm4_x2 = pm2_x1
pm5_x1 = pm3_x2
pm5_x2 = pm3_x2 + d

pm1_y1 = -a-b
pm1_y2 = 0.0
pm2_y1 = -a-b
pm2_y2 = -a
pm3_y1 = -a-b
pm3_y2 = -a
pm4_y1 = -a-b
pm4_y2 = 0.0
pm5_y1 = -a-b
pm5_y2 = 0.0

m1_x1 = - c -d -e -f/2.0
m1_x2 = -d-e-f/2.0
m2_x1 = -e -f/2.0
m2_x2 = -f/2.0
m3_x1 = f/2.0
m3_x2 = e +  f/2.0
m4_x1 = d +e +f/2.0
m4_x2 = c+d+e+f/2.0

m1_y1 = -a
m1_y2 = 0.0
m2_y1 = -a
m2_y2 = 0.0
m3_y1 = -a
m3_y2 = 0.0
m4_y1 = -a
m4_y2 = 0.0

sc_x1 = -w_sc/2.0
sc_x2 = w_sc/2.0

sc_y1 = ch
sc_y2 = sc_y1 + h_sc

# Param�tres du maillage
max_area = 0.0001 # [m^2] Taille maximale d'un �l�ment 

#**********************
# Options magn�tiques *
#**********************

# Choix des mat�riaux magn�tiques
material = ['Iron'] # [-] Choix possibles : 'Iron','Linear'
lin_val = [200.0]            # [-] mu_r pour le choix 'Linear'
tol = 1                         # [%] tol�rance sur la m�thode GFUN

magnetization =  953338
Jc = 1.4 * 10**7

nx_supra = 40
ny_supra = 8

#------------------------------------------------------------------------------
# Section 2 : Cr�ation du maillage
#------------------------------------------------------------------------------
#print '�tape 1 : G�n�ration du maillage'

#*******************
# Aimant permanent *
#*******************
#print '  --> Courant'
mesh_pm = mesh(2, 'meshpy',max_area)
mesh_pm.rectangle([[pm1_x1, pm1_x2],[pm1_y1, pm1_y2]])
mesh_pm.add_geom(mesh_pm.rectangle,[[pm2_x1, pm2_x2],[pm2_y1, pm2_y2]])
mesh_pm.add_geom(mesh_pm.rectangle,[[pm3_x1, pm3_x2],[pm3_y1, pm3_y2]])
mesh_pm.add_geom(mesh_pm.rectangle,[[pm4_x1, pm4_x2],[pm4_y1, pm4_y2]])
mesh_pm.add_geom(mesh_pm.rectangle,[[pm5_x1, pm5_x2],[pm5_y1, pm5_y2]])
mesh_pm.mesh()

mesh_pm.plot()


M = [0] * mesh_pm.n_ele
for i in range(mesh_pm.n_ele):
    if mesh_pm.cent[i][0]<(-f/2.0-e) or mesh_pm.cent[i][0]>(f/2.0+e):
        M[i] = [-magnetization , 0.0]
    elif mesh_pm.cent[i][0]>(-f/2.0-e) and mesh_pm.cent[i][0]<(-f/2.0):
        M[i] = [0.0 , -magnetization]
    elif mesh_pm.cent[i][0]<(f/2.0+e) and mesh_pm.cent[i][0]>(f/2.0):
        M[i] = [0.0 , magnetization]
    else:
        M[i] = [magnetization , 0.0]


#------------------------------------------------------------------------------
# Parties Magn�tiques
#------------------------------------------------------------------------------
print ("Step 1")
mesh_mag = mesh(2,'meshpy',max_area)
mesh_mag.rectangle([[m1_x1, m1_x2],[m1_y1, m1_y2]])
mesh_mag.add_geom(mesh_mag.rectangle , [[m2_x1, m2_x2],[m2_y1, m2_y2]])
mesh_mag.add_geom(mesh_mag.rectangle , [[m3_x1, m3_x2],[m3_y1, m3_y2]])
mesh_mag.add_geom(mesh_mag.rectangle , [[m4_x1, m4_x2],[m4_y1, m4_y2]])
mesh_mag.mesh()

mesh_mag.plot()

X = [0] * mesh_mag.n_ele
Y = [0] * mesh_mag.n_ele
for i in range(mesh_mag.n_ele):
    X[i] = mesh_mag.cent[i][0]
    Y[i] = mesh_mag.cent[i][1]


geom = [[sc_x1,sc_x2],[sc_y1,sc_y1+h_sc]]
SuperConductor = superconductor(geom, nx_supra, ny_supra)
SuperConductor.set_Jc(Jc)
SuperConductor.create_matrix()

Bm_x, Bm_y = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, SuperConductor.X_air, SuperConductor.Y_air) # B de l'aimant

SuperConductor.set_B_source(Bm_x, Bm_y)
SuperConductor.set_B_old()

SuperConductor.translate([0.0, sc_y1-ch])

Bx, By = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M,X,Y)

sol = find_magnetization(mesh_mag, [Bx, By], tol, material, lin_val)
M_mag = [0] * mesh_mag.n_ele
for i in range(mesh_mag.n_ele):
    M_mag[i] = [sol.Mx[i], sol.My[i]]
    

x = linspace(-0.1,0.1,100)
y = [0.005, 0.01, 0.015, 0.02, 0.025, 0.03,0.035, 0.04]
[X, Y] = meshgrid(x, y)
Bx1, By1 = field_from_magnetization(mesh_pm.node, mesh_pm.ele, M, X, Y)
Bx2, By2 = field_from_magnetization(mesh_mag.node, mesh_mag.ele, M_mag, X, Y)

Byt = zeros((len(Y),len(Y[0])))
Bxt = zeros((len(Y),len(Y[0])))

for i in range(len(Y)):
    By1_n = By1[i]
    By2_n = By2[i]
    Bx1_n = Bx1[i]
    Bx2_n = Bx2[i]
    for j in range(len(By1_n)):
        Byt[i][j] = By1_n[j] + By2_n[j]
        Bxt[i][j] = Bx1_n[j] + Bx2_n[j]


figure()
p0, = plot(x,Byt[0])
p1, = plot(x,Byt[1])
p2, = plot(x,Byt[2])
p3, = plot(x,Byt[3])
p4, = plot(x,Byt[4])
p5, = plot(x,Byt[5])
p6, = plot(x,Byt[6])
p7, = plot(x,Byt[7])
xlabel('Position Y')
ylabel('B [T]')
legend([p0, p1, p2, p3, p4, p5, p6, p7], ["5 mm","10 mm","15 mm","20 mm","25 mm","30 mm","35 mm","40 mm"])
grid('on')

figure()
p0, = plot(x,Bxt[0])
p1, = plot(x,Bxt[1])
p2, = plot(x,Bxt[2])
p3, = plot(x,Bxt[3])
p4, = plot(x,Bxt[4])
p5, = plot(x,Bxt[5])
p6, = plot(x,Bxt[6])
p7, = plot(x,Bxt[7])
xlabel('Position Y')
ylabel('B [T]')
legend([p0, p1, p2, p3, p4, p5, p6, p7], ["5 mm","10 mm","15 mm","20 mm","25 mm","30 mm","35 mm","40 mm"])
grid('on')

#------------------------------------------------------------------------------
# Solution
#------------------------------------------------------------------------------
print ("Step 2")

SuperConductor, mesh_sc, sol = Superconductor_with_MagneticMaterial(SuperConductor, mesh_mag, [material, lin_val,tol], mesh_pm, M)

mesh_pm.plot()
SuperConductor.plot_current()


mvt = -ch + hl

moving_loop = [
[0.0, mvt],  [0.002, 0.0],[-0.002, 0.0],[-0.002, 0.0],
[0.002, 0.0],[0.002, 0.0],[-0.002, 0.0],[-0.002, 0.0],
[0.002, 0.0],[0.002, 0.0],[-0.002, 0.0],[-0.002, 0.0]
]

from Samia.Support.Force import lorentz_force


Fx = [0] * (len(moving_loop))
Fy = [0] * (len(moving_loop))
x = [0] * (len(moving_loop))
act_pos = 0.0

J = [0] * len(SuperConductor.J)
for k in range(len(J)):
    J[k] = SuperConductor.J[k] * SuperConductor.Jc

mesh_sc = mesh_superconductor(SuperConductor.X_sc, SuperConductor.Y_sc)
mesh_sc.set_J(J)
x[0] = act_pos

M_mag = [0] * mesh_mag.n_ele
for i in range(mesh_mag.n_ele):
    M_mag[i] = [sol.Mx[i], sol.My[i]]

         
print ("Step 3")

for i in range(len(moving_loop)):
    
    act_pos += moving_loop[i][0]
    x[i] = act_pos
    
    print ("Movement",i+1,"of",len(moving_loop))
    # Remont�e du supra -----------------------------------------------------------
    SuperConductor.translate(moving_loop[i])
    mesh_sc.translate(moving_loop[i])
    
    SuperConductor, mesh_sc, sol = Superconductor_with_MagneticMaterial(SuperConductor, mesh_mag, [material, lin_val,tol], mesh_pm, M)
    SuperConductor.plot_current()
    
    J = [0] * len(SuperConductor.J)
    for k in range(len(J)):
        J[k] = SuperConductor.J[k] * SuperConductor.Jc
        
    mesh_sc.set_J(J)

    M_mag = [0] * mesh_mag.n_ele
    for k in range(mesh_mag.n_ele):
        M_mag[k] = [sol.Mx[k], sol.My[k]]
    
    geometry = [[SuperConductor.limits[0],SuperConductor.limits[1]],[SuperConductor.limits[2],SuperConductor.limits[3]]]
    Fx[i], Fy[i] = lorentz_force(mesh_sc.cent, mesh_sc.area, mesh_sc.J, [2,2], mesh_pm.node, mesh_pm.ele, M, mesh_mag.node, mesh_mag.ele, M_mag)
    #Fx[i+1], Fy[i+1] = lorentz_force(mesh_sc.cent, mesh_sc.area, mesh_sc.J, [2], mesh_pm.node, mesh_pm.ele, M)
    #Fx[i+1], Fy[i+1] = lorentz_force(mesh_sc.cent, mesh_sc.area, mesh_sc.J, [2], mesh_mag.node, mesh_mag.ele, M_mag)


print ("Hauteur de refroidissement :",ch)
print ("Hauteur de l�vitation :",hl)
figure()
p1, = plot(x,Fy,'k',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
grid('on')

#axis('tight')

show()