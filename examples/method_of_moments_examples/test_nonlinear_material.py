# -*- coding: utf-8 -*-

"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""

from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization, field_from_current
from Samia.MagneticField.method_of_moments import find_magnetization

from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi

material = ['Iron']
lin_val = [0.5]
tol = 1 # en %
r2 = 1.6
r1 = 1.3
rv = 1.65
r0 = 1.0
MY0 = -10.0**7
npts = 50
mu0 = 4 * pi * 10**-7


x = list(linspace(0.5,1.5,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]


max_area = 0.05
mesh1 = mesh(2,'meshpy',max_area)
mesh1.circle([r2 , [0,0]])
mesh1.substract(mesh1.circle, [r1, [0,0]])
mesh1.mesh()
mesh1.plot()

A = 1.0*10**6
p = 2

from examples.method_of_moments_examples.H_source import B_sin

X = [0] * mesh1.n_ele
Y = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    X[i] = mesh1.cent[i][0]
    Y[i] = mesh1.cent[i][1]
Bx, By = B_sin(X, Y , r0, A, p)

#print Bx
#print By

#BY0 = 1.0
#Bx = [0] * mesh1.n_ele
#By = [-BY0] * mesh1.n_ele


sol = find_magnetization(mesh1, [Bx, By], tol, material, lin_val)


sol.plot(mesh1, 'B')
sol.plot(mesh1, 'CHI')
#sol.plot(mesh1, 'Hc')

M = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    M[i] = [sol.Mx[i], sol.My[i]]

#print M

ang = list(linspace(0, pi, npts))

from math import cos, sin
x = [0.0] * len(ang)
y = [0.0] * len(ang)
z = [0.0] * len(ang)
for i in range(len(ang)):
    x[i] = rv*cos(ang[i])
    y[i] = rv*sin(ang[i])

    
Y2 = [y]
X2 = [x]

Bxc, Byc = B_sin(X2[0], Y2[0] , r0, A, p)
BX_2Dm, BY_2Dm = field_from_magnetization(mesh1.node, mesh1.ele, M, X2, Y2)

Bt2D = [0] * len(Bxc)
for i in range(len(Bxc)):
    Bt2D[i] = (BX_2Dm[0][i])*cos(ang[i]) + (BY_2Dm[0][i])*sin(ang[i])


#
# 3D Part
#
h = 0.5
n_ele = 100
mesh3 = mesh(3, 'regular')
geometry = [h,r1,r2,[0,0,0]]
#mesh3.empty_cylinder(n_ele, geometry)
mesh3.extrude_2D_mesh(3, -h/2.0, h/2.0, mesh1)
#print mesh3.n_ele

X = [0] * mesh3.n_ele
Y = [0] * mesh3.n_ele
Z = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    X[i] = mesh3.cent[i][0]
    Y[i] = mesh3.cent[i][1]
    Z[i] = mesh3.cent[i][2]
    
Bx, By = B_sin(X, Y , r0, A, p)
Bz  = [0] * mesh3.n_ele

sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val)


sol.plot(mesh3, 'B', 0.0, 'Z')

sol.plot(mesh3, 'CHI', 0.0, 'Z')

M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

#print M

z = [0.0] * len(x)
Z2 = [z]

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)

Bt3D = [0] * len(Bxc)
for i in range(len(Bxc)):
    Bt3D[i] = (BX_3Dm[0][i])*cos(ang[i]) + (BY_3Dm[0][i])*sin(ang[i])

h = 100.0
n_ele = 100
mesh3 = mesh(1, 'regular')
geometry = [h,r1,r2,[0,0,0]]
#mesh3.empty_cylinder(n_ele, geometry)
mesh3.extrude_2D_mesh(3, -h/2.0, h/2.0, mesh1)
#print mesh3.n_ele

X = [0] * mesh3.n_ele
Y = [0] * mesh3.n_ele
Z = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    X[i] = mesh3.cent[i][0]
    Y[i] = mesh3.cent[i][1]
    Z[i] = mesh3.cent[i][2]
    
Bx, By = B_sin(X, Y , r0, A, p)
Bz  = [0] * mesh3.n_ele

sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val)


sol.plot(mesh3, 'B', 0.0, 'Z')

sol.plot(mesh3, 'CHI', 0.0, 'Z')

M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

#print M

z = [0.0] * len(x)
Z2 = [z]

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)

Bt3D2 = [0] * len(Bxc)
for i in range(len(Bxc)):
    Bt3D2[i] = (BX_3Dm[0][i])*cos(ang[i]) + (BY_3Dm[0][i])*sin(ang[i])
    
#print x
figure()
p1, = plot(ang,Bt2D,'k',linewidth=4)
p4, = plot(ang,Bt3D,'k:',linewidth=4)
p2, = plot(ang,Bt3D2, 'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4,p2],['MoM 2D','MoM 3D (l=0.5m)','MoM 3D (l=100m)'])
grid('on')

Zsweep = [list(linspace(0, 1.5*h/2.0, npts))]
Xsweep = [[rv*cos(pi/4.0)] * len(Zsweep[0])]
Ysweep = [[rv*sin(pi/4.0)] * len(Zsweep[0])]

BX_3Dmz, BY_3Dmz, BZmz = field_from_magnetization(mesh3.node, mesh3.ele, M, Xsweep, Ysweep, Zsweep)

from math import sqrt
Bn = [0] * len(BX_3Dmz[0])
for i in range(len(Bn)):
    Bn[i] = sqrt(BX_3Dmz[0][i]**2 + BY_3Dmz[0][i]**2 + BZmz[0][i]**2)
    
figure()
plot(Zsweep[0],Bn,'k',linewidth=4)
grid('on')

show()



