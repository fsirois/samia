# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""

from Samia.Mesh import mesh
from Samia.Support.plot import plot_square_2val
from Samia.MagneticField.field_from_source import field_from_magnetization, field_from_current
from Samia.MagneticField.method_of_moments import find_magnetization, gen_complete_solution

from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi

material = ['Linear']
lin_val = [10.0]
tol = 1 # en %
r2 = 1.6
r1 = 1.3
rv = 1.1
r0 = 1.0
MY0 = -10.0**7
npts = 50
mu0 = 4 * pi * 10**-7

from examples.method_of_moments_examples.H_source import B_sin
A = 1.5*10**6
n_sym = 2
p = n_sym

ang = list(linspace(0, pi, npts))

from math import cos, sin
x = [0.0] * len(ang)
y = [0.0] * len(ang)
z = [0.0] * len(ang)
for i in range(len(ang)):
    x[i] = rv*cos(ang[i])
    y[i] = rv*sin(ang[i])

Y2 = [y]
X2 = [x]
Z2 = [z]




n_ele = 100
mesh1 = mesh(2,'regular')
mesh1.curved_rectangle(r1, r2, 2.0*pi, n_ele)
#mesh1.plot()

X = [0] * mesh1.n_ele
Y = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    X[i] = mesh1.cent[i][0]
    Y[i] = mesh1.cent[i][1]
Bx, By = B_sin(X, Y , r0, A, p)

sol = find_magnetization(mesh1, [Bx, By], tol, material, lin_val)

sol.plot(mesh1, 'B')

M = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    M[i] = [sol.Mx[i], sol.My[i]]

BX_2Dm, BY_2Dm = field_from_magnetization(mesh1.node, mesh1.ele, M, X2, Y2)

Bt2D = [0] * len(x)
for i in range(len(x)):
    Bt2D[i] = (BX_2Dm[0][i])*cos(ang[i]) + (BY_2Dm[0][i])*sin(ang[i])





n_ele = 20
mesh1 = mesh(2,'regular')
mesh1.curved_rectangle(r1, r2, pi/n_sym, n_ele)
mesh1.rotate([pi/4.0, [0.0, 0.0]])
#mesh1.plot()

X = [0] * mesh1.n_ele
Y = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    X[i] = mesh1.cent[i][0]
    Y[i] = mesh1.cent[i][1]
Bx, By = B_sin(X, Y , r0, A, p)

sol = find_magnetization(mesh1, [Bx, By], tol, material, lin_val, n_sym_rot=n_sym)

sol.plot(mesh1, 'B')

#from symmetry import gen_complete_solution
mesh2, sol = gen_complete_solution(mesh1, sol, n_sym)

sol.plot(mesh2, 'B')

M = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    M[i] = [sol.Mx[i], sol.My[i]]

BX_2Dm, BY_2Dm = field_from_magnetization(mesh1.node, mesh1.ele, M, X2, Y2)

Bt2D2 = [0] * len(x)
for i in range(len(x)):
    Bt2D2[i] = (BX_2Dm[0][i])*cos(ang[i]) + (BY_2Dm[0][i])*sin(ang[i])

figure()
p1, = plot(ang,Bt2D,'k',linewidth=4)
p2, = plot(ang,Bt2D2, 'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p2],['MoM 2D','MoM 2D rad_sym'])
grid('on')



#
# 3D Part
#
n_ele = 100
mesh1 = mesh(2,'regular')
mesh1.curved_rectangle(r1, r2, 2.0*pi, n_ele)
#mesh1.plot()
h = 6.0
plot_height = h/2.0
mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(4, -h/2.0, h/2.0, mesh1)
#print mesh3.n_ele

X = [0] * mesh3.n_ele
Y = [0] * mesh3.n_ele
Z = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    X[i] = mesh3.cent[i][0]
    Y[i] = mesh3.cent[i][1]
    Z[i] = mesh3.cent[i][2]
    
Bx, By = B_sin(X, Y , r0, A, p)
Bz  = [0] * mesh3.n_ele

#print(Bx,By)


sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val)

sol.plot(mesh3, 'B', plot_height, 'Z')
#print (sol.Mx)

M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)

#print (BX_3Dm)

Bt3D = [0] * len(x)
for i in range(len(x)):
    Bt3D[i] = (BX_3Dm[0][i])*cos(ang[i]) + (BY_3Dm[0][i])*sin(ang[i])



n_ele = 25
mesh2 = mesh(2,'regular')
mesh2.curved_rectangle(r1, r2, pi/n_sym, n_ele)
mesh2.rotate([pi/4, [0.0, 0.0]])
mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(4, -h/2.0, h/2.0, mesh2)
#print mesh3.n_ele

X = [0] * mesh3.n_ele
Y = [0] * mesh3.n_ele
Z = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    X[i] = mesh3.cent[i][0]
    Y[i] = mesh3.cent[i][1]
    Z[i] = mesh3.cent[i][2]
    
Bx, By = B_sin(X, Y , r0, A, p)
Bz  = [0] * mesh3.n_ele

sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val, n_sym_rot=n_sym)


mesh3, sol =  gen_complete_solution(mesh3, sol, n_sym)
 
 
sol.plot(mesh3, 'B', plot_height, 'Z')

M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)

Bt3D2 = [0] * len(x)
for i in range(len(x)):
    Bt3D2[i] = (BX_3Dm[0][i])*cos(ang[i]) + (BY_3Dm[0][i])*sin(ang[i])





mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(2, 0, h/2.0, mesh2)
#print mesh3.n_ele

X = [0] * mesh3.n_ele
Y = [0] * mesh3.n_ele
Z = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    X[i] = mesh3.cent[i][0]
    Y[i] = mesh3.cent[i][1]
    Z[i] = mesh3.cent[i][2]
    
Bx, By = B_sin(X, Y , r0, A, p)
Bz  = [0] * mesh3.n_ele

sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val, n_sym_rot=n_sym, sym_z=True)
sol.plot(mesh3, 'B', plot_height, 'Z')
mesh3, sol =  gen_complete_solution(mesh3, sol, n_sym_rot=n_sym, sym_z = True)
sol.plot(mesh3, 'B', plot_height, 'Z')

M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)

Bt3D3 = [0] * len(x)
for i in range(len(x)):
    Bt3D3[i] = (BX_3Dm[0][i])*cos(ang[i]) + (BY_3Dm[0][i])*sin(ang[i])

    


figure()
p1, = plot(ang,Bt3D,'b',linewidth=3)
p2, = plot(ang,Bt3D2, 'r--',linewidth=3)
p4, = plot(ang,Bt3D3,'k--',linewidth=3)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p2,p4],['MoM 3D','MoM 3D rad_sym','MoM 3D z_sym + rad_sym'])
grid('on')


show()



