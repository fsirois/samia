# -*- coding: utf-8 -*-
'''-----------------------------------------------------------------------------
--------------------------------------------------------------------------------
Different H-Field Sources

Call example : [Hx, Hy] = H_sin(pos, geom , const)
--------------------------------------------------------------------------------
INPUTS
    pos : position of the point where the source field is wanted
          it is a list of length=2, pos[0]=x position, pos[1] = y position
    geom : provides instruction on the geometry of the source field
    const : provides physical information on the strenght of the field

OUTPUTS
    Hx : X-component of H-Field from source in the air
    Hy : Y-component of H-Field from source in the air
--------------------------------------------------------------------------------
Written by : Kevin Ratelle
Creation date : July 26, 2011
Last Modification : July 26, 2011
Created at : Advanded Magnet Lab
-----------------------------------------------------------------------------'''

#-------------------------------------------------------------------------------
# From a wire in the z-axis
#-------------------------------------------------------------------------------
def H_wire(pos , geom , const):
    ''' GEOM must include geom.wire_pos
            geom.wire_pos[0] = x position of the wire
            geom.wire_pos[1] = y position of the wire
        
        CONST must include const.I where I is the current in the wire
    '''
    
    from math import sqrt, pi, atan2, sin, cos
    
    # creating norm of the field and angle of it
    d_x = pos[0] - geom.wire_pos[0]
    d_y = pos[1] - geom.wire_pos[1]
    r = sqrt(d_x**2 + d_y**2)
    Hc_norm = const.I / (2*r*pi)
    theta = atan2(d_y , d_x)
        
    # calculating Hcx and Hcy
    Hc_x = Hc_norm * -sin(theta)
    Hc_y = Hc_norm * cos(theta)
    
    return [Hc_x, Hc_y]

#-------------------------------------------------------------------------------
# Sinusoidal
#-------------------------------------------------------------------------------
def B_sin(X, Y , rho0, A, p):
    ''' GEOM must include geom.sinus where :
            geom.sinus[0] = radius of the source
            geom.sinus[1] = center of the source in the form [x , y]
        CONST must include geom.A and geom.p
    '''
    
    from math import cos, sin, sqrt, atan2, pi
    mu0 = 4 * pi * 10**-7
    Bc_x = [0] * len(X)
    Bc_y = [0] * len(Y)
    dephasage = 0 #pi/(4*p)
    
    for i in range(len(X)):
        # position to evaluate the field
        x = X[i]
        y = Y[i]
        
        # transforms x and y into rho and theta
        rho = sqrt(x**2+y**2)
        theta = atan2(y, x)
        
        #actual evaluation of the H-Field
        if rho<=rho0:
            hr = 0.5*A * (rho/rho0)**(p-1) * cos(p*theta) * mu0
            ht = -0.5*A * (rho/rho0)**(p-1) * sin(p*theta) * mu0
        else:
            hr = 0.5*A * (rho0/rho)**(p+1) * cos(p*theta) * mu0
            ht = 0.5*A * (rho0/rho)**(p+1) * sin(p*theta) * mu0
        
        # Conversion of field in X and Y components    
        Bc_x[i] = hr * cos(theta + dephasage) + ht * cos(theta + dephasage + pi/2)
        Bc_y[i] = hr * sin(theta + dephasage) + ht * sin(theta + dephasage + pi/2)
            
    return Bc_x , Bc_y
