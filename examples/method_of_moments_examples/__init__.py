
#input("Press Enter for linear material example")
#import examples.method_of_moments_examples.test_linear_material

#input("Press Enter for iron material example")
#import examples.method_of_moments_examples.test_nonlinear_material

#input("Press Enter for example with more than one material")
#import examples.method_of_moments_examples.test_multiple_materials

input("Press Enter for example with symmetries")
import examples.method_of_moments_examples.test_symmetries

#input("Press Enter for comparison between electromagnet and permanent magnet")
#import examples.method_of_moments_examples.test_magnet_vs_electromagnet
