# -*- coding: latin-1 -*-

"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""


from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization, field_from_current
from Samia.MagneticField.method_of_moments import find_magnetization
from Samia.Support.plot import plot_square_2val


from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi


#------------------------------------------------------------------------------
# Gestion de la géométrie
#------------------------------------------------------------------------------

material = ['Iron']
lin_val = None
tol = 1 # en %



magnet = 1.0 * 10**6


buff = 0.002

max_area = 0.00005
n_height = 2
n_height_iron = 6
h = 0.150
radius_in = 0.025
radius_out = 0.045
rad_pm = 0.0225
h_pm = 0.030
current = 5.0 * 10**6 # A/m^2

mesh_iron = mesh(2,'meshpy',max_area)
mesh_iron.circle([radius_in, [0.0,0.0]])
mesh_iron.mesh()
mesh_iron.plot()

mesh_cond = mesh(2, 'meshpy', 2*max_area)
mesh_cond.circle([radius_out, [0.0,0.0]])
mesh_cond.substract(mesh_cond.circle, [radius_in, [0.00, 0.00]])
mesh_cond.mesh()
mesh_cond.plot()

mesh_pm = mesh(2,'meshpy',max_area)
mesh_pm.circle([rad_pm, [0.0,0.0]])
mesh_pm.mesh()
mesh_pm.plot()

show()



mesh3D_iron = mesh(3, 'regular')
mesh3D_iron.extrude_2D_mesh(n_height_iron, -h, 0.0, mesh_iron)

mesh3D_cond = mesh(3, 'regular')
mesh3D_cond.extrude_2D_mesh(n_height_iron, -h, 0.0, mesh_cond)

mesh3D_pm = mesh(3, 'regular')
mesh3D_pm.extrude_2D_mesh(n_height, -h_pm, 0.0, mesh_pm)

from math import sin, cos, atan2

J = [0] * mesh3D_cond.n_ele
for i in range(mesh3D_cond.n_ele):
    J[i] = [0.0, 0.0, 0.0]
    
    angle = atan2(mesh3D_cond.cent[i][1], mesh3D_cond.cent[i][0])
    
    J[i][0] = -sin(angle)*current
    J[i][1] = cos(angle)*current
    
M = [0] * mesh3D_pm.n_ele
for i in range(mesh3D_pm.n_ele):
    M[i] = [0.0, 0.0, 0.0]
    
    M[i][2] = magnet


#------------------------------------------------------------------------------
# Définition des domaines
#------------------------------------------------------------------------------
n_pts_x = 30
n_pts_y = 30
xmin = -0.05
xmax = 0.05
ymin = -0.05
ymax = 0.05

xv = linspace(xmin, xmax, n_pts_x)
yv = linspace(ymin, ymax, n_pts_y)
[X1,Y1] = meshgrid(xv,yv)

zpos = buff
Z1 = zeros((len(X1),len(X1[0]))) + zpos


xmin = -0.05
xmax = 0.05
ymin = buff
ymax = 0.05

xv = linspace(xmin, xmax, n_pts_x)
yv = linspace(ymin, ymax, n_pts_y)
[X2,Y2] = meshgrid(xv,yv)

zpos = 0.0
Z2 = zeros((len(X1),len(X1[0]))) + zpos


#------------------------------------------------------------------------------
# Champ des rails sans acier ni aimant dessous
#------------------------------------------------------------------------------
BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3D_pm.node, mesh3D_pm.ele, M, X1, Y1, Z1)
plot_square_2val(X1, Y1, BX_3Dm, BY_3Dm, 'B [T], vue de dessus, aimant permanent')

BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3D_pm.node, mesh3D_pm.ele, M, X2, Z2, Y2)
plot_square_2val(X2, Y2, BX_3Dm, BZm, 'B [T] vue de cote, aimant permanent')

BX, BY, BZ = field_from_magnetization(mesh3D_pm.node, mesh3D_pm.ele, M, [0.0], [0.0], [buff])

from math import sqrt
field = sqrt(BX[0]**2 + BY[0]**2 + BZ[0]**2)

print ("Field is : ",field,"Tesla")
#show()


#------------------------------------------------------------------------------
# With IRON
#------------------------------------------------------------------------------
X = [0] * mesh3D_iron.n_ele
Y = [0] * mesh3D_iron.n_ele
Z = [0] * mesh3D_iron.n_ele
for i in range(mesh3D_iron.n_ele):
    X[i] = mesh3D_iron.cent[i][0]
    Y[i] = mesh3D_iron.cent[i][1]
    Z[i] = mesh3D_iron.cent[i][2]
    

Bx, By, Bz = field_from_current(mesh3D_cond.node, mesh3D_cond.ele, J, X, Y, Z)
sol = find_magnetization(mesh3D_iron, [Bx, By, Bz], tol, material, lin_val)


M = [0] * mesh3D_iron.n_ele
for i in range(mesh3D_iron.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]



BX_3Dpm, BY_3Dpm, BZpm = field_from_current(mesh3D_cond.node, mesh3D_cond.ele, J, X1, Y1, Z1)
BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3D_iron.node, mesh3D_iron.ele, M, X1, Y1, Z1)

BX_3Dt = [0] * len(BX_3Dpm)
BY_3Dt = [0] * len(BX_3Dpm)
BZt = [0] * len(BX_3Dpm)
for i in range(len(BX_3Dpm)):
    BX_3Dt[i] = [0] * len(BX_3Dpm[0])
    BY_3Dt[i] = [0] * len(BX_3Dpm[0])
    BZt[i] = [0] * len(BX_3Dpm[0])
    for j in range(len(BX_3Dpm[0])):
        BY_3Dt[i][j] = BY_3Dm[i][j] + BY_3Dpm[i][j]
        BX_3Dt[i][j] = BX_3Dm[i][j] + BX_3Dpm[i][j]
plot_square_2val(X1, Y1, BX_3Dt, BY_3Dt, 'B [T] vue de dessus, electroaimant')
    

BX_3Dpm, BY_3Dpm, BZpm = field_from_current(mesh3D_cond.node, mesh3D_cond.ele, J, X2, Z2, Y2)
BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3D_iron.node, mesh3D_iron.ele, M, X2, Z2, Y2)

for i in range(len(BX_3Dpm)):
    for j in range(len(BX_3Dpm[0])):
        BX_3Dt[i][j] = BX_3Dm[i][j] + BX_3Dpm[i][j]
        BZt[i][j] = BZpm[i][j] + BZm[i][j]
plot_square_2val(X2, Y2, BX_3Dt, BZt, 'B [T] vue de cote, electroaimant')


BXm, BYm, BZm = field_from_magnetization(mesh3D_pm.node, mesh3D_pm.ele, M, [0.0], [0.0], [buff])
BXc, BYc, BZc = field_from_current(mesh3D_cond.node, mesh3D_cond.ele, J, [0.0], [0.0], [buff])

from math import sqrt
field = sqrt((BXm[0]+BXc[0])**2 + (BYm[0]+BYc[0])**2 + (BZm[0]+BZc[0])**2)

print ("Field is : ",field,"Tesla")
show()

aire = (radius_out-radius_in)*h
A = current*aire
print ("Ampere tours :",A)

