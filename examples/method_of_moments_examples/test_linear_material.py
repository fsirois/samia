# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 17:07:34 2013

@author: Kevin
"""

from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization, field_from_current
from Samia.MagneticField.method_of_moments import find_magnetization

from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi

material = ['Linear']
lin_val = [10.0]
tol = 1 # en %
r = 1.0
MY0 = -10.0**7
npts=50
mu0 = 4 * pi * 10**-7

x1 = list(linspace(0.5, 1.0,npts))
x = list(linspace(0.5,1.5,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]


BY0 = 50.0

max_vol = 0.0136
mesh3 = mesh(3, 'meshpy',max_vol)
mesh3.add_geom(mesh3.sphere, [r,[0,0,0]])
mesh3.mesh()
#print mesh3.n_ele

 # Creating magnetization ARRAY
Bx = [0] * mesh3.n_ele
By = [BY0] * mesh3.n_ele
Bz = [0] * mesh3.n_ele


sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val)
#sol.plot(mesh3, 0.0, 'Z','B')
#print sol.Mx


M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]
    
#mesh3.node = mesh3.node.tolist()
#mesh3.ele = mesh3.ele.tolist()
BX_3D, BY_3D, BZ = field_from_magnetization(mesh3.node, mesh3.ele, M, X, Y, Z)


Bth = [0] * len(x)
for i in range(len(x)):
    Bth[i] = 3.0 * lin_val[0] /(2.0 + lin_val[0]) * BY0
    BY_3D[0][i] += BY0


figure()
p1, = plot(x,BY_3D[0],'k',linewidth=4)
p4, = plot(x1,Bth,'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['Method of Moments','Analytic Equation'])
grid('on')

show()

J0 = 10.0**7

max_area = 0.06
center = [0, 0] # center of the circle

mesh1 = mesh(2,'meshpy',max_area)
mesh1.circle([r , center])
mesh1.mesh()

#print mesh1.n_ele
BY0 = 2.0

Bx = [0] * mesh1.n_ele
By = [-BY0] * mesh1.n_ele


lin_val = [100.0]

sol = find_magnetization(mesh1, [Bx, By], tol, material, lin_val)

sol.plot(mesh1, 'B')

M = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    M[i] = [sol.Mx[i], sol.My[i]]

#print M

x = list(linspace(1.1,2.5,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y2 = [y]
X2 = [x]

BX_2Dm, BY_2Dm = field_from_magnetization(mesh1.node, mesh1.ele, M, X2, Y2)



#
# 3D Part
#
h = 100.0
mesh3 = mesh(3, 'regular')
mesh3.extrude_2D_mesh(4, -h/2.0, h/2.0, mesh1)
#print mesh3.n_ele

X = [[0] * mesh3.n_ele]
Y = [[0] * mesh3.n_ele]
Z = [[0] * mesh3.n_ele]
for i in range(mesh3.n_ele):
    X[0][i] = mesh3.cent[i][0]
    Y[0][i] = mesh3.cent[i][1]
    Z[0][i] = mesh3.cent[i][2]
    

Bx = [0] * mesh3.n_ele
By = [-BY0] * mesh3.n_ele
Bz = [0] * mesh3.n_ele


sol = find_magnetization(mesh3, [Bx, By, Bz], tol, material, lin_val)

sol.plot(mesh3, 'B', 2.0, 'Z')
M = [0] * mesh3.n_ele
for i in range(mesh3.n_ele):
    M[i] = [sol.Mx[i], sol.My[i], sol.Mz[i]]

#print M

x = list(linspace(1.1,2.5,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y2 = [y]
X2 = [x]
Z2 = [z]


BX_3Dm, BY_3Dm, BZm = field_from_magnetization(mesh3.node, mesh3.ele, M, X2, Y2, Z2)


figure()
p1, = plot(x,BY_2Dm[0],'k',linewidth=4)
p4, = plot(x,BY_3Dm[0],'r--',linewidth=4)
xlabel('Position [m]')
ylabel('Magnetic Flux Density [T]')
legend([p1,p4],['MoM 2D','MoM 3D'])
grid('on')


show()



