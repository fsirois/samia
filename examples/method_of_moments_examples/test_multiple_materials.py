# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 15:33:49 2013

@author: ratelle
"""

from Samia.Mesh import mesh
from Samia.MagneticField.field_from_source import field_from_magnetization, field_from_current
from Samia.MagneticField.method_of_moments import find_magnetization

from pylab import show, figure, grid, legend, xlabel, ylabel
from matplotlib.pyplot import plot
from numpy import meshgrid, zeros, linspace, pi


material = ['Iron', 'Linear']
lin_val = [0.5]
tol = 1 # en %
r2 = 1.6
r1 = 1.3
rv = 1.8
r0 = 1.0
rin = 0.8
MY0 = -10.0**7
npts = 50
mu0 = 4 * pi * 10**-7


x = list(linspace(0.5,1.5,npts))
y = [0.0] * len(x)
z = [0.0] * len(x)

Y = [y]
X = [x]
Z = [z]


max_area = 0.1
mesh1 = mesh(2,'meshpy',max_area)
mesh1.circle([r2 , [0,0]])
mesh1.substract(mesh1.circle, [r1, [0,0]])
mesh1.mesh()
mesh1.plot()

mesh1b = mesh(2,'meshpy',max_area)
mesh1b.circle([rin, [0,0]])
mesh1b.mesh()
mesh1.add_mesh(mesh1b)
A = 1.5*10**6
p = 2

from examples.method_of_moments_examples.H_source import B_sin

X = [0] * mesh1.n_ele
Y = [0] * mesh1.n_ele
for i in range(mesh1.n_ele):
    X[i] = mesh1.cent[i][0]
    Y[i] = mesh1.cent[i][1]
Bx, By = B_sin(X, Y , r0, A, p)

#print Bx
#print By

#BY0 = 1.0
#Bx = [0] * mesh1.n_ele
#By = [-BY0] * mesh1.n_ele


sol = find_magnetization(mesh1, [Bx, By], tol, material, lin_val)


sol.plot(mesh1, 'B')
sol.plot(mesh1, 'CHI')
sol.plot(mesh1, 'Hc')

show()