# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MAGCAL TUTORIAL - EXAMPLE 3

This tutorial shows how to use the 'mesh' method (mc.mesh) in order to create
2D meshes using "meshPy" mesh generator. When in 2D, the "meshPy" mesh
generator is a Python wrap over the "triangle" mesh generator.

The 'mesh' method wraps the "meshPy" mesh generator with a Python user
interface.

EXAMPLES INCLUDED :
    - 1.1 --> Creating the mesh of a circle using MeshPy
    - 1.2 --> Creating a more complex 2D mesh using MeshPy


For more information on MeshPy, see :
    http://mathema.tician.de/software/meshpy/
    
For more information on Triangle (contained in MeshPy), see :
    http://www.cs.cmu.edu/~quake/triangle.html
    
-------------------------------------------------------------------------------
WARNING : The mesh generator included with MagCal is rather basic and is
          only intended to show how to use MagCal, not to generate complex
          geometries and mesh them.
-------------------------------------------------------------------------------
Written by   : Kevin Ratelle
Created at   : Montreal
Created on   : January 14, 2014
Modification : January 14, 2014
-------------------------------------------------------------------------------
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh

#------------------------------------------------------------------------------
# Example 3.1 - Creating the mesh of a cylinder
#------------------------------------------------------------------------------
radius = 0.5       # [m]
height = 1.0       # [m]
center = [0.0, 0.0, 0.0] # [m]

dimension = 3  # flag on the dimension to use [2 for 2D and 3 for 3D]
max_vol  = 0.002 # maximum volume of an element in the mesh[m^2]

mesh_generator = 'meshpy' # choice of the mesh generator

new_mesh = mesh(dimension, mesh_generator, max_vol) # creates 3D mesh object
new_mesh.add_geom(new_mesh.cylinder, [radius, height, center])            # adds a circle to the geometry

new_mesh.mesh() # mesh is generated
new_mesh.plot(0.0, 'Z') # mesh is plotted
new_mesh.plot(0.0, 'X')
new_mesh.plot(0.2, 'Y')

from matplotlib.pylab import show
show()


#------------------------------------------------------------------------------
# Example 3.2 - Create a more complex geometry
#------------------------------------------------------------------------------
new_mesh = mesh(3, 'meshpy', 0.001) # new 2D mesh object

new_mesh.add_geom(new_mesh.cyl_arc, [2.0, 2.0, [0,0,0], 1.2])
new_mesh.substract_interior(new_mesh.cylinder, [0.4, 1.0, [1.0,0.7,0]])

new_mesh.mesh()     # creates the mesh
new_mesh.plot(0.0, 'z')     # plots the mesh
new_mesh.plot(0.75, 'z')
new_mesh.plot(1.0, 'z')
new_mesh.plot(0.75, 'x')
show()

