
input("Press Enter to run 2D Regular (and Stand-Alone) Examples")
import examples.mesh_examples.test_mesh_2D_reg

input("Press Enter to run 3D Regular (and Stand-Alone) Examples")
import examples.mesh_examples.test_mesh_3D_reg

input("Press Enter to run 2D Python (MeshPy.Triangle) Examples")
import examples.mesh_examples.test_mesh_2D_py

input("Press Enter to run 3D Python (MeshPy.Tet) Examples")
import examples.mesh_examples.test_mesh_3D_py