# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MAGCAL TUTORIAL - EXAMPLE 3

This tutorial shows how to use the 'mesh' method (mc.mesh) in order to create
2D meshes using "meshPy" mesh generator. When in 2D, the "meshPy" mesh
generator is a Python wrap over the "triangle" mesh generator.

The 'mesh' method wraps the "meshPy" mesh generator with a Python user
interface.

EXAMPLES INCLUDED :
    - 1.1 --> Creating the mesh of a circle using MeshPy
    - 1.2 --> Creating a more complex 2D mesh using MeshPy


For more information on MeshPy, see :
    http://mathema.tician.de/software/meshpy/
    
For more information on Triangle (contained in MeshPy), see :
    http://www.cs.cmu.edu/~quake/triangle.html
    
-------------------------------------------------------------------------------
WARNING : The mesh generator included with MagCal is rather basic and is
          only intended to show how to use MagCal, not to generate complex
          geometries and mesh them.
-------------------------------------------------------------------------------
Written by   : Kevin Ratelle
Created at   : Montreal
Created on   : January 14, 2014
Modification : January 14, 2014
-------------------------------------------------------------------------------
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh

#------------------------------------------------------------------------------
# Example 3.1 - Creating the mesh of a cylinder
#------------------------------------------------------------------------------
radius = 0.5       # [m]
height = 1.0       # [m]
center = [0.0, 0.0, 0.0] # [m]

dimension = 3  # flag on the dimension to use [2 for 2D and 3 for 3D]
number_of_elements  = 100 # maximum volume of an element in the mesh[m^2]

mesh_generator = 'regular' # choice of the mesh generator

new_mesh = mesh(dimension, mesh_generator) # creates 3D mesh object
Ri = 1.5
Ro = 3.0
active_length = 5.0
n_ele = 200
new_mesh.racetrack(height, active_length, Ri, Ro, n_ele)           # adds a circle to the geometry

new_mesh.plot(0.0, 'Z') # mesh is plotted
new_mesh.plot(0.0, 'Y')

from matplotlib.pylab import show
show()


#------------------------------------------------------------------------------
# Example 3.2 - Create a more complex geometry
#------------------------------------------------------------------------------
mesh_2D = mesh(2, 'regular') # new 2D mesh object

mesh_2D.rectangle([[0,1],[0,1]],100)

mesh_2D.plot()

mesh_3D = mesh(3, 'regular')
mesh_3D.extrude_2D_mesh(2, 0.0, 1.0, mesh_2D)

mesh_3D.plot(0.1, 'Z')

mesh_3D.rotate([0, 0, 0.5])

mesh_3D.plot(0.1,'Z')

mesh_3D_tmp = mesh(3, 'regular')

mesh_3D_tmp.empty_cylinder(100, [1.0, 0.5, 1.0, [0.5, 0.5, 0.0]], 3)
mesh_3D.add_mesh(mesh_3D_tmp)

mesh_3D.plot(0.1, 'Z')
show()







