# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MAGCAL TUTORIAL - EXAMPLE 1

This tutorial shows how to use the 'mesh' method (mc.mesh) in order to create
2D meshes using "meshPy" mesh generator. When in 2D, the "meshPy" mesh
generator is a Python wrap over the "triangle" mesh generator.

The 'mesh' method wraps the "meshPy" mesh generator with a Python user
interface.

EXAMPLES INCLUDED :
    - 1.1 --> Creating the mesh of a circle using MeshPy
    - 1.2 --> Creating a more complex 2D mesh using MeshPy


For more information on MeshPy, see :
    http://mathema.tician.de/software/meshpy/
    
For more information on Triangle (contained in MeshPy), see :
    http://www.cs.cmu.edu/~quake/triangle.html
    
-------------------------------------------------------------------------------
WARNING : The mesh generator included with MagCal is rather basic and is
          only intended to show how to use MagCal, not to generate complex
          geometries and mesh them.
-------------------------------------------------------------------------------
Written by   : Kevin Ratelle
Created at   : Montreal
Created on   : January 14, 2014
Modification : January 14, 2014
-------------------------------------------------------------------------------
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh

#------------------------------------------------------------------------------
# Example 1.1 - Creating the mesh of a circle
#------------------------------------------------------------------------------
radius = 1.0    # radius of a circle [m]
center = [0, 0] # center of the circle

dimension = 2    # flag on the dimension to use [2 for 2D and 3 for 3D]
max_area  = 0.1  # maximum area of an element in the mesh[m^2]

mesh_generator = 'meshpy' # choice of the mesh generator

new_mesh = mesh(dimension, mesh_generator, max_area) # creates 2D mesh object
new_mesh.circle([radius, center])               # adds a circle to the geometry

new_mesh.mesh() # mesh is generated
new_mesh.plot() # mesh is plotted

from matplotlib.pylab import show
show()


#------------------------------------------------------------------------------
# Example 1.2 - Creating a complex 2D mesh
#------------------------------------------------------------------------------
new_mesh = mesh(2, 'meshpy', 0.002) # new 2D mesh object

new_mesh.rectangle([[0.0, 1.0], [-0.5, 0.5]])               # add a rectangle
new_mesh.add_geom(new_mesh.rectangle, [[2.0, 3.0],[-0.5, 0.5]]) # add a rectangle
new_mesh.substract(new_mesh.circle, [0.4,[0.65, 0.05]])         # substract circle
new_mesh.cut_line([[2.0, -2.0],[3.0, 2.0]], 'right')        # cuts a line in mesh

new_mesh.geomplot() # plots the geometry
new_mesh.mesh()     # creates the mesh
new_mesh.plot()     # plots the mesh

show()

