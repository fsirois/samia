# -*- coding: utf-8 -*-
'''----------------------------------------------------------------------------
-------------------------------------------------------------------------------
MAGCAL TUTORIAL - EXAMPLE 2

This tutorial shows how to use the 'mesh' method (mc.mesh) in order to create
2D meshes using a homemade mesh generator. The 2D homemade mesh generator
create regular meshes of different shapes. Complex shapes cannot be handled,
but it can handle separate shapes if they are in the library.

EXAMPLES INCLUDED :
    - 2.1 --> Creating the mesh of a shape using the 2D homemade mesher
    - 1.2 --> Creating a complex mesh using both the homemade mesher and the
              'MeshPy' by adding meshes.

For more information on mesh generation, see :
    - Documentation.pdf in the MagCal's root
    
-------------------------------------------------------------------------------
WARNING : The mesh generator included with MagCal is rather basic and is
          only intended to show how to use MagCal, not to generate complex
          geometries and mesh them.
-------------------------------------------------------------------------------
Written by   : Kevin Ratelle
Created at   : Montreal
Created on   : January 15, 2014
Modification : January 15, 2014
-------------------------------------------------------------------------------
----------------------------------------------------------------------------'''

from Samia.Mesh import mesh

#------------------------------------------------------------------------------
# Example 2.1 - Creating the mesh of a shape in the library
#------------------------------------------------------------------------------
radius = 1.0    # radius of a circle [m]
center = [0, 0] # center of the circle

new_mesh = mesh(2, 'ordered')  # creates 2D "ordered" mesh object

# The shape meshed is a "curved rectangle"
inner_radius = 2.0 # [m]
outer_radius = 3.0 # [m]
angle = 1.2        # [rad] opening angle

number_of_elements = 100 # It will TRY (not so well) to respect it

# Mesh generation automatic (no need for the .mesh() method)
new_mesh.curved_rectangle(inner_radius, outer_radius, angle, number_of_elements)
new_mesh.plot() # mesh is plotted

from matplotlib.pylab import show
show()


#------------------------------------------------------------------------------
# Example 2.2 - Adding meshes together
#------------------------------------------------------------------------------

# Mesh generated with meshpy --------------------------------------------------
new_mesh = mesh(2, 'ordered', 0.005) # new 2D MESHPY mesh object

new_mesh.rectangle([[0.0, 1.0], [-0.5, 0.5]], 100) # add a rectangle

# Mesh generated with the homemade function -----------------------------------
mesh_tmp = mesh(2, 'ordered')  # new 2D HOMEMADE mesh object

mesh_tmp.rectangle([[-1.4, -0.5],[-0.0, 1.0]], 300) # mesh a rectangle

# Adding one mesh to another --------------------------------------------------
new_mesh.add_mesh(mesh_tmp)

# Some more operations
new_mesh.rotate([1.0, [0.0, 0.0]])
new_mesh.translate([0.33, 0.53])


new_mesh.plot()
show()






