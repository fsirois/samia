'''
    This folder (Examples) contains all the examples included in Samia.

    This file imports and automatically runs all the tests.

    This is done to both show the minimalistic yet useful in most cases
    mesh class possible uses, and how to do it.
'''

import sys
import os

strCurrentDirectory = os.path.dirname(os.path.abspath(__file__))

# Append Samia root
strSamiaRoot = os.path.dirname(strCurrentDirectory)
sys.path.append(strSamiaRoot)

# this imports all mesh examples (and runs them)
import examples.mesh_examples
import examples.source_examples
import examples.method_of_moments_examples
import examples.superconductor_examples
import examples.other_examples

input("All Examples where done - Press Enter to quit.")